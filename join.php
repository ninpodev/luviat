<?php include 'header.php' ?>

<body>
    <!--
<div id="preloader">
    <div>
        <canvas id='circleback' height='100' width='100'></canvas>
        <canvas id='circlemain' height='100' width='100'></canvas>
    </div>
</div>-->
    
    
    


    
    
<div class="page-title-simple">
    
    <div class="container">
        
        <h1>Join</h1>
        
    </div>
    
</div>
    
<!-- Items to share -->
<div id="join" class="container">
    
    <div class="row">
        
        <div class="col-xl-6 offset-xl-3 col-12 mt-5 mb-5">
            
            <div class="main-contact-form">
                
                <div class="mb-4">
                    
                    <p class="w-100 text-center text-orange">Sign up with Facebook or Google</p>
                    
                    
                    <a class="btn btn-block btn-social btn-facebook text-white mb-3">
                        
                        <span class="fa fa-facebook-f"></span> Sign up with Facebook
                        
                    </a>
                    
                    <a class="btn btn-block btn-social btn-google text-white mb-3">
                        
                        <span class="fa fa-google"></span> Sign up with Google
                        
                    </a>
                    
                </div>
                
                <div class="w-100 text-center py-2 mt-4">
                    <p>Or</p>
                </div>
                
                <form class="row" >
                    
                    <div class="form-group has-feedback col-12 ">
                        
                        <input type="email" class="form-control pt-3" placeholder="Email *" />

                        <i class="glyphicon glyphicon-envelope form-control-feedback"></i>
                        
                    </div>
                    
                    <div class="form-group col-6 has-feedback">
                        
                        <input type="text" class="form-control pt-3" placeholder="First Name *" />

                        <i class="glyphicon glyphicon-user form-control-feedback"></i>
                        
                    </div>
                    
                    <div class="form-group col-6 has-feedback">
                        
                        <input type="text" class="form-control pt-3" placeholder="Last Name *" />

                        <i class="glyphicon glyphicon-user form-control-feedback"></i>
                        
                    </div>
                    
                    <div class="form-group has-feedback col-12 ">
                        
                        <input type="tel" class="form-control pt-3" placeholder="Phone Number *" />

                        <i class="glyphicon glyphicon-earphone form-control-feedback"></i>
                        
                    </div>
                    
                    <div class="form-group col-6 has-feedback">
                        
                        <input type="password" class="form-control pt-3" placeholder="Password *" />

                        <i class="glyphicon glyphicon-asterisk form-control-feedback"></i>
                        
                    </div>
                    
                    <div class="form-group col-6 has-feedback">
                        
                        <input type="password" class="form-control pt-3" placeholder="Repeat password *" />

                        <i class="glyphicon glyphicon-asterisk form-control-feedback"></i>
                        
                    </div>
                    
                    <div class="col-12 mt-2 mb-5">
                        
                        <input type="checkbox" name="daily" id="daily" class="css-checkbox" checked>
                        <label for="daily" class="css-label"> Keep me updated with offers and opportunities (we won't give your information out to any third party).</label>
                        
                    </div>
                    
                    <div class="form-group col-12 has-feedback">
                        
                        <input type="submit" form="contact-main" value="Join" class="input-button">
                        <br>
                        <small class="text-emperor" >By clicking Join or Sign up with Facebook or Google, I agree to Luviat's <a href="#">Terms of Service</a></small>
                        
                    </div>
                    
                </form>
                
                
                <div class="py-5 mt-2 border-top d-flex justify-content-center align-items-center">
                    <p class="m-0">Already have a Luviat Account? <a href="login.php" class="cta-btn ml-3" >Log In</a></p>
                </div>
                
            </div>
            
        </div>
        
    </div>
    
</div>


    <?php include 'footer.php' ?>