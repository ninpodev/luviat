<?php /*<?php include 'header.php'; ?> */?>


<?php include 'header.php'; ?>



<div class="page-title-simple m-b-30">
    <div class="container">
        <h1 class="text-emperor">Contact</h1>
    </div>
</div>


<div class="container ">
    <div class="m-b-30">
        <!--CONTACT MAP DARK BEGIN-->

<div class="contact-map-block-dark">

    <div class="contact-map-block-dark__info-wrap">
        <div class="contact-wrapper">
            <div>
                <h2>Contact us</h2>
                <div class="contact-map-block-dark__address-info">
                    <i class="material-icons">location_on</i>
                    <p> Level 5, 1 Moore Street, Civic. Canberra, Australia </p>

                </div>

                <div class="contact-map-block-dark__phone-block">
                    <span class="title">Phone:</span>
                    <span class="value">+61 431 250 611</span>
                    <span class="title">Skype:</span>
                    <span class="value">luviat.skype</span>
                    <span class="title">E-mail:</span>
                    <span class="value">hello@luviat.com</span>

                </div>
            </div>
        </div>
    </div>


    <div class="contact-map-block-dark__social-icons">
        <a href="#" class="icons__item icons__item_margin-right_small">
            <i class="fa fa-facebook icons" aria-hidden="true"></i>
        </a>
        <a href="#" class="icons__item icons__item_margin-right_small">
            <i class="fa fa-twitter icons" aria-hidden="true"></i>
        </a>
        <a href="#" class="icons__item icons__item_margin-right_small">
            <i class="fa fa-google icons" aria-hidden="true"></i>
        </a>
        <a href="#" class="icons__item">
            <i class="fa fa-pinterest-p icons" aria-hidden="true"></i>
        </a>


    </div>


</div>
<!--CONTACT MAP DARK END-->
    </div>
    <div class="m-b-30">
        <!--MAIN CONTACT FORM BEGIN-->

<div class="main-contact-form">
    <h2>Contact form</h2>
    <p>
        Did not find the answer to your question? Ask your question through this form and we will reply to our e-mail as
        soon as possible.
    </p>
    <form id="contact-main">
        <div class="input-wrapper input-name">
            <input type="text" placeholder="Name" form="contact-main" name="name" required id="contact-name"
                   autocomplete="off"
                   pattern="[a-zA-Z ]{2,25}">
            <label for="contact-name"></label>
        </div>
        <div class="input-wrapper input-email">
            <input type="email" id="contact-email" name="email" placeholder="Your e-mail" form="contact-main" required
                   autocomplete="off">
            <label for="contact-email"></label>
        </div>
        <div class="input-wrapper input-phone">
            <input type="tel" id="contact-phone" name="phone" placeholder="Your phone" form="contact-main" required
                   autocomplete="off">
            <label for="contact-phone"></label>
        </div>
        <div class="input-wrapper input-apartment">
            <input type="text" placeholder="Apartment" name="apartment" form="contact-main" required
                   id="contact-apartment"
                   autocomplete="off"
                   pattern="[a-zA-z0-9]{1,4}">
            <label for="contact-apartment"></label>
        </div>
        <div class="input-wrapper input-subject">
            <input type="text" id="contact-subject" name="subject" placeholder="Subject" form="contact-main" required
                   autocomplete="off">
            <label for="contact-subject"></label>
        </div>
        <textarea form="contact-main" placeholder="Your message" name="message" class="input-textarea"></textarea>
        <input type="submit" form="contact-main" value="Send Message" class="input-button">
    </form>
</div>
<!--MAIN CONTACT FORM END-->
    </div>

</div>


<?php include 'footer.php' ?>