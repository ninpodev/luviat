<?php include 'header.php' ?>

<body>

    
<div class="page-title-simple">
    
    <div class="container">
        
        <h1>Forgot Password</h1>
        
    </div>
    
</div>
    
<!-- Items to share -->
<div id="join" class="container">
    
    <div class="row">
        
        <div class="col-xl-6 offset-xl-3 col-12 mt-5 mb-5">
            
            <div class="main-contact-form">
                
                <form class="row" >
                    
                    <div class="form-group has-feedback col-12 ">
                      
                        <label class="font-400 text-emperor" >Enter the email address you used for registration</label>
                        <br>
                        
                        <input type="email" class="form-control pt-3" placeholder="Email *" />

                        <i class="glyphicon glyphicon-envelope form-control-feedback"></i>
                        
                    </div>
                    
                    <div class="form-group col-12 has-feedback">
                        
                        <input type="submit" form="contact-main" value="Send" class="input-button">
                    </div>
                    
                </form>
                
                
                <div class="py-5 mt-2 border-top d-flex justify-content-center align-items-center">
                    <p class="m-0">Don't have an Account? <a href="join.php" class="cta-btn ml-3" >Join</a></p>
                </div>
                
            </div>
            
        </div>
        
    </div>
    
</div>


    <?php include 'footer.php' ?>