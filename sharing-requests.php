<?php include 'header.php'; ?>

<div class="account-page">
    
    <?php include 'user-sidebar.php' ?>
    
    <div class="account-page-wild_block">
        
        <div id="sharing-requests" class="js-content-blocks p-5">
            
            <h1 class="account-header-top mb-4">Your Sharing requests</h1>
            <p>Items, services or experiences someone is requesting from you</p>
            
            <div class="row">
                
                <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
                    
                    <div class="account-property m-b-30">
                        
                        <div class="m-b-30 no-header-agent-block-warpper">
                            
                            <!--SHARING REQUEST START-->

                            <div class="agent-block-wrapper mb-4">

                                    <div class="agent-block-agent-img">

                                        <div class="image-wrap">

                                            <div class="agent-status" title="online">

                                            </div>

                                            <a href="#chatorsomething">

                                                <img src="./images/userpicbig.png" alt="agent-photo">

                                            </a>

                                        </div>

                                    </div>

                                    <div class="agent-block-agent-information">

                                        <div class="row">

                                            <div class="block-left col-3">

                                                <h2 class="text-emperor font-400" >Ski Boots</h2>

                                            </div>

                                            <div class="block-right col-9 mb-4">

                                                <span class="small-text text-orange mb-4">From: Pauline Sanders</span>

                                            </div>

                                        </div>

                                        <div class="row mb-4">

                                            <div class="block-left col-3">

                                                <div class="stars" data-stars="3">

                                                    <i class="material-icons star">star</i>

                                                    <i class="material-icons star">star</i>

                                                    <i class="material-icons star">star</i>

                                                    <i class="material-icons star">star</i>

                                                    <i class="material-icons star">star</i>

                                                </div>

                                            </div>

                                            <div class="block-right col-6">

                                                <div class="text-container">

                                                    Hey Pauline! I'm Frances and I'm travelling to Canberra with my partner. We're both into skling and would love to borrow your ski boots for a weekend trip to Perisher. We have a few questions: are the ski boots size 11 womens or? Do....

                                                </div>

                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="col-xl-3 col-lg-3 col-md-6 col-xs-6 col-6 mb-md-5 mb-sm-5 mb-5">

                                                <div class="font-700 text-emperor">

                                                    July 08, 2018

                                                </div>

                                            </div>

                                            <div class="col-xl-3 col-lg-3 col-md-6 col-xs-6 col-6 mb-xl-0 mb-lg-0 mb-md-5 mb-sm-5 mb-5">

                                                <div class="font-700 text-emperor">

                                                    <span>Borrow date(s):</span>

                                                    <br>

                                                    <span>12.08.2018 - 15.08.2018</span>

                                                </div>

                                            </div>

                                            <div class="col-xl-6 col-lg-6 col-md-12 col-xs-12 col-12 text-right">


                                                <div class="button-wrapper btnwrspe d-inline mr-4">

                                                    <a href="send-message-to-borrower.php">VIEW CONVERSATION</a>

                                                </div>

                                                <div class="d-block m-4">

                                                    <span class="font-400 text-uppercase" style="font-size:11px;" >Or</span> <a href="#" class="font-400 text-cancelled text-uppercase" style="font-size:11px;" data-toggle="modal" data-target="#cancel-borrowing-request" >Cancel this request</a>



                                                </div>


                                            </div>

                                        </div>

                                    </div>

                                </div>
                            
                            <!--SHARING REQUEST START-->

                            <div class="agent-block-wrapper mb-4">

                                    <div class="agent-block-agent-img">

                                        <div class="image-wrap">

                                            <div class="agent-status" title="online">

                                            </div>

                                            <a href="#chatorsomething">

                                                <img src="./images/userpicbig.png" alt="agent-photo">

                                            </a>

                                        </div>

                                    </div>

                                    <div class="agent-block-agent-information">

                                        <div class="row">

                                            <div class="block-left col-3">

                                                <h2 class="text-emperor font-400" >Ski Boots</h2>

                                            </div>

                                            <div class="block-right col-9 mb-4">

                                                <span class="small-text text-orange mb-4">From: Pauline Sanders</span>

                                            </div>

                                        </div>

                                        <div class="row mb-4">

                                            <div class="block-left col-3">

                                                <div class="stars" data-stars="3">

                                                    <i class="material-icons star">star</i>

                                                    <i class="material-icons star">star</i>

                                                    <i class="material-icons star">star</i>

                                                    <i class="material-icons star">star</i>

                                                    <i class="material-icons star">star</i>

                                                </div>

                                            </div>

                                            <div class="block-right col-6">

                                                <div class="text-container">

                                                    Hey Pauline! I'm Frances and I'm travelling to Canberra with my partner. We're both into skling and would love to borrow your ski boots for a weekend trip to Perisher. We have a few questions: are the ski boots size 11 womens or? Do....

                                                </div>

                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="col-xl-3 col-lg-3 col-md-6 col-xs-6 col-6 mb-md-5 mb-sm-5 mb-5">

                                                <div class="font-700 text-emperor">

                                                    July 08, 2018

                                                </div>

                                            </div>

                                            <div class="col-xl-3 col-lg-3 col-md-6 col-xs-6 col-6 mb-xl-0 mb-lg-0 mb-md-5 mb-sm-5 mb-5">

                                                <div class="font-700 text-emperor">

                                                    <span>Borrow date(s):</span>

                                                    <br>

                                                    <span>12.08.2018 - 15.08.2018</span>

                                                </div>

                                            </div>

                                            <div class="col-xl-6 col-lg-6 col-md-12 col-xs-12 col-12 text-right">


                                                <div class="button-wrapper btnwrspe d-inline mr-4">

                                                    <a href="send-message-to-borrower.php">VIEW CONVERSATION</a>

                                                </div>

                                                <div class="d-block m-4">

                                                    <span class="font-400 text-uppercase" style="font-size:11px;" >Or</span> <a href="#" class="font-400 text-cancelled text-uppercase" style="font-size:11px;" data-toggle="modal" data-target="#cancel-borrowing-request" >Cancel this request</a>



                                                </div>


                                            </div>

                                        </div>

                                    </div>

                                </div>
                            
                            <div class="pagination-wrap m-b-30">

                                    <ul>

                                        <li>

                                            <a href="#"><i class="material-icons">&#xE5C4;</i></a>

                                        </li>

                                        <li>

                                            <a href="#">1</a>

                                        </li>

                                        <li>

                                            <a href="#">2</a>

                                        </li>

                                        <li>

                                            <a href="#">3</a>

                                        </li>

                                        <li>

                                            <a href="#">4</a>

                                        </li>

                                        <li>

                                            <a href="#"><i class="material-icons">&#xE5C8;</i></a>

                                        </li>

                                    </ul>

                                </div>
                            
                        </div>
                    </div>
                    
                </div>
                
                <div class="col-xs-3 col-lg-3 col-md-!2 col-sm-12 col-12">
                    
                    <div class="account_properties_counters_list">
                        
                        <a href="#" class="item">
                            <span class="badge rented"></span> Requested
                            <span class="count">1</span>
                        </a>
                        
                        <a href="#" class="item">
                            <span class="badge accepted"></span> Accepted
                            <span class="count">1</span>
                        </a>
                        <a href="#" class="item">
                            <span class="badge denied"></span> Denied
                            <span class="count">1</span>
                        </a>
                        
                        
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>

<?php include 'scripts.php'; ?>


</body>

</html>