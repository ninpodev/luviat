<?php include 'header.php' ?>

<body>

        
<div class="page-title-simple mb-5">
    <div class="container">
        <h1>Share an item</h1>
    </div>
</div>
    
<div class="container">
    
    <div class="row position-relative">
    
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 ">
        
            <div id="preview-item">
            <div class="property-block">
                    <p class="property-type">Item</p>
                    <p class="property-title"><a href="#">Ski Boots</a></p>

                <div class="apartment-image">
                        <img src="./images/image.png" alt="image">
                        <div class="badges">
                            
                            <!--<p class="sale">For Sale</p>-->
                        </div>

                    </div>

                    <div class="apartment-values">
                        <span><i class="material-icons">place</i> 6Km Away</span>
                    </div>
                    <div class="apartment-values">
                        <span>Size 12</span>
                        <span>Nordica </span>
                    </div>
                    <div class="apartment-info">
                        <div class="apartment-price">
                            <p class="price-big">$ 24</p>
                            <p class="price-small">week $67</p>
                        </div>
                        <div class="icons">
                            <a href="#" class="clone"> <i class="material-icons icons-style"></i></a>
                            <a href="#" class="heart"><i class="material-icons icons-style"></i></a>
                        </div>
                    </div>
                    <div class="apartment-manager">
                        <div class="manager-wrap">
                            <div class="manager-icon">
                                <a href="#chat"> <img src="./images/userpic.png" alt="userpic"></a>
                                <div class="online-status"></div>
                            </div>
                            <span class="manager-name">Eleanor French</span>
                        </div>
                        <div class="calendar">
                            <i class="material-icons">insert_invitation</i>
                            <span> 2 days ago</span>
                        </div>
                    </div>
                </div>
        </div>
    </div>
        
        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
            
            <form>
                <div id="toolbar">
                    <div class="submit-property-btns">
                        <div>
                            <!--comment-->
                            <a href="#" class="button-link-normal">
                                Preview Offer
                            </a>
                            <!--hola-->
                        </div>
                        <a href="#" class="cta-btn">
                            <span><i class="fa fa-check" aria-hidden="true"></i> Publish Item</span>
                        </a>
                    </div>
                </div>
                
                <p class="m-3 float-right">*Required Fields</p>
                <br>

                <!--ITEM OVERVIEW-->
                <div class="form-box bg-grey p-5 rounded mt-5">

                    <h3 class="font-open-sans text-emperor font-700 mb-5">Item Overview</h3>

                    <div class="mb-4">
                        <label class="font-open-sans font-400 text-emperor bg-transparent"> Name of Item*</label>
                        <div class="clearfix"></div>
                        <input type="text" placeholder="<Auto populated from previous screen>" class="w-100">
                    </div>

                    <div class="mb-4">
                        <label class="font-open-sans font-400 text-emperor bg-transparent"> Category*</label>
                        
                        <p> What is the area of interest this item falls into? You must enter at least one category with a maximum of four.</p>
                        <div class="clearfix"></div>
                        <input type="text" placeholder="eg. Skiing, Climbing, Education, Sports" class="w-100">
                    </div>
                    
                    <div class="mb-4">
                        <label class="font-open-sans font-400 text-emperor bg-transparent"> Short Description*</label>
                        <div class="clearfix"></div>
                        
                        <textarea rows="3" placeholder="Tell us about the item in 200 characters or less" maxlength="600" data-limit="200" ></textarea>
                        <span class="countdown">200 Characters remaining</span>
                        
                    </div>
                    
                    <div class="mb-4">
                        <label class="font-open-sans font-400 text-emperor bg-transparent"> Quantity*</label>
                        <div class="clearfix"></div>
                        
                        <input type="number">
                        
                    </div>

                    <div class="alert alert-danger mt-4" role="alert">
                        <p>Missing fields</p>
                    </div>

                </div>

                <!--ITEM DESCRIPTION-->
                <div class="form-box bg-grey p-5 rounded mt-5">
                    
                    <h3 class="font-open-sans text-emperor font-700 mb-5">Item Description</h3>

                    <div class="mb-4">
                        
                        <div class="description-textarea">
                        
                            <textarea rows="8" placeholder="Description" maxlength="600" data-limit="600" ></textarea>
                            <span class="countdown">600 Characters remaining</span>

                        
                            
                            
                        </div>
                        
                    </div>


                    <div class="alert alert-danger mt-4" role="alert">
                        <p>Missing fields</p>
                    </div>



                </div>
                
                <!--IMAGES-->
                <div class="form-box bg-grey p-5 rounded mt-5">
                    <h3 class="font-open-sans text-emperor font-700 mb-5">Images</h3>

                    <div class="property-img-drag-drop">
                        <div class="styling-file-input">
                            <label>
                                <span class="drag-span"> Drag and drop images here or</span>
                                <span class="select-span">Select Image</span>
                                <input class="style-file-input" type="file">
                            </label>
                        </div>
                        <div class="image-miniatures-grid">

                            <div class="image-miniature">
                                <img src="./images/mask.png" alt="image">
                                <div class="mask">
                                    <i class="material-icons mask-controls">done</i>
                                    <i class="material-icons mask-controls">close</i>
                                </div>
                            </div>
                            <div class="image-miniature">
                                <img src="./images/mask.png" alt="image">
                                <div class="mask">
                                    <i class="material-icons mask-controls">done</i>
                                    <i class="material-icons mask-controls">close</i>
                                </div>
                            </div>
                            <div class="image-miniature">
                                <img src="./images/mask.png" alt="image">
                                <div class="mask">
                                    <i class="material-icons mask-controls">done</i>
                                    <i class="material-icons mask-controls">close</i>
                                </div>
                            </div>


                        </div>
                    </div>
                    <br>
                    <p>Note: Images should not exceed 2mb. The best images are in a 1:1 ratio.</p>

                </div>

                <!--STORAGE ACCESS INFORMATION-->
                <div class="form-box bg-grey p-5 rounded mt-5">
                    <h3 class="font-open-sans text-emperor font-700 mb-5">Storage/access Information</h3>

                    <div class="mb-4">
                        <div class="row">
                            <div class="col-8">
                                <div class="location">
                                    <label class="font-open-sans font-400 text-emperor bg-transparent">Location</label>
                                    <input type="text" class="w-100" placeholder="54 Canberra Ave">
                                    <span style="font-size:12px;" >Address won't be displayed publically</span>
                                </div>
                            </div>
                            <div class="col-4">
                                <label>&nbsp;</label>
                                <div class="map-btn">
                                    <button class="button-secondary w-100" id="deploy-map">Select on google map</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="map-selector" class="mb-4">
                        <div class="map-responsive">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3254.9512399519494!2d149.16409919999998!3d-35.3320323!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b164c4488ec7559%3A0xa214ec1dfedc4023!2s54+Canberra+Ave%2C+Fyshwick+ACT+2609%2C+Australia!5e0!3m2!1ses!2scl!4v1530756283902" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>


                    <div class="mb-4">
                        <div class="row">
                            <div class="col-12">
                                <div class="property-features-block">
                                    <label class="font-open-sans font-400 text-emperor bg-transparent mb-3">Pickup or Delivery?</label>
                                        <div class="checkboxes-block">
                                            <div>
                                                <label class="radio-label">Pickup only
                                                    <input type="radio" name="pickup-delivery">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <br>
                                            </div>
                                            <div>
                                                <label class="radio-label">Pickup &amp; Delivery
                                                    <input type="radio" name="pickup-delivery">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>

                    <div class="mb-4">
                        <label class="font-open-sans font-400 text-emperor bg-transparent"> Distance willing to travel*</label>
                        <div class="clearfix"></div>
                        
                        <input type="number">
                        <span>Kms.</span>
                        
                    </div>

                    <div class="alert alert-danger mt-4" role="alert">
                        <p>Missing fields</p>
                    </div>
                    </div>
                
                <!--AVAILABILITY-->
                <div class="form-box bg-grey p-5 rounded mt-5">
                    
                    <h3 class="font-open-sans text-emperor font-700 mb-5">Availability</h3>
                    
                    <p>When is this item/experience/service available?</p>
                    
                    <div class="mb-4">
                        
                        <div id="widget-modal" class="specialcont bg-grey border-0">

                        <div class="form-box mb-4">

                            <div class="row">

                                <div class="col-6">

                                    <label class="radio-label">
                                        On request (available until you make it unavailable)

                                        <input type="radio" name="availability" id="on-request-check" checked>

                                        <span class="checkmark"></span>

                                    </label>

                                </div>

                                <div class="col-6">

                                    <label class="radio-label">On specified date(s)/time(s)

                                        <input type="radio" name="availability" id="on-specific-check">

                                        <span class="checkmark"></span>

                                    </label>

                                </div>

                            </div>

                        </div>
                        
                        <div id="request" class="row">
                    
                        </div>
                        
                        <div id="specific-times" class="row">
                            
                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6 mb-4">
                                <input class="datepick" placeholder="DDMMYY">
                            </div>
                            
                            <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6 col-6 mb-4">
                                <input class="timepick" placeholder="00:00">
                            </div>
                            <div class="col-xl-2 col-lg-2 col-md-12 col-sm-12 col-12 p-xl-0 p-lg-0 p-md-0 p-sm-5 d-flex justify-content-center align-items-center mb-4">
                                <span> to </span>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6 mb-4">
                                <input class="datepick" placeholder="DDMMYY">
                            </div>
                            <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6 col-6 mb-4">
                                <input class="timepick" placeholder="00:00">
                            </div>
                            
                            <!--<div class="col-12">
                                
                                <select id="cities" class="selectpicker" data-size="40">
                                    <option>Doesn't repeat</option>
                                    <option>Daily</option>
                                    <option>Weekly on [day of the week]</option>
                                    <option>Monthly on the first [day]</option>
                                    <option>Anually on 01 sept</option>
                                    <option>Every weekday</option>
                                    <option value="custom">Custom</option>
                                </select>       
                                
                            </div>-->
                    
                        </div>
                        
                    </div>
                        
                        <!--<input type="text" id="range-picker" class="w-50" >-->
                        
                    </div>
                </div>
                
                <!--PAYMENT-->
                <div class="form-box bg-grey p-5 rounded mt-5">
                    <h3 class="font-open-sans text-emperor font-700 mb-5">Payment</h3>

                    <div class="mb-4">
                        <div class="row">
                            <div class="col-12">
                                <div class="location mb-4">
                                    <label class="font-open-sans font-400 text-emperor bg-transparent">Is there a cost associated with attending this experience? If not, leave blank</label>
                                </div>
                                
                                
                                <div class="row">
                                    <div class="col-4">
                                        <input type="checkbox" name="negotiable" id="negotiable" class="css-checkbox">
                                        <label for="negotiable" class="css-label">Negotiable</label>
                                    </div>
                                    
                                    <div class="col-4" id="hourly-box"> 
                                        <input type="checkbox" name="hourly" id="hourly" class="css-checkbox">
                                        <label for="hourly" class="css-label">per Hour</label>
                                        <input type="text" placeholder="$AUD" id="hourly-input" class="w-100">
                                        
                                    </div>
                                    
                                    <div class="col-4" id="daily-box">
                                        <input type="checkbox" name="daily" id="daily" class="css-checkbox">
                                        <label for="daily" class="css-label">per Day</label>
                                        <input type="text" placeholder="$AUD" id="daily-input" class="w-100">
                                    </div>
                                </div>
                                 
                                
                                <div class="row align-items-center mb-3">
                                    <div class="col-4">
                                        
                                    </div>
                                    <div class="col-6">
                                        
                                    </div>
                                </div>
                                
                                <div class="row align-items-center mb-3">
                                    <div class="col-4">
                                        
                                    </div>
                                </div>
                                
                                <div class="row align-items-center mb-3">
                                    <div class="col-4">
                                        
                                    </div>
                                </div>
                                
                                <div class="location mt-4">
                                    <label class="font-open-sans font-400 text-emperor bg-transparent">Pick up Fee</label>
                                </div>
                                <div class="row align-items-center mb-3">
                                    <div class="col-3">
                                        <input type="text" placeholder="$AUD" class="w-100">
                                    </div>
                                </div>
                                
                                
                            </div>
                        </div>
                    </div>
                    
                    <div class="mb-5">
                        <div class="row">
                            <div class="col-12">
                                
                                <div class="location">
                                    <label class="font-open-sans font-400 text-emperor bg-transparent">Accepted payment methods</label>
                                </div>
                                <div class="row">
                                    <div class="col-4" >
                                        <input type="checkbox" name="cash" id="cash" class="css-checkbox">
                                        <label for="cash" class="css-label">Cash</label>
                                    </div>
                                    <div class="col-4">
                                        <input type="checkbox" name="Paypal" id="Paypal" class="css-checkbox">
                                        <label for="Paypal" class="css-label">Paypal</label>
                                    </div>
                                    <div class="col-4">
                                        <input type="checkbox" name="Debit" id="Debit" class="css-checkbox">
                                        <label for="Debit" class="css-label">Direct Debit</label>
                                    </div>
                                </div>
                                
                                
                                <div class="location mt-4">
                                    <label class="font-open-sans font-400 text-emperor bg-transparent">Deposit</label>
                                </div>
                                <div class="row align-items-center mb-4">
                                    <div class="col-3">
                                        <input type="text" placeholder="$AUD" class="w-100">
                                    </div>
                                </div>
                                
                                
                                
                            </div>
                        </div>
                    </div>
                    
                    <div class="mb-5">
                        <p class="colorgray"><strong>Note: Luviat does not manage payment.</strong></p>
                    </div>

                    <div class="alert alert-danger mt-4" role="alert">
                        <p>Missing fields</p>
                    </div>
                    </div>
                
                <!--CONDITION FOR ACCESS-->
                <div class="form-box bg-grey p-5 rounded mt-4">
                    <h3 class="font-open-sans text-emperor font-700 mb-5">Condition for access *</h3>

                    <div class="mb-4">
                        <div class="row">
                            <div class="col-12">
                                <div class="property-features-block">
                                    <label class="font-open-sans font-400 text-emperor bg-transparent mb-3">Are there any restrictions or licences required for this item?</label>
                                        <div class="checkboxes-block">
                                            <div>
                                                <label class="radio-label">No
                                                    <input type="radio" name="condition">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <br>
                                            </div>
                                            <div>
                                                <label class="radio-label">Yes
                                                    <input type="radio" name="condition">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    
                    <div class="mb-4">
                        <label class="font-open-sans font-400 text-emperor bg-transparent">More Information</label>
                        <div class="clearfix"></div>
                        <input type="text" placeholder="eg. Restrictions, licenses, etc." class="w-100">
                    </div>


                    <div class="alert alert-danger mt-4" role="alert">
                        <p>Missing fields</p>
                    </div>
                    </div>
                <br>
                <p>*Required Fields</p>
                
            </form>
            
        </div>
        
    </div>
    
</div>
    


    <?php include 'footer.php' ?>