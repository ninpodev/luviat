
<div class="footer">
    <div class="container">
        <div class="row footer-border">
            <div class="col-md-7">
                <div class="popular-destanations">
                    <h3>Available destanations:</h3>
                    <ul class="places">
                        <li>
                            <ul>
                                <li>
                                    <a href="#">
                                        Canberra
                                    </a>
                                </li>
                                <!--<li>
                                    <a href="#">
                                        Cayman Islands
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Tampa Bay Area, USA
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Seattle, Washington
                                    </a>
                                </li>-->
                            </ul>
                        </li>
                        <!--<li>
                            <ul>
                                <li>
                                    <a href="#">
                                        Charleston, USA
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        The Bahamas
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Costa del Sol, Spain
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                       French Riviera, France
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <ul>
                                <li>
                                    <a href="#">
                                        Milan, Italy
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Scottsdale and Paradise 
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Vail, Colorado, USA
                                    </a>
                                </li>
                            </ul>
                        </li>-->
                    </ul>
                    <h3>Stay updated with offers and opportunities, join our mailing list (we won't give your information out to any third party):</h3>
                    <input type="email" placeholder="Enter your e-mail">
                    <button type="button">subscribe</button>
                </div>
            </div>
            <div class="col-md-4 col-md-offset-1">
                <div class="row contacts">
                    <div class="col-md-12 col-sm-6">
                        <h3>Contact us:</h3>
                        <div class="address">Level 5, 1 Moore Street, Civic. Canberra, Australia</div>
                        <dl>
                            <dt>Phone:</dt>
                            <dd>+61 431 250 611</dd>
                            <dt>Skype:</dt>
                            <dd>luviat.skype</dd>
                            <dt>E-mail:</dt>
                            <dd><a href="#">hello@luviat.com</a></dd>
                        </dl>
                    </div>
                    <div class="col-md-12 col-sm-6">
                        <h3>Follow us:</h3>
                        <div class="social-wrap">
                            <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a href="#"><i class="fa fa-google" aria-hidden="true"></i></a>
                            <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row footer-bottom">
            <div class="col-sm-6">
                <div class="copyright">© 2018 — Luviat</div>
            </div>
            <div class="col-sm-6 text-right">
                <a href="#">Site map</a>
                <a href="#">Privacy policy</a>
                <a href="#">Terms & Conditions</a>
            </div>
        </div>
    </div>
</div>



<?php include 'scripts.php' ?>
</body>

</html> 