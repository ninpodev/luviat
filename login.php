<?php include 'header.php' ?>

<body>

    
<div class="page-title-simple">
    
    <div class="container">
        
        <h1>Log In</h1>
        
    </div>
    
</div>
    
<!-- Items to share -->
<div id="join" class="container">
    
    <div class="row">
        
        <div class="col-xl-6 offset-xl-3 col-12 mt-5 mb-5">
            
            <div class="main-contact-form">
                
                <div class="mb-4">
                    
                    <p class="w-100 text-center text-orange">Log in with Facebook or Google</p>
                    
                    
                    <a class="btn btn-block btn-social btn-facebook text-white mb-3">
                        
                        <span class="fa fa-facebook-f"></span> Log in with Facebook
                    </a>
                    
                    <a class="btn btn-block btn-social btn-google text-white mb-3">
                        
                        <span class="fa fa-google"></span> Log in with Google
                        
                    </a>
                    
                </div>
                
                <div class="w-100 text-center py-2 mt-4">
                    <p>Or</p>
                </div>
                
                <form class="row" >
                    
                    <div class="form-group has-feedback col-12 ">
                        
                        <input type="email" class="form-control pt-3" placeholder="Email *" />

                        <i class="glyphicon glyphicon-envelope form-control-feedback"></i>
                        
                    </div>
                    
                    <div class="form-group has-feedback col-12 ">
                        
                        <input type="password" class="form-control pt-3" placeholder="Password *" />

                        <i class="glyphicon glyphicon-asterisk form-control-feedback"></i>
                        
                    </div>
                    
                    
                    <div class="col-6 mt-2 mb-5">
                        
                        <input type="checkbox" name="daily" id="daily" class="css-checkbox">
                        <label for="daily" class="css-label">Remember me </label>
                        
                    </div>
                    
                     <div class="col-6 mt-2 mb-5 text-right">
                        
                        <p><a href="forgot-password.php" class="text-orange">Forgot Password?</a></p>
                        
                    </div>
                    
                    <div class="form-group col-12 has-feedback">
                        
                        <input type="submit" form="contact-main" value="Log In" class="input-button">
                    </div>
                    
                </form>
                
                
                <div class="py-5 mt-2 border-top d-flex justify-content-center align-items-center">
                    <p class="m-0">Don't have an Account? <a href="join.php" class="cta-btn ml-3" >Join</a></p>
                </div>
                
            </div>
            
        </div>
        
    </div>
    
</div>


    <?php include 'footer.php' ?>