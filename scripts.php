<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script type="text/javascript" src="js/library/bootstrap.js"></script>

<script type="text/javascript" src="js/jquery.sticky-kit.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>


<script type="text/javascript" src="js/property_carousel.js"></script>
<script type="text/javascript" src="js/partner_carousel.js"></script>
<script type="text/javascript" src="js/agents_carousel.js"></script>
<script type="text/javascript" src="js/property_slider.js"></script>
<script type="text/javascript" src="js/property_gallery.js"></script>
<script type="text/javascript" src="js/agent_slider.js"></script>
<script type="text/javascript" src="js/library/owl.carousel.js"></script>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBBGotOna22MxxiHpaGPV1aEOYTMx428-g"></script>
<script type="text/javascript" src="js/library/markerclusterer.js"></script>
<script type="text/javascript" src="js/library/ion.rangeSlider.min.js"></script>
<script type="text/javascript" src="js/library/jquery.mousewheel.js"></script>
<script type="text/javascript" src="js/library/chartist.min.js"></script>
<script type="text/javascript" src="js/library/microplugin.js"></script>
<script type="text/javascript" src="js/library/sifter.js"></script>
<script type="text/javascript" src="js/library/selectize.js"></script>
<script type="text/javascript" src="js/library/progressbar.js"></script>
<script type="text/javascript" src="js/library/module.js"></script>
<script type="text/javascript" src="js/library/hotkeys.js"></script>
<script type="text/javascript" src="js/library/uploader.js"></script>
<script type="text/javascript" src="js/library/simditor.js"></script>
<script type="text/javascript" src="js/rangeslider.js"></script>

<script type="text/javascript" src="js/post_slider.js"></script>
<script type="text/javascript" src="js/counters.js"></script>
<script type="text/javascript" src="js/slider_text.js"></script>
<script type="text/javascript" src="js/pageviews_chart.js"></script>
<script type="text/javascript" src="js/search_form.js"></script>
<script type="text/javascript" src="js/search_form_slider.js"></script>
<script type="text/javascript" src="js/count_checker.js"></script>
<script type="text/javascript" src="js/form_toggle.js"></script>
<script type="text/javascript" src="js/featured_carousel.js"></script>
<script type="text/javascript" src="js/one_element_slider.js"></script>
<script type="text/javascript" src="js/accordion.js"></script>
<script type="text/javascript" src="js/partners_small.js"></script>
<script type="text/javascript" src="js/pageviews_chart.js"></script>
<script type="text/javascript" src="js/intro_page.js"></script>
<script type="text/javascript" src="js/map_component.js"></script>
<script type="text/javascript" src="js/header.js"></script>

<script type="text/javascript" src="js/visible.js"></script>
<script type="text/javascript" src="js/coming-soon.js"></script>
<script type="text/javascript" src="js/rate_article.js"></script>
<script type="text/javascript" src="js/service-page-size.js"></script>
<script type="text/javascript" src="js/preloader.js"></script>
<script type="text/javascript" src="js/clear_form.js"></script>

<script type="text/javascript" src="js/area_tabs.js"></script>
<script type="text/javascript" src="js/testimonials.js"></script>
<script type="text/javascript" src="js/sign_in.js"></script>
<script type="text/javascript" src="js/account_page.js"></script>
<script type="text/javascript" src="js/chat_component.js"></script>
<script type="text/javascript" src="js/account_chart.js"></script>
<script type="text/javascript" src="js/account_component.js"></script>
<script type="text/javascript" src="js/text_editor.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/js/bootstrap-select.min.js"></script>

<script type="text/javascript" src="js/custom.js"></script>

<script type="text/javascript" src="js/main.js"></script>



<!--<script type="text/javascript" src="js-min/min.js"></script>-->
