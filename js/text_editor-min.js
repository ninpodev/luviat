/**
 * @encoding     UTF-8
 * @copyright    Copyright (C) 2016 Torbara (http://torbara.com). All rights reserved.
 * @license      Envato Standard License http://themeforest.net/licenses/standard?ref=torbara
 * @author       Alexei Andriyashevskyi (a.andriyashevskyi@gmail.com)
 * @support      support@torbara.com
 */
!function($,t){"use strict";$.fn.textEditor=function(){var e=$(this);return this.each(function(){t.locale="en-US";var n=new t({textarea:e,toolbar:["bold","underline","ul","ol"]})})}}(jQuery,Simditor);