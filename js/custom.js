//CUSTOM
$(window).on('load',function(){
    
    //$('#widget-modal').modal('show');

    $('#request').hide();

    $('#specific-times').hide();

});

$('#editor').textEditor();
         
$("#range-picker").flatpickr({
    mode:"range",
    minDate: "today",
    inline: true,
    altFormat: "F j, Y",
    dateFormat: "Y-m-d",
});
        
$(".datepick").flatpickr({
    minDate: "today",
    altFormat: "F j, Y",
    dateFormat: "Y-m-d",
    allowInput:true,
});

$(".timepick").flatpickr({
    enableTime: true,
    noCalendar: true,
    dateFormat: "H:i",
    allowInput:true,
});
        
$("#sidebar-price").stick_in_parent({
            parent:'#sidebar-single',
            offset_top: 100,
        });
        
$("#preview-item").stick_in_parent({
            offset_top:100
        });

$("#change-email-frequency").stick_in_parent({});

$("#toolbar").stick_in_parent({
            offset_top:100
        });

$('#map-selector').hide();

$('#deploy-map').on('click', function(e) {
            $('#map-selector').toggle();
            e.preventDefault();
        });
        
$(this).scrollTop(0);
        
function goBack() {
            window.history.back();
        }
        
$('textarea').on("propertychange keyup input paste", function () {
            var limit = $(this).data("limit");
            var remainingChars = limit - $(this).val().length;
            if (remainingChars <= 0) {
                $(this).val($(this).val().substring(0, limit));
            }
            $(this).next('span').text(remainingChars<=0?0:remainingChars + ' ' + 'Characters remaining');
        });

$('.on-date').hide();

$("input[name='datecheck'].date").change(function() {
        $(".on-date").show();
});

$("input[name='datecheck'].never").change(function() {
        $(".on-date").hide();
});

$('input[name="negotiable"]').click(function() {
    
    if($(this).is(':checked')) { 
        $('#hourly-box').addClass("disabled");
        $('#daily-box').addClass("disabled");
        
        $('#daily').attr('disabled', 'disabled');
        $('#daily-input').attr('disabled', 'disabled');
        
        $('#hourly').attr('disabled', 'disabled');
        $('#hourly-input').attr('disabled', 'disabled');
        
    } else {
        $('#hourly-box').removeClass("disabled");
        $('#daily-box').removeClass("disabled");
        
        $('#daily').removeAttr('disabled');
        $('#daily-input').removeAttr('disabled');
        
        $('#hourly').removeAttr('disabled');
        $('#hourly-input').removeAttr('disabled');
    }
    
});

$('#nav-icon1').click(function(){
    $(this).toggleClass('open');
    $('body, html').toggleClass('scrolllock');
    $('#mobile-menu').toggleClass('open');
});

$(window).resize(function() {
    var width = $(document).width();
    if (width > 991) {
        $('#nav-icon1').removeClass('open');
        $('#mobile-menu').removeClass('open');
        $('body, html').removeClass('scrolllock');
    }
});


//WIDGET
$('#on-request-check').click(function(){
    $('#request').show();
    $('#specific-times').hide();    
});

$('#on-specific-check').click(function(){
    $('#request').hide();
    $('#specific-times').show();    
});


//SELECTS
$('.selectpicker').selectpicker({
    showTick: true,
    size: 5
});

$('#cities').change(function() { //jQuery Change Function
    var opval = $(this).val(); //Get value from select element
    if(opval=="custom"){ //Compare it and if true
        $('#custom-modal').modal("show"); //Open Modal
    }
});

//END CUSTOM


(function ($, window, document) {
    "use strict";
    $(document).ready(function () {
		$('.stars').each(function(){
			var stars = $(this).attr('data-stars')
			for(var i = 0; i<stars; i++){
				$(this).find('.star').eq(i).addClass('active')
			}
		})
		$('.bg').each(function(){
			var img = $(this).find('img')
			$(this).css('background-image', 'url('+img.attr('src')+')')
		})
		$(function () {
			$('#myTab li:nth-child(1) a').tab('show')
		})

    });

})(jQuery, window, document);
