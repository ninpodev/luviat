<?php include 'header.php'; ?>

<div class="account-page">
    
    <?php include 'user-sidebar.php' ?>
    
    <div class="account-page-wild_block">
        
        <div id="borrowing-requests" class="js-content-blocks p-5">
				
            <h1 class="account-header-top mb-4">My Sharing Offers</h1>
				
            <div class="tab-content">

                <div id="reviews-by-you" role="tabpanel" >

                    <div class="row">

                        <div class="col-12">

                            <div class="account-property m-b-30">


                                <!--SHARING REQUEST START-->

                                <div class="agent-block-wrapper mb-4">

                                    <div class="agent-block-agent-img">

                                        <div class="image-wrap">

                                            <div class="agent-status" title="online">

                                            </div>

                                            <a href="#chatorsomething">

                                                <img src="./images/userpicbig.png" alt="agent-photo">

                                            </a>

                                        </div>

                                    </div>

                                    <div class="agent-block-agent-information">

                                        <div class="row">

                                            <div class="block-left col-3">

                                                <h2 class="text-emperor font-400" >Ski Boots</h2>

                                            </div>

                                            <div class="block-right col-9 mb-4">

                                                

                                            </div>

                                        </div>

                                        <div class="row mb-4">

                                            <div class="block-left col-3">

                                                <div class="stars" data-stars="3">

                                                    <i class="material-icons star">star</i>

                                                    <i class="material-icons star">star</i>

                                                    <i class="material-icons star">star</i>

                                                    <i class="material-icons star">star</i>

                                                    <i class="material-icons star">star</i>

                                                </div>

                                            </div>

                                            <div class="block-right col-6">

                                                <div class="text-container">

                                                   Nulla historic ea est, eu Carrara marble volutpat sit. Vim amenities necessitatibus id. Id eos breathtaking views intellegat, id dicant stainless steel constituto pro.

                                                </div>

                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="col-xl-3 col-lg-3 col-md-6 col-xs-6 col-6 mb-md-5 mb-sm-5 mb-5">

                                                <div class="font-700 text-emperor">

                                                    July 08, 2018

                                                </div>

                                            </div>

                                            <div class="col-xl-3 col-lg-3 col-md-6 col-xs-6 col-6 mb-xl-0 mb-lg-0 mb-md-5 mb-sm-5 mb-5">

                                                <div class="font-700 text-emperor">

                                                    <span>Sharing date(s):</span>

                                                    <br>

                                                    <span>12.08.2018 - 15.08.2018</span>

                                                </div>

                                            </div>

                                            <div class="col-xl-6 col-lg-6 col-md-12 col-xs-12 col-12 text-right">


                                                

                                                <div class="button-wrapper btnwrspe d-inline mr-4">

                                                    <a href="single-offer.php" target="_blank">VIEW</a>

                                                </div>

                                                <div class="d-block m-4">

                                                    <span class="font-400 text-uppercase" style="font-size:11px;" >Or</span> <a href="#" class="font-400 text-cancelled text-uppercase" style="font-size:11px;" data-toggle="modal" data-target="#cancel-borrowing-request" >Cancel this sharing offer</a>



                                                </div>


                                            </div>

                                        </div>

                                    </div>

                                </div>

                                <!--SHARING REQUEST START-->

                                <div class="agent-block-wrapper mb-4">

                                    <div class="agent-block-agent-img">

                                        <div class="image-wrap">

                                            <div class="agent-status" title="online">

                                            </div>

                                            <a href="#chatorsomething">

                                                <img src="./images/userpicbig.png" alt="agent-photo">

                                            </a>

                                        </div>

                                    </div>

                                    <div class="agent-block-agent-information">

                                        <div class="row">

                                            <div class="block-left col-3">

                                                <h2 class="text-emperor font-400" >Ski Boots</h2>

                                            </div>

                                            <div class="block-right col-9 mb-4">

                                                

                                            </div>

                                        </div>

                                        <div class="row mb-4">

                                            <div class="block-left col-3">

                                                <div class="stars" data-stars="3">

                                                    <i class="material-icons star">star</i>

                                                    <i class="material-icons star">star</i>

                                                    <i class="material-icons star">star</i>

                                                    <i class="material-icons star">star</i>

                                                    <i class="material-icons star">star</i>

                                                </div>

                                            </div>

                                            <div class="block-right col-6">

                                                <div class="text-container">

                                                   Nulla historic ea est, eu Carrara marble volutpat sit. Vim amenities necessitatibus id. Id eos breathtaking views intellegat, id dicant stainless steel constituto pro.

                                                </div>

                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="col-xl-3 col-lg-3 col-md-6 col-xs-6 col-6 mb-md-5 mb-sm-5 mb-5">

                                                <div class="font-700 text-emperor">

                                                    July 08, 2018

                                                </div>

                                            </div>

                                            <div class="col-xl-3 col-lg-3 col-md-6 col-xs-6 col-6 mb-xl-0 mb-lg-0 mb-md-5 mb-sm-5 mb-5">

                                                <div class="font-700 text-emperor">

                                                    <span>Sharing date(s):</span>

                                                    <br>

                                                    <span>12.08.2018 - 15.08.2018</span>

                                                </div>

                                            </div>

                                            <div class="col-xl-6 col-lg-6 col-md-12 col-xs-12 col-12 text-right">


                                                

                                                <div class="button-wrapper btnwrspe d-inline mr-4">

                                                    <a href="single-offer.php" target="_blank">VIEW</a>

                                                </div>

                                                <div class="d-block m-4">

                                                    <span class="font-400 text-uppercase" style="font-size:11px;" >Or</span> <a href="#" class="font-400 text-cancelled text-uppercase" style="font-size:11px;" data-toggle="modal" data-target="#cancel-borrowing-request" >Cancel this sharing offer</a>



                                                </div>


                                            </div>

                                        </div>

                                    </div>

                                </div>
                                
                                <!--SHARING REQUEST START-->

                                <div class="agent-block-wrapper mb-4">

                                    <div class="agent-block-agent-img">

                                        <div class="image-wrap">

                                            <div class="agent-status" title="online">

                                            </div>

                                            <a href="#chatorsomething">

                                                <img src="./images/userpicbig.png" alt="agent-photo">

                                            </a>

                                        </div>

                                    </div>

                                    <div class="agent-block-agent-information">

                                        <div class="row">

                                            <div class="block-left col-3">

                                                <h2 class="text-emperor font-400" >Ski Boots</h2>

                                            </div>

                                            <div class="block-right col-9 mb-4">

                                                

                                            </div>

                                        </div>

                                        <div class="row mb-4">

                                            <div class="block-left col-3">

                                                <div class="stars" data-stars="3">

                                                    <i class="material-icons star">star</i>

                                                    <i class="material-icons star">star</i>

                                                    <i class="material-icons star">star</i>

                                                    <i class="material-icons star">star</i>

                                                    <i class="material-icons star">star</i>

                                                </div>

                                            </div>

                                            <div class="block-right col-6">

                                                <div class="text-container">

                                                   Nulla historic ea est, eu Carrara marble volutpat sit. Vim amenities necessitatibus id. Id eos breathtaking views intellegat, id dicant stainless steel constituto pro.

                                                </div>

                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="col-xl-3 col-lg-3 col-md-6 col-xs-6 col-6 mb-md-5 mb-sm-5 mb-5">

                                                <div class="font-700 text-emperor">

                                                    July 08, 2018

                                                </div>

                                            </div>

                                            <div class="col-xl-3 col-lg-3 col-md-6 col-xs-6 col-6 mb-xl-0 mb-lg-0 mb-md-5 mb-sm-5 mb-5">

                                                <div class="font-700 text-emperor">

                                                    <span>Sharing date(s):</span>

                                                    <br>

                                                    <span>12.08.2018 - 15.08.2018</span>

                                                </div>

                                            </div>

                                            <div class="col-xl-6 col-lg-6 col-md-12 col-xs-12 col-12 text-right">


                                                

                                                <div class="button-wrapper btnwrspe d-inline mr-4">

                                                    <a href="single-offer.php" target="_blank">VIEW</a>

                                                </div>

                                                <div class="d-block m-4">

                                                    <span class="font-400 text-uppercase" style="font-size:11px;" >Or</span> <a href="#" class="font-400 text-cancelled text-uppercase" style="font-size:11px;" data-toggle="modal" data-target="#cancel-borrowing-request" >Cancel this sharing offer</a>



                                                </div>


                                            </div>

                                        </div>

                                    </div>

                                </div>


                                <div class="pagination-wrap m-b-30">

                                    <ul>

                                        <li>

                                            <a href="#"><i class="material-icons">&#xE5C4;</i></a>

                                        </li>

                                        <li>

                                            <a href="#">1</a>

                                        </li>

                                        <li>

                                            <a href="#">2</a>

                                        </li>

                                        <li>

                                            <a href="#">3</a>

                                        </li>

                                        <li>

                                            <a href="#">4</a>

                                        </li>

                                        <li>

                                            <a href="#"><i class="material-icons">&#xE5C8;</i></a>

                                        </li>

                                    </ul>

                                </div>

                            </div>

                        </div>


                        <!--<div class="col-md-4">

                            <div class="account_properties_counters_list">

                                <a href="#" class="item">

                                    <span class="badge rented"></span> REQUESTED

                                    <span class="count">1</span>

                                </a>


                                <a href="#" class="item">

                                    <span class="badge awaiting-admin"></span> ACCEPTED

                                    <span class="count">1</span>

                                </a>


                                <a href="#" class="item">

                                    <span class="badge awaiting-admin-b"></span> DENIED

                                    <span class="count">1</span>

                                </a>


                                <a href="#" class="item">

                                    <span class="badge cancelled"></span> CANCELLED

                                    <span class="count">1</span>

                                </a>

                            </div>

                        </div>-->

                    </div>

                </div>

            </div>
			

        </div>
        
    </div>
    
</div>

<!--MODALS-->
<div class="modal fade" id="cancel-borrowing-request" role="dialog" data-backdrop="static">
        
    <div class="modal-dialog modal-lg">
        
        <div class="modal-content bg-light-grey">
            
            <div class="modal-header d-flex justify-content-start">
            
                <h4 class="modal-title">Cancel sharing offer</h4> 
            
            
            </div>
            
            <div class="modal-body">
                
                <p>You are about to cancel offering <a href="single-offer.php" target="_blank"> Ski boots</a>. If you are sure you want to cancel this click on "Confirm Cancellation."</p>
                
            </div>
            
            <div class="modal-footer">
                
                <button type="button" class="btn btn-default btnwb bg-transparent" data-dismiss="modal">Go Back</button>
                &nbsp;&nbsp;                
               
                <button type="button" class="btn btn-secondary bg-orange text-white text-uppercase font-700 font-open-sans px-5 py-3" data-dismiss="modal"  data-toggle="modal" data-target="#confirm-borrowing-cancel">Confirm cancellation</button>
            
            </div>
        
        </div>
        
    </div>
    
</div>
    
<!-- CONFIRM BORROWING CANCELLATION -->

<div class="modal fade" id="confirm-borrowing-cancel" role="dialog" data-backdrop="static">
        
    <div class="modal-dialog modal-lg">
        
        <div class="modal-content">
            
            <div class="modal-header justify-content-start">
            
                <h4 class="modal-title">Sharing offer cancelled!</h4>
            
            </div>
            
            <div class="modal-body">
            
                <p class="fz-12-">[Missing copy]</p>
            
            </div>
            
            <div class="modal-footer">
                
                <button type="button" class="btn btn-secondary bg-orange text-white text-uppercase font-700 font-open-sans px-5 py-3" data-dismiss="modal">Close this</button>
            
            </div>
        
        </div>
        
    </div>
    
</div>


<?php include 'scripts.php'; ?>


</body>

</html>