<?php include 'header.php'; ?>
<body>


    
<div class="page-title-simple m-b-30">
    <div class="container">
        <h1 class="text-emperor">Send Request</h1>
    </div>
</div>

<div class="container">
    
    <div class="row">
        
        <div class="col-lg-12">
            
            <nav aria-label="breadcrumb">
  
                <ol class="breadcrumb">
    
                    <li class="breadcrumb-item"><a href="home.php" class="text-orange" >&larr; Back to Review Details</a></li>
                    
  
                </ol>

            </nav>
            
        </div>
        
    </div>
    
</div>


<div class="container">
    <div class="row bor-btm-2">
        <div class="col-lg-8 col-md-7">
            <div class="single-property-description m-b-30">
                <div class="sidebar-agent bg-white border-0">
                    <div class="agent-info-wrap p-0 bg-white border-0">
                        <div class="row">
                            <div class="col-2">
                                <div class="agent-img online">
                                    <img src="./images/mask.png" alt="agent-img">
                                </div>
                            </div>
                            <div class="col-10">
                                <div class="agent-name mb-3 text-emperor">
                                    Fraces Modalu
                                </div>
                                <ul class="list-inline">
                                    <li class="list-inline-item text-orange mr-5">
                                        <h5><i class="fa fa-globe" aria-hidden="true"></i> Canberra, Australia</h5>
                                    </li>
                                    <li class="list-inline-item text-orange mr-5">
                                        <h5><i class="fa fa-mobile" aria-hidden="true"></i> 686-986-7249</h5>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="single-property-details mb-5">
                <h4 class="text-emperor font-700 mb-5">Things to keep in mind</h3>
                <span>When you click on "Agree and Continue" you agree to Luviat T&Cs etc. Overview of T&Cs with link.</span>
            </div>            
        </div>
        <div class="col-lg-4 col-md-5">
            <div class="mb-5 m-0 border-0">
                <div class="border bg-grey p-5">
                    <div class="row">
                        <div class="col-md-12 pd-bt-4">
                            <span class="font-400 mb-4 fs-18">Ski Boots <small class="pd-l5"> <a href="#">view item</a></small></span>
                        </div>
                        <div class="col-md-12">
                            <ul class="list-inline">
                                <li class="list-inline-item text-orange">
                                    <span class="fs-18"><i class="fa fa-globe" aria-hidden="true"></i> Canberra, Australia</span>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-12">
                            <ul class="wp">
                                <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li class="list-inline-item text-emperor m-0 p-0"><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                <li class="list-inline-item text-emperor m-0 p-0"><i class="fa fa-star-o" aria-hidden="true"></i></li>
                            </ul>
                        </div>
                        <div class="col-md-12 bor-btm"></div>
                        <div class="pad-top">
                            <div class="col-md-6">
                                <div class="mb-4">
                                    <label class="font-open-sans font-400 text-emperor bg-transparent">Borrow Date</label>
                                    <div class="clearfix"></div>
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1">
                                            <i class="far fa-calendar-alt"></i>
                                        </span>
                                        <input type="text" class="form-control w-100 datepick" aria-describedby="basic-addon1">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-4">
                                    <label class="font-open-sans font-400 text-emperor bg-transparent">Return Date</label>
                                    <div class="clearfix"></div>
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1">
                                            <i class="far fa-calendar-alt"></i>
                                        </span>
                                        <input type="text" class="form-control w-100 datepick" aria-describedby="basic-addon1">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="mb-4">
                                <div class="row">
                                    <div class="col-6">
                                        <input type="checkbox" name="Pickup" id="Pickup" class="css-checkbox" checked>
                                        <label for="Pickup" class="css-label">Pickup</label>
                                    </div>
                                    <div class="col-6">
                                        <input type="checkbox" name="Delivery" id="Delivery" class="css-checkbox">
                                        <label for="Delivery" class="css-label">Delivery</label>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-0 border-top">
                                <div class="row">
                                    <div class="col-6 text-left">
                                        <p>Delivery Fee</p>
                                    </div>
                                    <div class="col-6 text-right">
                                        <p>$6</p>
                                    </div>
                                </div>
                            </div>
                            <div class="border-top">
                                <div class="row">
                                    <div class="col-6 text-left">
                                        <p class="font-weight-bold font-700" >Total</p>
                                    </div>
                                    <div class="col-6 text-right">
                                        <p>$21</p>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="pt-3 mb-5 border-top">
                                <p class="colorgray"><strong>Note: Luviat does not manage payment.</strong></p>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="mb-4">
                <button type="submit" class="btn btn-secondary bg-orange text-white text-uppercase font-700 font-open-sans px-5 py-4 w-100" style="font-size:13px;">AGREE AND CONTINUE</button>
            </div>
        </div>
    </div>
</div>
<!--PROPERTY SLIDER END-->
<div class="container js-home-1-search m-b-30">

    <!--PROPERTY SEARCH FORM 1 BEGIN-->

    <form id="search-form-1" class="mt-5">
        
        <div class="property-search">
            
            <div class="main-apartment-search-options bg-orange">
                
                <h3 class="text-white mb-4">Not what you are looking for? Try another search</h3>
                
                <div class="options-wrapper-main">
                    
                     <div class="wrapper search-property">
                            <input type="search" class="property-searchinput" placeholder="Area of interest">
                        </div>

                        <div class="wrapper">

                            <select form="search-form-1" name="location" class="location-select big-input"
                                    data-placeholder="location">
                                <option value="london">London</option>
                                <option value="miami">Miami</option>
                                <option value="new-york">New-York</option>
                                <option value="houston">Houston</option>

                            </select>
                        </div>

                        <div class="wrapper search-property">
                            <input type="search" class="property-searchinput" placeholder="Keyword">
                        </div>

                        <div class="wrapper">

                            <div class="range-picker-wrapper position-relative" >
                                <i class="fa fa-calendar text-orange" aria-hidden="true"></i>
                                <input type="text" class="dates-input pl-5 datepick" placeholder="Dates">
                            </div>
                        </div>

                    <div class="wrapper advanced-wrapper">
                        
                        <!--<h3 class="text-white">Advanced mode</h3>
                        
                        <label class="switch">
                            
                            <input type="checkbox">
                            
                            <span class="slider round"></span>
                            
                        </label>-->
                        
                        <button class="button-secondary w-100 ">Search</button>
                    </div>
                </div>

            </div>
           
        </div>
        
    </form>

    <!--PROPERTY SEARCH FORM 1 END-->
</div>


<?php include 'footer.php' ?>