<?php include 'header.php'; ?>

<div class="account-page">
    
    <?php include 'user-sidebar.php' ?>
    
    <div class="account-page-wild_block">
        
        <div id="sharing-requests-view" class="js-content-blocks p-5 w-100">
            
            <h1 class="account-header-top mb-4">Sharing requests</h1>
            
            <div class="row">
            
                <div class="col-lg-12">
            
                    <nav aria-label="breadcrumb">
  
                        <ol class="breadcrumb">
                    
                            <li class="breadcrumb-item">
                                <a href="sharing-requests.php" onclick="window.history.back()" class="text-orange">Back to all sharing requests</a>
                            </li>
                    
                            <li class="breadcrumb-item active" aria-current="page">Sharing Request</li>
                
                        </ol>
            
                    </nav>
            
                </div>
    
            </div>				
            
            <div class="row">
                
                <div class="col-12">
                    
                    <div class="property-block">
                                    
                        <div class="row">
                                        <dir class="col-12 col-sm-5">
                                            <img src="./images/image.png">
                                        </dir>
                                        <dir class="col-12 col-sm-7">
                                            <div class="requested-name">Frances</div>
                                            <div class="stars" data-stars="3">
                                                <i class="material-icons star">star</i>
                                                <i class="material-icons star">star</i>
                                                <i class="material-icons star">star</i>
                                                <i class="material-icons star">star</i>
                                                <i class="material-icons star">star</i>
                                            </div>
                                            <span>
                                                July, 08, 2018
                                            </span>
                                        </dir>
                                    </div>
                                    
                        <div class="apartment-values">
                                        <span class="orange-text"><i class="material-icons orange-text">email</i> email@gmail.com</span>
                                        <br>
                                        <span class="orange-text"><i class="material-icons orange-text">phone</i> 0403 201 301</span>
                                    </div>
                                    
                        <div class="data-box">
                                        <div class="title">                                        
                                            Requesting to borrow you:
                                        </div>
                                        <strong>Ski boots</strong>
                                        view item
                                        <div class="stars" data-stars="3">
                                            <i class="material-icons star">star</i>
                                            <i class="material-icons star">star</i>
                                            <i class="material-icons star">star</i>
                                            <i class="material-icons star">star</i>
                                            <i class="material-icons star">star</i>
                                        </div>
                                    </div>
                            
                        <div class="apartment-info">
                                        <div class="apartment-price">
                                            <p class="price-big">$ 24</p>
                                            <p class="price-small">week $67</p>
                                        </div>
                                        <div class="icons">
                                            <a href="#" class="clone"> <i class="material-icons icons-style"></i></a>
                                            <a href="#" class="heart"><i class="material-icons icons-style"></i></a>
                                        </div>
                                    </div>

                    </div>
                    
                </div>
                
                
                
            </div>
            
        </div>
        
    </div>
    
</div>

<?php include 'scripts.php'; ?>

</body>

</html>