<?php /*<?php include 'header.php'; ?> */?>

<?php include 'header-single-for-approval.php'; ?>

<div class="page-title-simple m-b-30">
    <div class="container">
        <h1 class="text-emperor">Hiking Boots</h1>
    </div>
</div>

<div class="container">
    
    <div class="row">
        
        <div class="col-lg-12">
            
            <nav aria-label="breadcrumb">
  
                <ol class="breadcrumb">
    
                    <li class="breadcrumb-item"><a href="#" onclick="window.history.back()" >Back</a></li>
    
                    <li class="breadcrumb-item active" aria-current="page">Hiking Boots</li>
  
                </ol>

            </nav>
            
        </div>
        
    </div>
    
</div>

<div class="container">
    
    <div class="row">
        
        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
            
            <div class="single-property-description mb-4">
                
                
                <div class="row">
                    
                    <div class="col-6">
                        
                        <ul class="list-inline m-0">
                    
                            <li class="list-inline-item text-orange m-0">
                        
                                <h3 class="m-0"><i class="fa fa-globe" aria-hidden="true"></i> Canberra, Australia</h3>
                    
                            </li>
                
                        </ul>
                        
                    </div>
                    
                    <div class="col-6">
                        
                        <ul class="list-inline m-0">
                            
                            <li class="list-inline-item text-emperor m-0">
                        
                                <h3 class="m-0 text-emperor"><i class="fa fa-calendar-check-o" aria-hidden="true"></i> Available Now</h3>
                        
                    
                            </li>
                
                        </ul>
                        
                    </div>
                    
                </div>
                
            </div>
            
        </div>
            
        <div class="col-xl-8 col-lg-8 col-md-7 col-sm-12 col-12">
            
            <!-- LOCATION -->
            <div class="single-property-description m-b-30">
                
                <h3 class="text-emperor font-700">Categories</h3>
                
                <ul class="list-inline m-0">
                            
                    <li class="list-inline-item m-0 border text-orange font-weight-bold p-4 mr-3">
                        Hiking
                            
                    </li>
                    
                    <li class="list-inline-item m-0 border text-orange font-weight-bold p-4 mr-3">
                        Fishing
                            
                    </li>
                    
                    <li class="list-inline-item m-0 border text-orange font-weight-bold p-4 mr-3">
                        Skiing
                            
                    </li>
                        
                </ul>
                
            </div>
            
            <!-- SHORT DESCRIPTION -->
            <div class="single-property-description m-b-30">
                
                <h3 class="text-emperor font-700">Short description</h3>
                
                <p>Nulla historic ea est, eu Carrara marble volutpat sit. Vim amenities necessitatibus id. Id eos breathtaking views intellegat, id dicant stainless steel constituto pro. </p>
                
            </div>
            
            <!-- OWNER -->
            <div class="single-property-description m-b-30">
                
                <div class="sidebar-agent bg-white border-0">
                    
                    <div class="agent-info-wrap p-0 bg-white border-0">
                        
                        <div class="row">
                            
                            <div class="col-xl-2 col-lg-2 col-md-4 col-sm-4 col-4">
                                
                                <div class="agent-img">
                                    
                                    <img src="./images/mask.png" alt="agent-img">
                                </div>
                                
                            </div>
                            
                            <div class="col-xl-10 col-lg-10 col-md-8 col-sm-8 col-8 ">
                                
                                <div class="agent-name mb-3 text-emperor text-left">
                                    Pauline Saunders
                                </div>
                                
                                <ul class="list-inline">
                                    
                                    <li class="list-inline-item text-orange mr-5">
                                        
                                        <h5><i class="fa fa-globe" aria-hidden="true"></i> Canberra, Australia</h5>
                                        
                                    </li>
                                    
                                    
                                    <li class="list-inline-item mr-5" >
                                        
                                        <ul class="list-inline">

                                            <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                            
                                            <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                            
                                            <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>

                                            <li class="list-inline-item text-emperor m-0 p-0"><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                            
                                            <li class="list-inline-item text-emperor m-0 p-0"><i class="fa fa-star-o" aria-hidden="true"></i></li>

                                        </ul>
                                        
                                    </li>
                                    
                                </ul>
                                
                            </div>
                            
                        </div>
                        
                    </div>
                    
                </div>
                
            </div>

            <!-- GALLERY -->
            <div class="property_gallery mb-5">
    
                <div class="wrapper">
        
                    <div class="slide" data-height="450">
            <div class="js-image-preloader">
                <div>
                    <canvas id='imagecircleback' height='100' width='100'></canvas>
                    <canvas id='imagecirclemain' height='100' width='100'></canvas>

                </div>
            </div>
            <p class="js-fullscreen-message">Press <kbd>ESC</kbd> to exit full screen mode </p>
        </div>
        
                    <a href="#" class="js-fullscreen-btn fullscreen"> <i class="material-icons"></i></a>
        
                    <div class="property-slides-list-wrapper">
            <div class="owl-carousel owl-theme">
                <div class="item">
                    <a href="#" data-image='./images/property_slider_first_slide.png'>
                        <img src="./images/property_slider_first_slide.png" alt="active-slide">
                    </a>
                </div>
                <div class="item">
                    <a href="#" data-image='./images/mask.png'>
                        <img src="./images/property_slider_first_slide.png" alt="active-slide">
                    </a>
                </div>
                <div class="item">
                    <a href="#" data-image='./images/mask.png'>
                        <img src="./images/property_slider_first_slide.png" alt="active-slide">
                    </a>
                </div>
                <div class="item">
                    <a href="#" data-image='./images/mask.png'>
                        <img src="./images/property_slider_first_slide.png" alt="active-slide">
                    </a>
                </div>
                <div class="item">
                    <a href="#">
                        <img src="./images/property_slider_first_slide.png" alt="active-slide">
                    </a>
                </div>
                <div class="item">
                    <a href="#">
                        <img src="./images/property_slider_first_slide.png" alt="active-slide">
                    </a>
                </div>

            </div>
        </div>
        
                    <a href="#" class="nav-arrow left-arrow"><i class="material-icons">arrow_back</i></a>
        
                    <a href="#" class="nav-arrow right-arrow"><i class="material-icons">arrow_forward</i></a>
    
                </div>

            </div>
            
            <!-- OVERVIEW -->
            <div class="single-property-details mb-5">
                <div class="main-info">
                    <h3 class="text-emperor font-700 d-inline">Overview</h3>
                    <span class="update d-inline">Updated on February 11, 2017 at 1:07 pm</span>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-4">
                        <div class="wrap">
                            <span>Area of interest:</span>Hiking
                        </div>
                        <div class="wrap">
                            <span>Pickup/Delivery:</span>Both
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-4">
                        <div class="wrap">
                            <span>Published:</span>3 months ago
                        </div>
                        <div class="wrap">
                            <span>Distance willing to deliver:</span>12km
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-4">
                        <div class="wrap">
                            <span>Updated:</span>20 hours ago
                        </div>
                        <div class="wrap">
                            <span>Available:</span>19/03/2018
                        </div>
                    </div>
                </div>
            </div>  

            <!-- DESCRIPTION -->
            <div class="single-property-details mb-5">
                
                <h3 class="text-emperor font-700 mb-5">Description</h3>
                
                <p>Nulla historic ea est, eu Carrara marble volutpat sit. Vim amenities necessitatibus id. Id eos breathtaking views intellegat, id dicant stainless steel constituto pro. </p>
                
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                
            </div>            
            
            <!-- SHORT DESCRIPTION -->
            <div class="single-property-description m-b-30">
                
                <h3 class="text-emperor font-700"><i class="fa fa-calendar-check-o text-orange" aria-hidden="true"></i>  Availability</h3>
                
                <ul class="p-0 m-0">
                    <li>· Fridays from 4pm-5pm</li>
                    <li>· Saturdays from 4pm-5pm</li>
                    <li>· Monday - Wednesday all day</li>
                </ul>
                
            </div>
            
            
            <!-- Acreditations -->
            <div class="single-property-description m-b-30">
                
                <h3 class="text-emperor font-700"><i class="fa fa-exclamation-triangle text-orange" aria-hidden="true"></i>  Required licenses or accreditations for use</h3>
                
                <p>Nulla historic ea est, eu Carrara marble volutpat sit. Vim amenities necessitatibus id. Id eos breathtaking views intellegat, id dicant stainless steel constituto pro. </p>
                
            </div>
            
        </div>
        
        <div id="sidebar-single" class="col-xl-4 col-lg-4 col-md-5 col-sm-12 col-12">
            
            <div class="d-block">
                
                <!--PRICE-->
                <div class="mb-3 border-0">
                
                    <div class="border bg-grey px-5 py-4">

                        <h3 class="font-700 text-emperor mb-5">Price</h3>

                        <div class="w-100 pb-4 mb-5">

                        <div class="float-left mr-5 mb-2">

                            <h4 class="font-400">$4 AUD / <small>Hour</small></h4>

                        </div>

                        <div class="float-left mb-2">

                            <h4 class="text-align-last-auto">$8 AUD / <small>Day</small></h4>

                        </div>
                        
                    </div>
                    
                        <div class="w-100">
                        
                        <input type="checkbox" name="daily" id="daily" class="css-checkbox" checked disabled>                
                        <label for="daily" class="css-label">By Negotiation</label>
                        
                    </div>
                
                    </div>
                
                </div>
                
                <!-- DETAILS -->
                <div class="mb-3 border-0">
                
                    <div class="border bg-grey px-5 py-4">
                    
                    <div class="row mt-4">
                        
                        <div class="col-12">
                            
                            <h4 class="font-400 mb-4">$21 AUD / <small>Hour</small></h4>
                            
                        </div>
                             
                            
                        <div class="col-lg-6">

                            <div class="mb-4">

                                <label class="font-open-sans font-400 text-emperor bg-transparent">Borrow Date</label>

                                <div class="clearfix"></div>

                                <input type="text" class="w-100" value="12-07-2018" disabled>

                            </div>

                        </div>

                        <div class="col-lg-6 mb-4">

                            <div class="mb-4">

                                <label class="font-open-sans font-400 text-emperor bg-transparent">Return Date</label>

                                <div class="clearfix"></div>

                                <input type="text" class="w-100" value="14-07-2018" disabled>

                            </div>

                        </div>
                        
                        <div class="col-lg-12 mb-4">

                            <div class="mb-5">

                                <div class="row">
                                    
                                    <div class="col-6">
                                        
                                        <input type="checkbox" name="Pickup" id="Pickup" class="css-checkbox" checked>
                                        
                                        <label for="Pickup" class="css-label">Pickup</label>
                                    </div>
                                    
                                    <div class="col-6">
                                        
                                        <input type="checkbox" name="Delivery" id="Delivery" class="css-checkbox">
                                        
                                        <label for="Delivery" class="css-label">Delivery</label>
                                    </div>
                                    
                                </div>

                            </div>
                            
                            <div class="mb-0 border-top">

                                <div class="row">
                                    
                                    <div class="col-6 text-left">
                                        
                                        <p>Delivery Fee</p>
                                        
                                    </div>
                                    
                                    <div class="col-6 text-right">
                                        
                                        <p>$6</p>
                                        
                                    </div>
                                    
                                </div>

                            </div>
                            
                            <div class="border-top mb-4">

                                <div class="row">
                                    
                                    <div class="col-6 text-left">
                                        
                                        <p class="font-weight-bold font-700" >Total</p>
                                        
                                    </div>
                                    
                                    <div class="col-6 text-right">
                                        
                                        <p>$21</p>
                                        
                                    </div>
                                    
                                </div>

                            </div>
                            
                            <div class="mb-4">
                                
                                <button type="submit" class="btn btn-secondary bg-orange text-white text-uppercase font-700 font-open-sans px-5 py-4 w-100" style="font-size:13px;">Request to book</button>
                                
                            </div>

                        </div>
                        
                    </div>
                    
                </div>
            
                </div>
                
                <!-- ACCEPTED PAYMENT METHODS -->
                <div class="mb-3 m-0 border-0">
                
                    <div class="border bg-grey pt-4 pr-5 pb-0 pl-5">

                    
                        <h4 class="font-700 text-emperor mb-2">Accepted Payment Methods</h4>
                        <div class="w-100">
                            
                            <ul class="m-0 p-0" >
                                <li>
                                    <input type="checkbox" name="cash" id="cash" class="css-checkbox" checked disabled ><label for="cash" class="css-label">By Cash</label>
                                </li>
                                
                                <li>
                                    
                                    <input type="checkbox" name="paypal" id="paypal" class="css-checkbox" checked disabled><label for="paypal" class="css-label">Paypal</label>
                                    
                                </li>
                                
                                <li>
                                    
                                    <input type="checkbox" name="debit" id="debit" class="css-checkbox" checked disabled><label for="debit" class="css-label">Direct Debit</label>
                                    
                                </li>
                                
                                <li>    
                                    
                                    <p class="colorgray"><strong>Note: Luviat does not manage payment.</strong></p>
                                    
                                </li>
                                
                            </ul>
                            
                        </div>
                
                    </div>
                
                </div>
                
            
            </div>
            
        </div>
        
    </div>
    
</div>


<?php include 'footer.php' ?>