<?php include 'header.php'; ?>

    <div class="modal fade" id="updatemyModal" role="dialog">
        <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title">You are about to approve a sharing request.</h4>
            <button type="button" class="btn btn-specials-red"  data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i></button>
            </div>
            <div class="modal-body">
            <p class="fz-12-">Once you approve this sharing requestthe borrower will receive an email notifying them their request has been approved. Your email and contact number will also be shared with them.
            </p>
            <p class="fz-12-">No payment will be made through the luviat platform and will need to be arranged externally.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btnwb" data-dismiss="modal">BACK</button>
                <button type="button" class="btn btn-warning" data-dismiss="modal"  data-toggle="modal" data-target="#sharingaccepted">APPROVE REQUEST</button>
            </div>
        </div>

        </div>
    </div>

    <div class="modal fade" id="sharingaccepted" role="dialog">
        <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title">Sharing request accepted!</h4>
            <button type="button" class="btn btn-specials-red"  data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i></button>
            </div>
            <div class="modal-body">
            <p class="fz-12-">Thank you for accepting the sharing request! Not only are you keeping Luviat alive but you are helping someone have an unforgettable experience om a new city.
            </p>
            <p class="fz-12-">They have received your contact information you have provided to us. You can either contact them first or wait until they contact you.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btnwb" data-dismiss="modal">BACK</button>
            </div>
        </div>

        </div>
    </div>

    <div class="modal fade" id="denysharing" role="dialog">
        <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title">Deny sharing request and send message via email</h4>
            <button type="button" class="btn btn-specials-red"  data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i></button>
            </div>
            <div class="modal-body">
                <textarea id="editor2" placeholder="Lorem ipsum dolor sit amet, consectetur adipsiicis, sed do eisuaid labore et dolore magna aliqua. Ut aneim adms imnir pevnit ventinima laboris ullamanco laboris nisi ut aliquimp ex ae comodo cosequat." ></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal"  data-toggle="modal" data-target="#sharingdenied">DENY OFFER AND SEND</button>
            </div>
        </div>

        </div>
    </div>

    <div class="modal fade" id="sharingdenied" role="dialog">
        <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title">Sharing request denied!</h4>
            <button type="button" class="btn btn-specials-red"  data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i></button>
            </div>
            <div class="modal-body">
            <p class="fz-12-">Thanks for responding to this sharing request. The offer will still be available on Luviat to share unless you change it's status to unavailble. Tou can do this from the My sharing offers tab in your traveller dashboard.
            </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btnwb" data-dismiss="modal">BACK</button>
            </div>
        </div>

        </div>
    </div>

	<div class="account-page">
        
		<div class="account_menu account_menu__normal">
			
            <a href="#" class="js-toggle-menu">
				<div class="account_menu_agent">
					<div class="agent-avatar">
						<img src="./images/userpic.png" alt="user">
						<div class="agent-status status-online"></div>
					</div>
					<h2>Eunice Rios</h2>
				</div>
			</a>
            
            
            
			<div class="account_menu_categories">
                
				<a href="#dashboard" class="js-menu-link" data-display-id="dashboard">
					<div class="category_item">
						<div class="category_item__category-name">
							<i class="material-icons">insert_chart</i>
							
							<span>Dashboard</span>
						</div>
					</div>
				</a>
                
				<a href="#submit" class="js-menu-link" data-display-id="submit">
					<div class="category_item">
						<div class="category_item__category-name">
							<i class="material-icons">add_circle</i>
							
							<span>Submit a sharing offer</span>
						</div>
					</div>
				</a>
                
				<a href="#property" class="js-menu-link new-notification__enabled" data-display-id="property">
					<div class="category_item">
						<div class="category_item__category-name">
							<i class="material-icons">view_module</i>
							
							<span>My sharing offers</span>
						</div>
						<span class="category_item__item-counts">
							34
						</span>
					</div>
				</a>
                
				<a href="#favorites" class="js-menu-link" data-display-id="favorites">
					<div class="category_item">
						<div class="category_item__category-name">
							<i class="material-icons">favorite</i>
							
							<span>Watchlist</span>
						</div>
						<span class="category_item__item-counts">
							3
						</span>
					</div>
				</a>
                
				<a href="#messages" class="js-menu-link new-notification__enabled" data-display-id="messages">
					<div class="category_item">
						<div class="category_item__category-name">
							<i class="material-icons">comment</i>
							
							<span>Sharing requests</span>
						</div>
					</div>
				</a>
                
				<a href="#messages" class="js-menu-link new-notification__enabled" data-display-id="messages">
					<div class="category_item">
						<div class="category_item__category-name">
							<i class="material-icons">star</i>
							
							<span>Reviews</span>
						</div>
					</div>
				</a>
                
				<a href="#messages" class="js-menu-link new-notification__enabled" data-display-id="messages">
					<div class="category_item">
						<div class="category_item__category-name">
							<i class="material-icons">person</i>
							
							<span>Your personal information</span>
						</div>
					</div>
				</a>
                
				<a href="#settings" class="js-menu-link" data-display-id="settings">
					<div class="category_item">
						<div class="category_item__category-name">
							<i class="material-icons">settings</i>
							
							<span>Settings</span>
						</div>
					</div>
				</a>
                
                
			</div>
            
		</div>
        
		<div class="account-page-wild_block">
			<div id="dashboard" class="js-content-blocks">
				<h1 class="account-header-top">Your traveller persona</h1>
                
                
                <div class="alert bg-grey alert-dismissible p-5 position-relative" role="alert">
                                
                                
                   <h4 class="text-emperor font-400">Welcome to your personal information page</h4>
                    
                    
                                
                    <br>
                                
                    <p>Luviat is about sharing and community. Help other people get to know what you’re about. In this section, you can build a profile with details such as your travel history and the languages you speak. It will describe you as a traveller and sharer and, will help people feel like they know you.</p>
                                
                                
                    <button type="button" class="close bg-orange position-absolute " data-dismiss="alert" aria-label="Close">
                                
                                    
                        <span aria-hidden="true">&times;</span>
                                
                                
                    </button>
                            
                            
                </div>
                
				
                <div class="account-header-tabs mb-5 border-bottom">
					<ul class="nav" id="myTab" role="tablist">
						<li class="nav-item">
							<a class="nav-link" id="about-you-tab" data-toggle="tab" href="#about-you" role="tab" aria-controls="about-you" aria-selected="true">About you</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="travel-history-tab" data-toggle="tab" href="#travel-history" role="tab" aria-controls="travel-history" aria-selected="false">Travel history</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="langua-tab" data-toggle="tab" href="#langua" role="tab" aria-controls="langua" aria-selected="false">Languages</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="community-tab" data-toggle="tab" href="#community" role="tab" aria-controls="community" aria-selected="false">Community</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="licensesqual-tab" data-toggle="tab" href="#licensesqual" role="tab" aria-controls="licensesqual" aria-selected="false">Licenses & Qualifications</a>
						</li>
					</ul>
				</div>
				
                <div class="tab-content">
					
                    <div class="tab-pane" id="reviews-by-you" role="tabpanel" aria-labelledby="about-you">
						<div class="row">
							<div class="col-12">
								<div class="account-property m-b-30">
									
									<div class="m-b-30">
										<!--AGENT BLOCK BEGIN-->
										<div class="agent-block-wrapper-header">
											Rewrites to write
										</div>
										<!--AGENT BLOCK END-->
										<div class="agent-block-wrapper">
											<div class="agent-block-agent-img">
												<div class="image-wrap">
													<!--agent-status & status-online styles in style.less-->
													<div class="agent-status" title="online">
													</div>
													<a href="#chatorsomething">
														<img src="./images/userpicbig.png" alt="agent-photo">
													</a>
												</div>
											</div>
											<div class="agent-block-agent-information">
												<div class="row">
													<div class="block-left col-3">
														<h2>Pauline Saunders</h2>
													</div>
													<div class="block-right col-9">
														<span class="small-text orange-text">Ski Boots size 10</span>
													</div>
												</div>
												<div class="row">
													<div class="block-left col-3">
														<div class="stars" data-stars="3">
															<i class="material-icons star">star</i>
															<i class="material-icons star">star</i>
															<i class="material-icons star">star</i>
															<i class="material-icons star">star</i>
															<i class="material-icons star">star</i>
														</div>
													</div>
													<div class="block-right col-9">
														<div class="text-container">
															<div class="write-review">
																<div class="review"></div>
																<div class="review"></div>
																<div class="review"></div>
																<div class="review"></div>
															</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="block-left col-3">
														<div class="small-text align-bottom">
															July 08, 2018
														</div>
													</div>
													<div class="block-right col-9">
														<div class="small-text">
															borrow date(s): 12.08.2018 - 15.08.2018
														</div>
													</div>
												</div>
												<div class="send-button">
													<a href="#" data-toggle="modal" data-target="#write-review-modal" class="button button-link-primary-icon" >write review</a>
													
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
                <div class="tab-pane" id="travel-history" role="tabpanel" aria-labelledby="travel-history-tab">
					<div class="row">
                        <div class="account-property m-b-30 pdt20 col-md-10">
                            <div class="m-b-30 bg-grey">
                                
                                <div class="agent-block-wrapper-header bg-grey p-4">
                                    
                                    <h4 class="font-700" >Travel history</h4>
                                    
                                </div>
                                
                                <!--AGENT BLOCK BEGIN-->
                                <div class="col-md-12 bg-grey specialcont">
                                    <div class="col-md-12">
                                        <div class="pdb4"><small >I am currently:</small></div>
                                        <div class="row pdb4">
                                            <div class="col-4">
                                                <input type="checkbox" name="planning" id="planning" class="css-checkbox">
                                                <label for="planning" class="css-label">Planning for a comming trip.</label>
                                            </div>
                                            <div class="col-3">
                                                <input type="checkbox" name="travelling" id="travelling" class="css-checkbox">
                                                <label for="travelling" class="css-label">Travelling</label>
                                            </div>
                                            <div class="col-5">
                                                <input type="checkbox" name="connect" id="connect" class="css-checkbox">
                                                <label for="connect" class="css-label">Just looking forward to connecting and sharing with visitors to my city.</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="pdb4"><small >Places I've travelled to:</small></div>
                                    </div>
                                    <div class="col-md-12 pdb4">
                                        <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="Enter a city or country" tabindex="-1" >
                                    </div>
                                    <div class="col-md-6 pdb4">
                                        <div class="button-wrapper btnwrspe">
                                            <a href="#">ADD MORE</a>
                                        </div>
                                    </div>
                                    <div class="col-md-6 pdb4">
                                        <div class="button-wrapper btnwrspe fr">
                                            <a href="#" >REMOVE</a>
                                        </div>
                                    </div>
                                    <div class="col-md-12 ">
                                        <div class="pdb4"><small >Places I'm planning to travel to:</small></div>
                                    </div>
                                    <div class="col-md-12 pdb4">
                                        <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="Enter a city or country" tabindex="-1" >
                                    </div>
                                    <div class="col-md-6 pdb4">
                                        <div class="button-wrapper btnwrspe">
                                            <a href="#">ADD MORE</a>
                                        </div>
                                    </div>
                                    <div class="col-md-6 pdb4">
                                        <div class="button-wrapper btnwrspe fr">
                                            <a href="#" >REMOVE</a>
                                        </div>
                                    </div>
                                </div>
                                <!--AGENT BLOCK END-->
                            </div>
                                
                    </div>
                        <div class="col-md-10">
							<div class="account-property m-b-30 ">
								
								<div class="m-b-30 ">
									<div class="row no-header">
										<div class="col-12">
                                            <div class="col-md-6">
                                            </div>
                                            <div class="col-md-6">
                                                <div class="button-wrapper btnwrspe fr">
                                                    <a href="#" ><i class="fas fa-check-double"></i> UPDATE PROFILE</a>
                                                </div>
                                            </div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="licensesqual" role="tabpanel" aria-labelledby="licensesqual-tab">
					<div class="row">
                        <div class="account-property m-b-30 pdt20 col-md-10">
                            <div class="m-b-30 bg-grey">
                                
                                
                                <div class="agent-block-wrapper-header bg-grey p-4">
                                    
                                    <h4 class="font-700" >Licenses & qualifications</h4>
                                    
                                </div>
                                
                                <!--AGENT BLOCK BEGIN-->
                                <div class="specialcont col-md-12 bg-grey">
                                    <div class="col-md-12">
                                        <div class="pdb4"><small >What technical or sports qualification do you have?</small></div>
                                    </div>
                                    <div class="col-md-12 pdb4">
                                        <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="e.g scuba diving, sky diving, etc.." tabindex="-1" >
                                    </div>
                                </div>
                                <!--AGENT BLOCK END-->
                            </div>
                                
                    </div>
                        <div class="col-md-10">
							<div class="account-property m-b-30 ">
								
								<div class="m-b-30 ">
									<div class="row no-header">
										<div class="col-12">
                                            <div class="col-md-6">
                                            </div>
                                            <div class="col-md-6">
                                                <div class="button-wrapper btnwrspe fr">
                                                    <a href="#" ><i class="fas fa-check-double"></i> UPDATE PROFILE</a>
                                                </div>
                                            </div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="community" role="tabpanel" aria-labelledby="community-tab">
					<div class="row">
                        <div class="account-property m-b-30 pdt20 col-md-10">
                            <div class="m-b-30 bg-grey">
                                
                                <div class="agent-block-wrapper-header bg-grey p-4">
                                    
                                    <h4 class="font-700">Community</h4>
                                    
                                </div>
                                
                                <!--AGENT BLOCK BEGIN-->
                                <div class="specialcont col-md-12 bg-grey">
                                    <div class="col-md-12">
                                        <div class="pdb4"><small >Besides standard sharing offers (items, services, experiences) what else can you share with the community?</small></div>
                                    </div>
                                    <div class="col-md-12 pdb4">
                                        <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="Placeholder" tabindex="-1" >
                                    </div>
                                    <div class="col-md-12">
                                        <div class="pdb4"><small >What can the community share with you?</small></div>
                                    </div>
                                    <div class="col-md-12 pdb4">
                                        <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="Placeholder" tabindex="-1" >
                                    </div>
                                </div>
                                <!--AGENT BLOCK END-->
                            </div>
                                
                    </div>
                        <div class="col-md-10">
							<div class="account-property m-b-30 ">
								
								<div class="m-b-30 ">
									<div class="row no-header">
										<div class="col-12">
                                            <div class="col-md-6">
                                            </div>
                                            <div class="col-md-6">
                                                <div class="button-wrapper btnwrspe fr">
                                                    <a href="#" ><i class="fas fa-check-double"></i> UPDATE PROFILE</a>
                                                </div>
                                            </div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="langua" role="tabpanel" aria-labelledby="langua-tab">
					<div class="row">
                        <div class="account-property m-b-30 pdt20 col-md-10">
                            <div class="m-b-30 bg-grey">
                                
                                <div class="agent-block-wrapper-header bg-grey p-4">
                                    
                                    <h4 class="font-700">Languages</h4>
                                    
                                </div>
                                
                                <!--AGENT BLOCK BEGIN-->
                                <div class="specialcont col-md-12 bg-grey">
                                    <div class="col-md-12">
                                        <div class="pdb4"><small >Native language:</small></div>
                                    </div>
                                    <div class="col-md-12 pdb4">
                                        <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="Enter a language" tabindex="-1" >
                                    </div>
                                    <div class="col-md-8">
                                        <div class="pdb4"><small >Other languages spoken:</small></div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="pdb4"><small >Level:</small></div>
                                    </div>
                                    <div class="col-md-8 pdb4">
                                        <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="Enter a language" tabindex="-1" >
                                    </div>
                                    <div class="col-md-4 pdb4">
                                        <select form="search-form-1" name="location" class="location-select big-input searchspecial selectized" data-placeholder="location" tabindex="-1" style="display: none;"><option value="london" selected="selected">Paris, FR</option>
                                        </select>
                                        <div class="selectize-control location-select big-input searchspecial single">
                                            <div class="selectize-input items full has-options has-items">
                                                <div class="item" data-value="london"></div>
                                                <input type="text" autocomplete="off" tabindex="" style="width: 4px;">
                                            </div>
                                            <div class="selectize-dropdown single location-select big-input searchspecial" style="display: none; width: 152px; top: 39px; left: 0px;">
                                                <div class="selectize-dropdown-content"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 pdb4">
                                        <div class="button-wrapper btnwrspe">
                                            <a href="#">ADD MORE</a>
                                        </div>
                                    </div>
                                    <div class="col-md-6 pdb4">
                                        <div class="button-wrapper btnwrspe fr">
                                            <a href="#" >REMOVE</a>
                                        </div>
                                    </div>
                                </div>
                                <!--AGENT BLOCK END-->
                            </div>
                                
                    </div>
                        <div class="col-md-10">
							<div class="account-property m-b-30 ">
								
								<div class="m-b-30 ">
									<div class="row no-header">
										<div class="col-12">
                                            <div class="col-md-6">
                                            </div>
                                            <div class="col-md-6">
                                                <div class="button-wrapper btnwrspe fr">
                                                    <a href="#" ><i class="fas fa-check-double"></i> UPDATE PROFILE</a>
                                                </div>
                                            </div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="about-you" role="tabpanel" aria-labelledby="about-you-tab">
					<div class="row">
                        <div class="account-property m-b-30 pdt20 col-md-10">
                            <div class="m-b-30 bg-grey">
                                <div class="agent-block-wrapper-header bg-grey p-4">
                                    <h4 class="font-700" >Personal</h4>
                                </div>
                                <!--AGENT BLOCK BEGIN-->
                                <div class="specialcont col-md-12 bg-grey">
                                    
                                    <div class="col-md-12 mb-5 ">
                                        <p>Describe yourself:</p>
                                        <p><small >Tell them about the things you like: What are 5 things you can't live without? Share your favourite travel destinations, books, movies, shows, music,food. <br>Tell them what it's like to spend time with you. What's your style of travelling? Tell them about you: Do you have a life motto?</small></p>
                                        
                                        <textarea placeholder="Describe the type of traveller you are or aspire to be." maxlength="600" data-limit="600" rows="8" ></textarea>
                                        <span class="countdown"><small>600 Characters remaining</small></span><br><br>
                                        
                                        
                                        
                                    </div>
                                    <div class="col-md-12">
                                        <div class="pdb4"><small >What is something you are interested in? (Add up to 8 - we recommend at least 3)</small></div>
                                    </div>
                                    <div class="col-md-12 pdb4">
										<select form="search-form-1" name="location" class="location-select big-input searchspecial selectized" data-placeholder="location" tabindex="-1" style="display: none;"><option value="london" selected="selected">Paris, FR</option>
										</select>
										<div class="selectize-control location-select big-input searchspecial single">
											<div class="selectize-input items full has-options has-items">
												<div class="item" data-value="london">Paris, FR</div>
												<input type="text" autocomplete="off" tabindex="" style="width: 4px;">
											</div>
											<div class="selectize-dropdown single location-select big-input searchspecial" style="display: none; width: 152px; top: 39px; left: 0px;">
												<div class="selectize-dropdown-content"></div>
											</div>
										</div>
                                    </div>
                                    <div class="col-md-6 pdb4">
                                        <div class="button-wrapper btnwrspe">
                                            <a href="#">ADD MORE</a>
                                        </div>
                                    </div>
                                    <div class="col-md-6 pdb4">
                                        <div class="button-wrapper btnwrspe fr">
                                            <a href="#" >REMOVE</a>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="pdb4"><small >Where is your favourite travel destination?</small></div>
                                    </div>
                                    <div class="col-md-12 pdb4">
                                        <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="" tabindex="-1" >
                                    </div>
                                    <div class="col-md-12">
                                        <div class="pdb4"><small >What do you like to do when you travel?</small></div>
                                    </div>
                                    <div class="col-md-12 pdb4">
                                        <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="" tabindex="-1" >
                                    </div>
                                    <div class="col-md-12">
                                        <div class="pdb4"><small >What can't you travel without?</small></div>
                                    </div>
                                    <div class="col-md-12 pdb4">
                                        <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="" tabindex="-1" >
                                    </div>
                                    <div class="col-md-12">
                                        <div class="pdb4"><small >What's your favourite part about travelling?</small></div>
                                    </div>
                                    <div class="col-md-12 pdb4">
                                        <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="" tabindex="-1" >
                                    </div>
                                    <div class="col-md-12">
                                        <div class="pdb4"><small >What's your least favourite part about travellling?</small></div>
                                    </div>
                                    <div class="col-md-12 pdb4">
                                        <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="" tabindex="-1" >
                                    </div>

                                </div>
                                <!--AGENT BLOCK END-->
                            </div>
                                
                    </div>
                        <div class="col-md-10">
							<div class="account-property m-b-30 ">
								
								<div class="m-b-30 ">
									<div class="row no-header">
										<div class="col-12">
                                            <div class="col-md-6">
                                            </div>
                                            <div class="col-md-6">
                                                <div class="button-wrapper btnwrspe fr">
                                                    <a href="#" ><i class="fas fa-check-double"></i> UPDATE PROFILE</a>
                                                </div>
                                            </div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

</div>
<!--modal approve-->
<div class="modal fade" id="write-review-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

	<div class="modal-dialog rounded-0" role="document">
		
		<div class="modal-content rounded-0 bg-grey">
			
			<div class="modal-header rounded-0 border-0">
				
				<h4 class="modal-title text-emperor font-700 ">Review Frances</h4>
				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				
				<span aria-hidden="true">&times;</span>
				
				</button>
				
			</div>
			
			<div class="modal-body">
				<div class="row">
					<div class="col-12 col-sm-6">
						<div class="user-container">
							<div class="user-block">
								<div class="avatar-image-container">
									<div class="avatar-image">
										<!--agent-status & status-online styles in style.less-->
										<a href="#chatorsomething">
											<img src="./images/userpicbig.png" alt="agent-photo">
										</a>
									</div>
								</div>
							</div>
							<div class="user-block">
								<div class="user-name">
									Frances
								</div>
								<div class="user-stars">
									<div class="stars" data-stars="3">
										<i class="material-icons star">star</i>
										<i class="material-icons star">star</i>
										<i class="material-icons star">star</i>
										<i class="material-icons star">star</i>
										<i class="material-icons star">star</i>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12 col-sm-6">
						<ul class="no-style-list">
							<li>
								<span>Metric #1 </span>
								<div class="stars orange-stars" data-stars="0">
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
								</div>
							</li>
							<li>
								<span>Metric #1 </span>
								<div class="stars orange-stars" data-stars="0">
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
								</div>
							</li>
						</ul>
					</div>
				</div>
				
				<textarea placeholder="Tell others what Frances was like."></textarea>
				
			</div>
			
			<div class="modal-footer border-0">
				
				<button type="button" class="btn btn-secondary floating-button bg-orange text-white text-uppercase font-700 font-open-sans px-5 py-3" style="font-size:13px;" data-dismiss="modal" data-toggle="modal" data-target="#approval-confirmation-modal" >Post review</button>
				
				
			</div>
			
		</div>
		
	</div>
	
</div>
<!--modal approval confirmation-->
<?php include 'scripts.php'; ?>
</body>
</html>