<?php include 'header.php' ?>

    <div class="modal fade" id="updatemyModal" role="dialog">
        <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title">Awaiting admin approval.</h4>
            <button type="button" class="btn btn-specials-red"  data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i></button>
            </div>
            <div class="modal-body">
            <p class="fz-12-">Thank you for responding to a need by publishing a sharing offer. Your offer is awaiting admin approval. Upon admin approval the community member whose need you responded to will be notified and may get in touch with you.
            </p>
            <p class="fz-12-">In the mean time, we've created a dashboard for you where you can view your offers, send and receive messages and much more. Feel free to explore this!</p>
            </div>
        </div>

        </div>
    </div>

<div class="account-page">
    <div class="account_menu account_menu__normal">
    <a href="#" class="js-toggle-menu">
        <div class="account_menu_agent">
            <div class="agent-avatar">
                <img src="./images/userpic.png" alt="user">
                <div class="agent-status status-online"></div>
            </div>
            <h2>Admin</h2>
        </div>
    </a>
    <div class="account_menu_categories">
        <a href="#property" class="js-menu-link new-notification__enabled" data-display-id="property">
            <div class="category_item">
                <div class="category_item__category-name">
                    <i class="material-icons">view_module</i>
                    <div class="new-notification"></div>
                    <span>Review sharing offers </span>
                </div>

                <span class="category_item__item-counts">
                    12
                </span>
            </div>
        </a>
        
        
        <a href="#settings" class="js-menu-link" data-display-id="settings">
            <div class="category_item">
                <div class="category_item__category-name">
                    <i class="material-icons">settings</i>
                    <div class="new-notification"></div>
                    <span>Settings</span>
                </div>
            </div>
        </a>
        
        
        <a href="#users" class="js-menu-link" data-display-id="users">
            <div class="category_item">
                <div class="category_item__category-name">
                    <i class="material-icons">group</i>
                    <div class="new-notification"></div>
                    <span>Users</span>
                </div>
            </div>
        </a>
        
        

        <!--<i class="material-icons js-test-toggle lg-visible"></i>-->
    </div>
</div>
    
    <div class="account-page-wild_block">
        
        <div id="property" class="js-content-blocks">
            <h1 class="account-header-top mb-4">Review sharing offers</h1>
            <div class="row">
                <div class="col-md-9">

                    <div class="account-property m-b-30">
    
                        <div class="table-property-listing"> 
        
                            <div class="item d-block">
            
                                <div class="container-fluid">            
                
                                    <div class="row ">
                    
                    
                                        <div class="col-2 p-0">
                        
                                            <img src="./images/property-img.png" alt="apartment">
                        
                                            <div class="badge-wrapper">
                            
                                                <div class="small-badge awaiting-admin"></div>
                        
                                            </div>
                    
                                        </div>

                    
                                        <div class="col-4">
                        <div class="name-wrap">
                            <a href="#">Item</a>
                            <a href="single-for-approval.php" title="Hiking boots"><h2>Hiking boots</h2></a>
                        </div>
                        <div class="property-info body">
                            <!--<div class="bad">Bad.<span>2</span></div>
                            <div class="bath">Bath.<span>7</span></div>
                            <div class="sq">Sq.<span>4700</span></div>
                            <div class="price">Price,£<span>6.900m</span></div>
                            <div class="perweek">Per week<span>7.200</span></div>-->
                        </div>
                    </div>

                    
                                        <div class="col-6 d-flex align-items-end justify-content-end text-right">
                    <!--<a href="#" class="button-link-normal mx-2 my-3"  data-toggle="modal" data-target="#updatemyModal">Approve</a>-->
                    <!--<a href="#" class="button-link-normal mx-2 my-3">Deny</a>-->
                    <!--<a href="#" class="button-link-normal mx-2 my-3">Deny</a>-->
                    <a href="single-for-approval.php" class="button-link-normal mx-2 my-3">View Offer</a>
                </div>

                </div>
                
            </div>
            
        </div>
        
    </div>
                        
</div>

                </div>
                <div class="col-md-3">
                    
                    <div class="account_properties_counters_list">
                        
                        <h4 class="mb-4">Filter by</h4>
                        
                        <a href="#" class="item">
                            <span class="badge open-for-sharing"></span> Open for sharing
                            <span class="count">38</span>
                        </a>
                        
                        <a href="#" class="item">
                            <span class="badge denied"></span> Denied
                            <span class="count">40</span>
                        </a>
                        
                        <a href="#" class="item">
                            <span class="badge rented"></span> Currently rented
                            <span class="count">88</span>
                        </a>
                        
                        <a href="#" class="item">
                            <span class="badge awaiting-admin"></span> Awaiting  Approval
                            <span class="count">50</span>
                        </a>
                        
                        
                        
                    </div>
                </div>
            </div>

        </div>

        <div id="settings" class="js-content-blocks">
    <div class="settings-wrapper">
        <div class="settings-options">

            <a href="#option1" class="settings-item">
                <div>
                    <i class="material-icons">bookmark</i>
                    <span>General settings</span>
                </div>

            </a>
            <a href="#option2" class="settings-item">
                <div>
                    <i class="material-icons">credit_card</i>
                    <span>Payment settings</span>
                </div>

            </a>
            <a href="#option3" class="settings-item">
                <div>
                    <i class="material-icons">notifications</i>
                    <span>Notifications settings</span>
                </div>

            </a>
            <a href="#option4" class="settings-item">
                <div>
                    <i class="material-icons">rss_feed</i>
                    <span>Social networks</span>
                </div>

            </a>
            <a href="#option5" class="settings-item">
                <div>
                    <i class="material-icons">speaker_notes</i>
                    <span>Support tickets</span>
                </div>

            </a>
            <a href="#option6" class="settings-item">
                <div>
                    <i class="material-icons">delete</i>
                    <span>Remove account</span>
                </div>

            </a>


        </div>
        <div id="chat1" class="option-window">
            <h1 class="account-header-top">Payment settings</h1>
            <div class="payment-wrapper">
                <div class="credit-card-form">
                    <img src="./images/mastercard.png" alt="credit-card-logo">
                    <div class="card-number">
                        <p class="card-props">Card number</p>
                        <div>
                            <input type="text" pattern="[0-9]{4}">
                            <input type="text" pattern="[0-9]{4}">
                            <input type="text" pattern="[0-9]{4}">
                            <input type="text" pattern="[0-9]{4}">
                        </div>

                    </div>
                    <div class="exp-date">
                        <p class="card-props">Expiration date</p>
                        <div>
                            <input type="text" pattern="[0-9]{2}">
                            <span>/</span>
                            <input type="text" pattern="[0-9]{2}">
                        </div>

                    </div>

                    <div class="card-holder">
                        <p class="card-props">Card holder</p>
                        <input type="text" required id="card-holder" pattern="[a-zA-Z]"
                               placeholder="Card holder name">
                        <label for="card-holder"></label>

                    </div>

                    <div class="card-cvv">
                        <p class="card-props">CVC/CVV</p>
                        <input type="text" pattern="[0-9]{3}">

                    </div>

                    <input type="submit" value="Save Payment Method" class="button-primary">

                </div>
                <div class="payment-methods">
                    <h2>Payment methods</h2>
                    <div class="payment-method-type">
                        <div class="pay-card">
                            <img src="./images/paypal.png">
                            <span>pp@tor• • • •</span>

                        </div>

                        <i class="material-icons js-hide">close</i>

                    </div>
                    <div class="payment-method-type">
                        <div class="pay-card">
                            <img src="./images/visa.png">
                            <span>pp@tor• • • •</span>

                        </div>

                        <i class="material-icons js-hide">close</i>
                    </div>

                    <button class="button-secondary">Add payment method</button>
                </div>
            </div>
        </div>

    </div>
</div>
        
        
        <div id="users" class="js-content-blocks w-100 p-5">
            
            <div class="row">
                
                <div class="col-12">
                    
                    <h1 class="account-header-top mb-4">Users</h1>
                    
                </div>
                
            </div>
            

        </div>

    </div>
</div>


<?php include 'scripts.php' ?>

</body>

</html>