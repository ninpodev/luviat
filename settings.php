<?php include 'header.php'; ?>
	
<div class="account-page">
		
    <?php include 'user-sidebar.php' ?>
		
    <div class="account-page-wild_block">
        
        <div id="settings" class="js-content-blocks p-5">
            <h1 class="account-header-top">Settings</h1>
            
            <div class="account-header-tabs">
                
                <ul class="nav" id="myTab" role="tablist">
                    
                    <li class="nav-item">
                        <a class="nav-link" id="personal-tab" data-toggle="tab" href="#personal" role="tab" aria-controls="personal" aria-selected="true">Personal information</a>
                    </li>
                    
                    <li class="nav-item">
                        <a class="nav-link" id="email-tab" data-toggle="tab" href="#email" role="tab" aria-controls="email" aria-selected="false">Email notifications</a>
                    </li>
                    
                    <li class="nav-item">
                        <a class="nav-link" id="password-tab" data-toggle="tab" href="#password" role="tab" aria-controls="password" aria-selected="false">Password</a>
                    </li>
                    
                    <li class="nav-item">
                        <a class="nav-link" id="privacy-tab" data-toggle="tab" href="#privacy" role="tab" aria-controls="privacy" aria-selected="false">Privacy</a>
                    </li>
                    
                    <li class="nav-item">
                        <a class="nav-link" id="deactivate-tab" data-toggle="tab" href="#deactivate" role="tab" aria-controls="deactivate" aria-selected="false">Deactivate Account</a>
                    </li>
                    
                </ul>
                
            </div>
            
            <div class="tab-content">
                
                <div class="tab-pane" id="reviews-by-you" role="tabpanel" aria-labelledby="email">
                    
                    <div class="row">
                        
                        <div class="col-12">
                            
                            <div class="account-property m-b-30">

                                <div class="m-b-30">
                                    <!--AGENT BLOCK BEGIN-->
                                    <div class="agent-block-wrapper-header">
                                        Rewrites to write
                                    </div>
                                    <!--AGENT BLOCK END-->
                                    <div class="agent-block-wrapper">
                                        <div class="agent-block-agent-img">
                                            <div class="image-wrap">
                                                <!--agent-status & status-online styles in style.less-->
                                                <div class="agent-status" title="online">
                                                </div>
                                                <a href="#chatorsomething">
                                                    <img src="./images/userpicbig.png" alt="agent-photo">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="agent-block-agent-information">
                                            <div class="row">
                                                <div class="block-left col-3">
                                                    <h2>Pauline Saunders</h2>
                                                </div>
                                                <div class="block-right col-9">
                                                    <span class="small-text orange-text">Ski Boots size 10</span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="block-left col-3">
                                                    <div class="stars" data-stars="3">
                                                        <i class="material-icons star">star</i>
                                                        <i class="material-icons star">star</i>
                                                        <i class="material-icons star">star</i>
                                                        <i class="material-icons star">star</i>
                                                        <i class="material-icons star">star</i>
                                                    </div>
                                                </div>
                                                <div class="block-right col-9">
                                                    <div class="text-container">
                                                        <div class="write-review">
                                                            <div class="review"></div>
                                                            <div class="review"></div>
                                                            <div class="review"></div>
                                                            <div class="review"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="block-left col-3">
                                                    <div class="small-text align-bottom">
                                                        July 08, 2018
                                                    </div>
                                                </div>
                                                <div class="block-right col-9">
                                                    <div class="small-text">
                                                        borrow date(s): 12.08.2018 - 15.08.2018
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="send-button">
                                                <a href="#" data-toggle="modal" data-target="#write-review-modal" class="button button-link-primary-icon" >Deactivate my account</a>

                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="deactivate" role="tabpanel" aria-labelledby="deactivate-tab">
                <div class="row">
                    <div class="account-property m-b-30 pdt20 col-md-10">
                        <div class="m-b-30 bg-grey">
                            <div class="agent-block-wrapper-header bg-grey">
                                <h4 class="p-4">Deactivate my account</h4>
                            </div>
                            <!--AGENT BLOCK BEGIN-->
                            <div class="specialcont col-md-12 bg-grey">

                                <div class="button-wrapper btnwrsperrr">

                                    <a href="#" data-toggle="modal" data-target="#deactivate-account" class="font-700" >Deactivate my account</a>

                                </div>

                            </div>
                            <!--AGENT BLOCK END-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="privacy" role="tabpanel" aria-labelledby="privacy-tab">
                <div class="row">
                    <div class="account-property m-b-30 pdt20 col-md-10">
                        <div class="m-b-30 bg-grey">
                            <div class="agent-block-wrapper-header bg-grey">
                                <h4 class="p-4">Privacy</h4>
                            </div>
                            <!--AGENT BLOCK BEGIN-->
                            <div class="specialcont col-md-12 bg-grey mh-140">

                            </div>
                            <!--AGENT BLOCK END-->
                        </div>

                </div>
                    <div class="col-md-10">
                        <div class="account-property m-b-30 ">

                            <div class="m-b-30 ">
                                <div class="row no-header">
                                    <div class="col-12">
                                        <div class="col-md-6">
                                        </div>
                                        <div class="col-md-6">
                                            <div class="button-wrapper btnwrspe fr">
                                                <a href="#" ><i class="fas fa-check-double"></i> UPDATE</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="password" role="tabpanel" aria-labelledby="password-tab">
                <div class="row">
                    <div class="account-property m-b-30 pdt20 col-md-10">
                        <div class="m-b-30 bg-grey">
                            <div class="agent-block-wrapper-header bg-grey">
                                <h4 class="p-4">Your password</h4>
                            </div>
                            <!--AGENT BLOCK BEGIN-->
                            <div class="specialcont col-md-12 bg-grey">
                                <div class="col-md-12">
                                    <div class="pdb4"><small >Old Password:</small></div>
                                </div>
                                <div class="col-md-12 pdb4">
                                    <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="Placeholder" tabindex="-1" >
                                </div>
                                <div class="col-md-12">
                                    <div class="pdb4"><small >New password:</small></div>
                                </div>
                                <div class="col-md-12 pdb4">
                                    <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="Placeholder" tabindex="-1" >
                                </div>
                                <div class="col-md-12">
                                    <div class="pdb4"><small >Confirm password:</small></div>
                                </div>
                                <div class="col-md-12 pdb4">
                                    <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="Placeholder" tabindex="-1" >
                                </div>
                            </div>
                            <!--AGENT BLOCK END-->
                        </div>

                </div>
                    <div class="col-md-10">
                        <div class="account-property m-b-30 ">

                            <div class="m-b-30 ">
                                <div class="row no-header">
                                    <div class="col-12">
                                        <div class="col-md-6">
                                        </div>
                                        <div class="col-md-6">
                                            <div class="button-wrapper btnwrspe fr">
                                                <a href="#" ><i class="fas fa-check-double"></i> UPDATE</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="email" role="tabpanel" aria-labelledby="email-tab">
                <div class="row">
                    <div class="account-property m-b-30 pdt20 col-md-10">
                        <div class="m-b-30 bg-grey">
                            <div class="agent-block-wrapper-header bg-grey">
                                <h4 class="p-4">Email notifications</h4>
                            </div>
                            <!--AGENT BLOCK BEGIN-->
                            <div class="specialcont col-md-12 bg-grey">
                                <div class="col-md-2">
                                    <div class="pdb4">Reminders:</div>
                                </div>
                                <div class="col-md-10 pdb4">
                                    <div class="col-md-12 pdb4"><small >Receive booking reminders, requests to write a review and other reminders related to your activities on Luviat.</small></div>
                                    <div class="col-md-12">
                                        <input type="checkbox" name="reminder-email" id="reminder-email" class="css-checkbox">
                                        <label for="reminder-email" class="css-label">Emails</label>
                                    </div>
                                    <div class="col-md-12">
                                        <input type="checkbox" name="reminder-text" id="reminder-text" class="css-checkbox">
                                        <label for="reminder-text" class="css-label">Text messages</label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="pdb4">Promotions:</div>
                                </div>
                                <div class="col-md-10 pdb4">
                                    <div class="col-md-12 pdb4"><small >Receive coupons, promotions, surveys, product updates, and inspiration from Luviat and its partners.</small></div>
                                    <div class="col-md-12">
                                        <input type="checkbox" name="promotions-email" id="promotions-email" class="css-checkbox">
                                        <label for="promotions-email" class="css-label">Emails</label>
                                    </div>
                                    <div class="col-md-12">
                                        <input type="checkbox" name="promotions-text" id="promotions-text" class="css-checkbox">
                                        <label for="promotions-text" class="css-label">Text messages</label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="pdb4">Policy & Community:</div>
                                </div>
                                <div class="col-md-10 pdb4">
                                    <div class="col-md-12 pdb4"><small >Receive updates on changes to Luviat's policy.</small></div>
                                    <div class="col-md-12">
                                        <input type="checkbox" name="policy-email" id="policy-email" class="css-checkbox">
                                        <label for="policy-email" class="css-label">Emails</label>
                                    </div>
                                    <div class="col-md-12">
                                        <input type="checkbox" name="policy-text" id="policy-text" class="css-checkbox">
                                        <label for="policy-text" class="css-label">Text messages</label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="pdb4">Account support:</div>
                                </div>
                                <div class="col-md-10 pdb4">
                                    <div class="col-md-12 pdb4"><small >We may need to send you messages regarding your account, legal notifications, security and privacy matters, and customer support requests. For your security you cannot disable email notifications and we may contact you by phone or other means if needed.</small></div>
                                    <div class="col-md-12">
                                        <input type="checkbox" name="support-email" id="support-email" class="css-checkbox" checked >
                                        <label for="support-email" class="css-label">Emails</label>
                                    </div>
                                    <div class="col-md-12">
                                        <input type="checkbox" name="support-text" id="support-text" class="css-checkbox">
                                        <label for="support-text" class="css-label">Text messages</label>
                                    </div>
                                </div>
                            </div>
                            <!--AGENT BLOCK END-->
                        </div>

                </div>
                    <div class="col-md-10">
                        <div class="account-property m-b-30 ">

                            <div class="m-b-30 ">
                                <div class="row no-header">
                                    <div class="col-12">
                                        <div class="col-md-6">
                                        </div>
                                        <div class="col-md-6">
                                            <div class="button-wrapper btnwrspe fr">
                                                <a href="#" ><i class="fas fa-check-double"></i> UPDATE</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="personal" role="tabpanel" aria-labelledby="personal-tab">
                <div class="row">
                    <div class="account-property m-b-30 pdt20 col-md-10">
                        <div class="m-b-30 bg-grey">
                            <div class="agent-block-wrapper-header bg-grey">
                                <h4 class="p-4">Required</h4>
                            </div>
                            <!--AGENT BLOCK BEGIN-->
                            <div class="specialcont col-md-12 bg-grey">
                                <div class="col-md-2">
                                    <div class="pdb4">Account:</div>
                                </div>
                                <div class="col-md-10 pdb4 border-bottom mb-5">
                                    <div class="col-md-12"><small >Username:</small></div>
                                    <div class="col-md-12 pdb4">
                                        <input class="location-select big-input inputs" placeholder="anita_bonfield" type="text" disabled >
                                    </div>

                                    <div class="col-md-12"><small>Password:</small></div>
                                    
                                    <div class="col-md-12 pdb4 text-right">
                                        
                                        <input class="location-select big-input inputs" placeholder="········" type="password">

                                    </div>
                                    
                                    
                                    <div class="col-md-12"><small>Repeat your password:</small></div>
                                    
                                    <div class="col-md-12 pdb4 text-right">
                                        
                                        <input class="location-select big-input inputs" placeholder="········" type="password">

                                    </div>
                                    
                                </div>


                                <div class="col-md-2">
                                    <div class="pdb4">Basic:</div>
                                </div>
                                <div class="col-md-10 pdb4 border-bottom mb-5">
                                    <div class="col-md-12"><small >First name:</small></div>
                                    <div class="col-md-12 pdb4">
                                        <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="Placeholder" tabindex="-1" >
                                    </div>
                                    <div class="col-md-12"><small >Last name:</small></div>
                                    <div class="col-md-12 pdb4">
                                        <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="Placeholder" tabindex="-1" >
                                    </div>
                                </div>


                                <div class="col-md-2">
                                    <div class="pdb4">Contact Information:</div>
                                </div>
                                <div class="col-md-10 pdb4">
                                    <div class="col-md-12"><small >Email:</small></div>
                                    <div class="col-md-12 pdb4">
                                        <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="Placeholder" tabindex="-1" >
                                    </div>
                                    <div class="col-md-6"><small >Mobile Phone:</small></div>
                                    <div class="col-md-6"><small >Landline:</small></div>
                                    <div class="col-md-6 pdb4">
                                        <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="Placeholder" tabindex="-1" >
                                    </div>
                                    <div class="col-md-6 pdb4">
                                        <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="Placeholder" tabindex="-1" >
                                    </div>
                                    <div class="col-md-8"><small >Your address (this is not displayed publicly):</small></div>
                                    <div class="col-md-4 pdb4"><small ></small></div>
                                    <div class="col-md-8 pdb4">
                                        <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="54 Canberra Avenue, Kingston, ACT, Australia" tabindex="-1" >
                                    </div>
                                </div>

                            </div>
                            <!--AGENT BLOCK END-->
                        </div>

                </div>
                    <div class="col-md-10">
                        <div class="account-property m-b-30 ">

                            <div class="m-b-30 ">
                                <div class="row no-header">
                                    <div class="col-12">
                                        <div class="col-md-6">
                                        </div>
                                        <div class="col-md-6">
                                            <div class="button-wrapper btnwrspe fr">
                                                <a href="#" ><i class="fas fa-check-double"></i> UPDATE</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
	</div>

</div>

<div class="modal fade" id="write-review-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

	<div class="modal-dialog rounded-0" role="document">
		
		<div class="modal-content rounded-0 bg-grey">
			
			<div class="modal-header rounded-0 border-0">
				
				<h4 class="modal-title text-emperor font-700 ">Review Frances</h4>
				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				
				<span aria-hidden="true">&times;</span>
				
				</button>
				
			</div>
			
			<div class="modal-body">
				<div class="row">
					<div class="col-12 col-sm-6">
						<div class="user-container">
							<div class="user-block">
								<div class="avatar-image-container">
									<div class="avatar-image">
										<!--agent-status & status-online styles in style.less-->
										<a href="#chatorsomething">
											<img src="./images/userpicbig.png" alt="agent-photo">
										</a>
									</div>
								</div>
							</div>
							<div class="user-block">
								<div class="user-name">
									Frances
								</div>
								<div class="user-stars">
									<div class="stars" data-stars="3">
										<i class="material-icons star">star</i>
										<i class="material-icons star">star</i>
										<i class="material-icons star">star</i>
										<i class="material-icons star">star</i>
										<i class="material-icons star">star</i>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12 col-sm-6">
						<ul class="no-style-list">
							<li>
								<span>Metric #1 </span>
								<div class="stars orange-stars" data-stars="0">
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
								</div>
							</li>
							<li>
								<span>Metric #1 </span>
								<div class="stars orange-stars" data-stars="0">
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
								</div>
							</li>
						</ul>
					</div>
				</div>
				
				<textarea placeholder="Tell others what Frances was like."></textarea>
				
			</div>
			
			<div class="modal-footer border-0">
				
				<button type="button" class="btn btn-secondary floating-button bg-orange text-white text-uppercase font-700 font-open-sans px-5 py-3" style="font-size:13px;" data-dismiss="modal" data-toggle="modal" data-target="#approval-confirmation-modal" >Post review</button>
				
				
			</div>
			
		</div>
		
	</div>
	
</div>

<div class="modal fade" id="deactivate-account" tabindex="-1" data-backdrop="static" role="dialog" aria-hidden="true">

	<div class="modal-dialog modal-lg">
        
            <div class="modal-content">
            <div class="modal-header d-flex justify-content-start">
            <h4 class="modal-title"><i class="fas fa-exclamation-triangle"></i> You are about to deactivate your account.</h4>
            </div>
            <div class="modal-body">
                <p>If you are having problems on Luviat, maybe we can help you. Contact the help team  to tell us what is happening to you. If you wish to delete your account, you can follow these steps. We're sorry you're leaving</p>


                <p>We may retain some personal information for certain business, legal or security issues. For example, we will retain information about any purchases you may have made through Luviat, and when you accepted our Terms of Service and our Privacy Policy.</p>


                <p>Do you want to reactivate your account? Too easy! You just have to sign in to Luviat again as usual within 30 days after disabling it.</p>


                <p>Keep in mind  that while your account is deactivated you will not be able to sign in with your email address, nor will you be able to change your password, so keep it always handy in case you want to log in again and reactivate your account.</p>

                <p>For more information, see our <a href="#">Privacy Policy</a></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btnwb" data-dismiss="modal">Cancel this action</button>
                <button type="button" class="btn btn-danger text-white font-700 font-open-sans px-5 py-3 text-uppercase" data-toggle="modal" data-dismiss="modal" data-target="#deactivate-confirmed">Proceed with deactivation</button>
            </div>
        </div>

        </div>
	
</div>
    
<div class="modal fade" id="deactivate-confirmed" tabindex="-1" data-backdrop="static" role="dialog" aria-hidden="true">
    
        <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header d-flex justify-content-start">
            
                <h4 class="modal-title"><i class="fa fa-frown-o fa-2x" aria-hidden="true"></i> We're sorry you're leaving</h4>
                
            </div>
            <div class="modal-body">
                <p>Do you want to reactivate your account? Too easy! You just have to <a href="#">log in</a> to Luviat again as usual within 30 days after disabling it.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btnwb" data-dismiss="modal">Close this</button>
                
            </div>
        </div>

        </div>
    </div>

<?php include 'scripts.php'; ?>

</body>

</html>