<?php include 'header.php'; ?>
	
<div class="account-page">
		
    <?php include 'user-sidebar.php' ?>
		
    <div class="account-page-wild_block">
			
        <div id="reviews" class="js-content-blocks p-5">
				
            <h1 class="account-header-top">Reviews</h1>
				
            <div class="account-header-tabs">
					<ul class="nav" id="myTab" role="tablist">
						<li class="nav-item">
							<a class="nav-link" id="reviews-by-you-tab" data-toggle="tab" href="#reviews-by-you" role="tab" aria-controls="reviews-by-you" aria-selected="true">Reviews by you</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="reviews-about-you-tab" data-toggle="tab" href="#reviews-about-you" role="tab" aria-controls="reviews-about-you" aria-selected="false">Reviews about you</a>
						</li>
					</ul>
				</div>
				
            <div class="tab-content">
					
                <div class="tab-pane" id="reviews-by-you" role="tabpanel" aria-labelledby="reviews-by-you-tab">
						<div class="row">
							<div class="col-9">
								<div class="account-property m-b-30">
									
									<div class="m-b-30">
										<!--AGENT BLOCK BEGIN-->
										<div class="agent-block-wrapper-header">
                                            <h4 class="my-5" >Reviews to write</h4>
										</div>
										<!--AGENT BLOCK END-->
										<div class="agent-block-wrapper">
											<div class="agent-block-agent-img">
												<div class="image-wrap">
													<!--agent-status & status-online styles in style.less-->
													<div class="agent-status" title="online">
													</div>
													<a href="#chatorsomething">
														<img src="./images/userpicbig.png" alt="agent-photo">
													</a>
												</div>
											</div>
											<div class="agent-block-agent-information">
												<div class="row">
													<div class="block-left col-3">
														<h2>Pauline Saunders</h2>
													</div>
													<div class="block-right col-9">
														<span class="small-text orange-text">Ski Boots size 10</span>
													</div>
												</div>
												<div class="row">
													<div class="block-left col-3">
														<div class="stars" data-stars="3">
															<i class="material-icons star">star</i>
															<i class="material-icons star">star</i>
															<i class="material-icons star">star</i>
															<i class="material-icons star">star</i>
															<i class="material-icons star">star</i>
														</div>
													</div>
													<div class="block-right col-9">
														<div class="text-container">
															<div class="write-review">
																<div class="review"></div>
																<div class="review"></div>
																<div class="review"></div>
																<div class="review"></div>
															</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="block-left col-3">
														<div class="small-text align-bottom">
															July 08, 2018
														</div>
													</div>
													<div class="block-right col-9">
														<div class="small-text">
															borrow date(s): 12.08.2018 - 15.08.2018
														</div>
													</div>
												</div>
                                                
                                                
                                                <div class="button-wrapper btnwrspe float-right">
                                                    <a href="#" data-toggle="modal" data-target="#write-review-modal">Write Review</a>
                                                </div>
                                                
												
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-9">
							<div class="account-property m-b-30">
								
								<div class="m-b-30">
									<div class="agent-block-wrapper-header">
                                        <h4 class="my-4" >Past reviews you've written</h4>
									</div>
									<!--AGENT BLOCK BEGIN-->
									<div class="agent-block-wrapper mb-4">
										<div class="agent-block-agent-img">
											<div class="image-wrap">
												<!--agent-status & status-online styles in style.less-->
												<div class="agent-status" title="online">
												</div>
												<a href="#chatorsomething">
													<img src="./images/userpicbig.png" alt="agent-photo">
												</a>
											</div>
										</div>
										<div class="agent-block-agent-information">
											<div class="row">
												<div class="block-left col-3">
													<h2>Pauline Saunders</h2>
												</div>
												<div class="block-right col-9">
													<span class="small-text orange-text">Ski Boots size 10</span>
												</div>
											</div>
											<div class="row">
												<div class="block-left col-3">
													<div class="stars" data-stars="3">
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
													</div>
												</div>
												<div class="block-right col-9">
													<div class="text-container">
														It's a little bit over a week til the mighty Rock M Roll Alternative Markets wich Includes the Recotd Fail will be on. The fail is put on by Egg Records (the creators of the biggest record fair in Australia, theGlebe Fair) and Revolve Records the creators of The Glenbrock Fair.
													</div>
												</div>
											</div>
											<div class="row">
												<div class="block-left col-3">
													<div class="small-text align-bottom">
														July 08, 2018
													</div>
												</div>
												<div class="block-right col-9">
													<div class="small-text">
														borrow date(s): 12.08.2018 - 15.08.2018
													</div>
												</div>
											</div>
										</div>
									</div>
									<!--AGENT BLOCK END-->
									<div class="agent-block-wrapper mb-4">
										<div class="agent-block-agent-img">
											<div class="image-wrap">
												<!--agent-status & status-online styles in style.less-->
												<div class="agent-status" title="online">
												</div>
												<a href="#chatorsomething">
													<img src="./images/userpicbig.png" alt="agent-photo">
												</a>
											</div>
										</div>
										<div class="agent-block-agent-information">
											<div class="row">
												<div class="block-left col-3">
													<h2>Pauline Saunders</h2>
												</div>
												<div class="block-right col-9">
													<span class="small-text orange-text">Ski Boots size 10</span>
												</div>
											</div>
											<div class="row">
												<div class="block-left col-3">
													<div class="stars" data-stars="3">
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
													</div>
												</div>
												<div class="block-right col-9">
													<div class="text-container">
														It's a little bit over a week til the mighty Rock M Roll Alternative Markets wich Includes the Recotd Fail will be on. The fail is put on by Egg Records (the creators of the biggest record fair in Australia, theGlebe Fair) and Revolve Records the creators of The Glenbrock Fair.
													</div>
												</div>
											</div>
											<div class="row">
												<div class="block-left col-3">
													<div class="small-text align-bottom">
														July 08, 2018
													</div>
												</div>
												<div class="block-right col-9">
													<div class="small-text">
														borrow date(s): 12.08.2018 - 15.08.2018
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
                <div class="tab-pane mt-5 " id="reviews-about-you" role="tabpanel" aria-labelledby="reviews-about-you-tab">
                    
                    <h4 class="mt-4 mb-5"> Reviews About You</h4>
                    
					<div class="row">
						<div class="col-9">
							<div class="account-property m-b-30">
								
								<div class="m-b-30">
									<div class="row no-header">
										<div class="col-3">
											<div class="avatar-image-container">
												<div class="avatar-image">
													<!--agent-status & status-online styles in style.less-->
													<a href="#chatorsomething">
														<img src="./images/userpicbig.png" alt="agent-photo">
													</a>
												</div>
											</div>
											<a href="#" class="edit-profile-link text-orange my-4">
													Edit your profile
											</a>
										</div>
										<div class="col-9">
											<div class="row stars-container">
												<div class="col-12 main-star">
													<label>
														9 reviews
													</label>
													<div class="stars" data-stars="3">
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
													</div>
												</div>
												<div class="col-6">
													<label>
														Metric #1
													</label>
													<div class="stars" data-stars="3">
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
													</div>
												</div>
												<div class="col-6">
													<label>
														Metric #1
													</label>
													<div class="stars" data-stars="3">
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
													</div>
												</div>
												<div class="col-6">
													<label>
														Metric #1
													</label>
													<div class="stars" data-stars="3">
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
													</div>
												</div>
												<div class="col-6">
													<label>
														Metric #1
													</label>
													<div class="stars" data-stars="3">
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
													</div>
												</div>
												<div class="col-6">
													<label>
														Metric #1
													</label>
													<div class="stars" data-stars="3">
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
													</div>
												</div>
												<div class="col-6">
													<label>
														Metric #1
													</label>
													<div class="stars" data-stars="3">
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
													</div>
												</div>
												<div class="col-6">
													<label>
														Metric #1
													</label>
													<div class="stars" data-stars="3">
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
													</div>
												</div>
												<div class="col-6">
													<label>
														Metric #1
													</label>
													<div class="stars" data-stars="3">
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-9">
							<div class="account-property m-b-30">
								
								<div class="m-b-30">
									<div class="agent-block-wrapper-header">
                                        <h4 class="my-4" >Reviews writted about you</h4>
									</div>
									<!--AGENT BLOCK BEGIN-->
									<div class="agent-block-wrapper mb-4">
										<div class="agent-block-agent-img">
											<div class="image-wrap">
												<!--agent-status & status-online styles in style.less-->
												<div class="agent-status" title="online">
												</div>
												<a href="#chatorsomething">
													<img src="./images/userpicbig.png" alt="agent-photo">
												</a>
											</div>
										</div>
										<div class="agent-block-agent-information">
											<div class="row">
												<div class="block-left col-3">
													<h2>Pauline Saunders</h2>
												</div>
												<div class="block-right col-9">
													<span class="small-text orange-text">Ski Boots size 10</span>
												</div>
											</div>
											<div class="row">
												<div class="block-left col-3">
													<div class="stars" data-stars="3">
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
													</div>
												</div>
												<div class="block-right col-9">
													<div class="text-container">
														It's a little bit over a week til the mighty Rock M Roll Alternative Markets wich Includes the Recotd Fail will be on. The fail is put on by Egg Records (the creators of the biggest record fair in Australia, theGlebe Fair) and Revolve Records the creators of The Glenbrock Fair.
													</div>
												</div>
											</div>
											<div class="row">
												<div class="block-left col-3">
													<div class="small-text align-bottom">
														July 08, 2018
													</div>
												</div>
												<div class="block-right col-9">
													<div class="small-text">
														borrow date(s): 12.08.2018 - 15.08.2018
													</div>
												</div>
											</div>
										</div>
									</div>
									<!--AGENT BLOCK END-->
									<div class="agent-block-wrapper mb-4">
										<div class="agent-block-agent-img">
											<div class="image-wrap">
												<!--agent-status & status-online styles in style.less-->
												<div class="agent-status" title="online">
												</div>
												<a href="#chatorsomething">
													<img src="./images/userpicbig.png" alt="agent-photo">
												</a>
											</div>
										</div>
										<div class="agent-block-agent-information">
											<div class="row">
												<div class="block-left col-3">
													<h2>Pauline Saunders</h2>
												</div>
												<div class="block-right col-9">
													<span class="small-text orange-text">Ski Boots size 10</span>
												</div>
											</div>
											<div class="row">
												<div class="block-left col-3">
													<div class="stars" data-stars="3">
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
														<i class="material-icons star">star</i>
													</div>
												</div>
												<div class="block-right col-9">
													<div class="text-container">
														It's a little bit over a week til the mighty Rock M Roll Alternative Markets wich Includes the Recotd Fail will be on. The fail is put on by Egg Records (the creators of the biggest record fair in Australia, theGlebe Fair) and Revolve Records the creators of The Glenbrock Fair.
													</div>
												</div>
											</div>
											<div class="row">
												<div class="block-left col-3">
													<div class="small-text align-bottom">
														July 08, 2018
													</div>
												</div>
												<div class="block-right col-9">
													<div class="small-text">
														borrow date(s): 12.08.2018 - 15.08.2018
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
                
			</div>
		
        </div>

    </div>

</div>

<!--modal approve-->

<div class="modal fade" id="write-review-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

	<div class="modal-dialog rounded-0" role="document">
		
		<div class="modal-content rounded-0 bg-grey">
			
			<div class="modal-header rounded-0 border-0">
				
				<h4 class="modal-title text-emperor font-700 ">Review Frances</h4>
				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				
				<span aria-hidden="true">&times;</span>
				
				</button>
				
			</div>
			
			<div class="modal-body">
				<div class="row">
					<div class="col-12 col-sm-6">
						<div class="user-container">
							<div class="user-block">
								<div class="avatar-image-container">
									<div class="avatar-image">
										<!--agent-status & status-online styles in style.less-->
										<a href="#chatorsomething">
											<img src="./images/userpicbig.png" alt="agent-photo">
										</a>
									</div>
								</div>
							</div>
							<div class="user-block">
								<div class="user-name">
									Frances
								</div>
								<div class="user-stars">
									<div class="stars" data-stars="3">
										<i class="material-icons star">star</i>
										<i class="material-icons star">star</i>
										<i class="material-icons star">star</i>
										<i class="material-icons star">star</i>
										<i class="material-icons star">star</i>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12 col-sm-6">
						<ul class="no-style-list">
							<li>
								<span>Metric #1 </span>
								<div class="stars orange-stars" data-stars="0">
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
								</div>
							</li>
							<li>
								<span>Metric #1 </span>
								<div class="stars orange-stars" data-stars="0">
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
								</div>
							</li>
						</ul>
					</div>
				</div>
				
				<textarea placeholder="Tell others what Frances was like."></textarea>
				
			</div>
			
			<div class="modal-footer border-0">
				
				<button type="button" class="btn btn-secondary floating-button bg-orange text-white text-uppercase font-700 font-open-sans px-5 py-3" style="font-size:13px;" data-dismiss="modal" data-toggle="modal" data-target="#approval-confirmation-modal" >Post review</button>
				
				
			</div>
			
		</div>
		
	</div>
	
</div>
<!--modal approval confirmation-->

<?php include 'scripts.php'; ?>

</body>

</html>