<?php include 'header.php'; ?>

<div class="account-page">
        
    <?php include 'user-sidebar.php' ?>

    <div class="account-page-wild_block">

        <div id="wantlist" class="js-content-blocks p-5">

            <h1 class="account-header-top mb-4">Wantlist</h1>

            <div class="tab-content">

                <div id="reviews-by-you" role="tabpanel">

                    <div class="row">
                        
                        <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">

                            <div class="account-property mb-4">

                                <div class="specialcont col-12 float-left mb-5">

                                        <div class="agent-block-wrapper-header">

                                            <h4 class="font-weight-bold my-3 text-emperor " >Create new Need</h4>

                                        </div>

                                        <div class="col-md-3">
                                            <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="Nightlife" tabindex="-1" >
                                        </div>

                                        <div class="col-md-3">
                                            <select form="search-form-1" name="location" class="location-select big-input searchspecial selectized" data-placeholder="location" tabindex="-1" style="display: none;"><option value="london" selected="selected">Paris, FR</option>
                                            </select>
                                            <div class="selectize-control location-select big-input searchspecial single">
                                                <div class="selectize-input items full has-options has-items">
                                                    <div class="item" data-value="london">Paris, FR</div>
                                                    <input type="text" autocomplete="off" tabindex="" style="width: 4px;">
                                                </div>
                                                <div class="selectize-dropdown single location-select big-input searchspecial" style="display: none; width: 152px; top: 39px; left: 0px;">
                                                    <div class="selectize-dropdown-content"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <select form="search-form-1" name="location" class="location-select big-input searchspecial selectized" data-placeholder="location" tabindex="-1" style="display: none;"><option value="london" selected="selected">Paris, FR</option>
                                            </select>
                                            <div class="selectize-control location-select big-input searchspecial single">
                                                <div class="selectize-input items full has-options has-items">
                                                    <div class="item" data-value="london">Wine Tour</div>
                                                    <input type="text" autocomplete="off" tabindex="" style="width: 4px;">
                                                </div>
                                                <div class="selectize-dropdown single location-select big-input searchspecial" style="display: none; width: 152px; top: 39px; left: 0px;">
                                                    <div class="selectize-dropdown-content"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <select form="search-form-1" name="location" class="location-select big-input searchspecial selectized" data-placeholder="location" tabindex="-1" style="display: none;"><option value="london" selected="selected">Paris, FR</option>
                                            </select>
                                            <div class="selectize-control location-select big-input searchspecial single">
                                                <div class="selectize-input items full has-options has-items">
                                                    <div class="item" data-value="london">18.08.2018</div>
                                                    <input type="text" autocomplete="off" tabindex="" style="width: 4px;">
                                                </div>
                                                <div class="selectize-dropdown single location-select big-input searchspecial" style="display: none; width: 152px; top: 39px; left: 0px;">
                                                    <div class="selectize-dropdown-content"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 pdtop10">
                                            <textarea id="editor2" placeholder="Leave a short message so peoplpe know exactly what you're after." ></textarea>
                                        </div>

                                        <div class="col-md-5">
                                            <div class="button-wrapper btnwrspe">
                                                <a href="#">VIEW SEARCH RESULTS</a>
                                            </div>
                                        </div>

                                        <div class="col-md-1">&nbsp;</div>

                                        <div class="col-md-3">&nbsp;</div>

                                        <div class="col-md-3">
                                            <div class="button-wrapper btnwrspe fr">
                                                <a href="#" data-dismiss="modal" data-toggle="modal" data-target="#create-need-confirmed">CREATE NEED AD</a>
                                            </div>
                                        </div>

                                    </div>

                            </div>

                            <div class="account-property mb-5 pt-2 col-12 float-left">

                                <div class="m-b-30">

                                    <div class="agent-block-wrapper-header">
                                        <h4 class="font-weight-bold my-3 text-emperor" >Your Need ads</h4>
                                    </div>
                                    <!--AGENT BLOCK BEGIN-->
                                    <div class="specialcont col-md-12 bg-grey mb-4">

                                        <div class="col-md-3">

                                            <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="Nightlife" tabindex="-1">

                                        </div>

                                        <div class="col-md-3">

                                            <select form="search-form-1" name="location" class="location-select big-input searchspecial selectized" data-placeholder="location" tabindex="-1" style="display: none;"><option value="london" selected="selected">Paris, FR</option>
                                            </select>

                                            <div class="selectize-control location-select big-input searchspecial single">
                                                <div class="selectize-input items full has-options has-items">
                                                    <div class="item" data-value="london">Paris, FR</div>
                                                    <input type="text" autocomplete="off" tabindex="" style="width: 4px;">
                                                </div>
                                                <div class="selectize-dropdown single location-select big-input searchspecial" style="display: none; width: 152px; top: 39px; left: 0px;">
                                                    <div class="selectize-dropdown-content"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <select form="search-form-1" name="location" class="location-select big-input searchspecial selectized" data-placeholder="location" tabindex="-1" style="display: none;"><option value="london" selected="selected">Paris, FR</option>
                                            </select>
                                            <div class="selectize-control location-select big-input searchspecial single">
                                                <div class="selectize-input items full has-options has-items">
                                                    <div class="item" data-value="london">Wine Tour</div>
                                                    <input type="text" autocomplete="off" tabindex="" style="width: 4px;">
                                                </div>
                                                <div class="selectize-dropdown single location-select big-input searchspecial" style="display: none; width: 152px; top: 39px; left: 0px;">
                                                    <div class="selectize-dropdown-content"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <select form="search-form-1" name="location" class="location-select big-input searchspecial selectized" data-placeholder="location" tabindex="-1" style="display: none;"><option value="london" selected="selected">Paris, FR</option>
                                            </select>
                                            <div class="selectize-control location-select big-input searchspecial single">
                                                <div class="selectize-input items full has-options has-items">
                                                    <div class="item" data-value="london">18.08.2018</div>
                                                    <input type="text" autocomplete="off" tabindex="" style="width: 4px;">
                                                </div>
                                                <div class="selectize-dropdown single location-select big-input searchspecial" style="display: none; width: 152px; top: 39px; left: 0px;">
                                                    <div class="selectize-dropdown-content"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 pdtop10">
                                            <textarea id="editor2" placeholder="Lorem ipsum dolor sit amet, consectetur adipsiicis, sed do eisuaid labore et dolore magna aliqua. Ut aneim adms imnir pevnit ventinima laboris ullamanco laboris nisi ut aliquimp ex ae comodo cosequat." ></textarea>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="button-wrapper btnwrspe">
                                                <a href="#">VIEW RESULTS FOR THIS SEARCH</a>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="button-wrapper btnwrsp btnwrsp fr">
                                                <a href="#"  data-toggle="modal" data-target="#myModal" class="text-red" >REMOVE FROM WANTLIST</a>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="button-wrapper btnwrspe fr">
                                                <a href="#" data-toggle="modal" data-target="#updatemyModal">UPDATE NEED</a>
                                            </div>
                                        </div>

                                    </div>
                                    <!--AGENT BLOCK END-->
                                    <div class="specialcont col-md-12 bg-grey mb-4">

                                        <div class="col-md-3">

                                            <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="Nightlife" tabindex="-1">

                                        </div>

                                        <div class="col-md-3">

                                            <select form="search-form-1" name="location" class="location-select big-input searchspecial selectized" data-placeholder="location" tabindex="-1" style="display: none;">

                                                <option value="london" selected="selected">Paris, FR</option>

                                            </select>

                                            <div class="selectize-control location-select big-input searchspecial single">

                                                <div class="selectize-input items full has-options has-items">

                                                    <div class="item" data-value="london">Paris, FR</div>

                                                    <input type="text" autocomplete="off" tabindex="" style="width: 4px;">

                                                </div>

                                                <div class="selectize-dropdown single location-select big-input searchspecial" style="display: none; width: 152px; top: 39px; left: 0px;">

                                                    <div class="selectize-dropdown-content"></div>

                                                </div>

                                            </div>

                                        </div>

                                        <div class="col-md-3">

                                            <select form="search-form-1" name="location" class="location-select big-input searchspecial selectized" data-placeholder="location" tabindex="-1" style="display: none;">

                                                <option value="london" selected="selected">Paris, FR</option>

                                            </select>

                                            <div class="selectize-control location-select big-input searchspecial single">

                                                <div class="selectize-input items full has-options has-items">

                                                    <div class="item" data-value="london">Wine Tour</div>

                                                    <input type="text" autocomplete="off" tabindex="" style="width: 4px;">

                                                </div>

                                                <div class="selectize-dropdown single location-select big-input searchspecial" style="display: none; width: 152px; top: 39px; left: 0px;">

                                                    <div class="selectize-dropdown-content"></div>

                                                </div>

                                            </div>

                                        </div>

                                        <div class="col-md-3">

                                            <select form="search-form-1" name="location" class="location-select big-input searchspecial selectized" data-placeholder="location" tabindex="-1" style="display: none;">

                                                <option value="london" selected="selected">Paris, FR</option>

                                            </select>

                                            <div class="selectize-control location-select big-input searchspecial single">

                                                <div class="selectize-input items full has-options has-items">

                                                    <div class="item" data-value="london">18.08.2018</div>

                                                    <input type="text" autocomplete="off" tabindex="" style="width: 4px;">

                                                </div>

                                                <div class="selectize-dropdown single location-select big-input searchspecial" style="display: none; width: 152px; top: 39px; left: 0px;">

                                                    <div class="selectize-dropdown-content"></div>

                                                </div>

                                            </div>

                                        </div>

                                        <div class="col-md-12 pdtop10">

                                            <textarea id="editor2" placeholder="Lorem ipsum dolor sit amet, consectetur adipsiicis, sed do eisuaid labore et dolore magna aliqua. Ut aneim adms imnir pevnit ventinima laboris ullamanco laboris nisi ut aliquimp ex ae comodo cosequat."></textarea>

                                        </div>

                                        <div class="col-md-5">

                                            <div class="button-wrapper btnwrspe">

                                                <a href="#">VIEW RESULTS FOR THIS SEARCH</a>

                                            </div>

                                        </div>

                                        <div class="col-md-4">

                                            <div class="button-wrapper btnwrsp btnwrsp fr">

                                                <a href="#"  data-toggle="modal" data-target="#myModal" class="text-red" >REMOVE FROM WANTLIST</a>

                                            </div>

                                        </div>

                                        <div class="col-md-3">

                                            <div class="button-wrapper btnwrspe fr">

                                                <a href="#" data-toggle="modal" data-target="#updatemyModal">UPDATE NEED</a>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                            <!--PAGINATION-->
                            <div class="col-md-12">

                                <div class="pagination-wrap m-b-30">

                                    <ul>
                                        <li>
                                            <a href="#"><i class="material-icons">&#xE5C4;</i></a>
                                        </li>
                                        <li>
                                            <a href="#">1</a>
                                        </li>
                                        <li>
                                            <a href="#">2</a>
                                        </li>
                                        <li>
                                            <a href="#">3</a>
                                        </li>
                                        <li>
                                            <a href="#">4</a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="material-icons">&#xE5C8;</i></a>
                                        </li>
                                    </ul>

                                </div>

                            </div>

                        </div>
                        
                        <div id="change-email-frequency" class="col-4 col-lg-4 col-md-12 col-sm-12 col-12">
                        
                            <div class="mb-5">

                                <div class="border bg-grey p-5">

                                    <div class="row">

                                        <div class="col-md-12 pd-bt-4">
                                            <span class="font-400 mb-4 fs-19"><i class="fas fa-bell"></i> Change email frequency</span>
                                        </div>

                                        <div class="col-md-12 pdtp4">
                                            <small class="fsm12">You'll be sent emails about any sharing offers that meet your search criteria. <b>Currently you're being emailed weekly.</b></small>
                                        </div>

                                        <div class="col-md-6 pdtp4">
                                            <button type="submit" class="btn btn-secondary bg-orange text-white text-uppercase font-700 font-open-sans px-5 py-4 w-100" style="font-size:13px;" data-dismiss="modal" data-toggle="modal" data-target="#search-to-watchlist-modal" >Change</button>
                                        </div>

                                    </div>

                                </div>

                            </div>
                            
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>


<!--MODALS-->

<div class="modal fade" id="myModal" role="dialog" data-backdrop="static">
        
        <div class="modal-dialog modal-lg">
        
            <div class="modal-content">
            <div class="modal-header d-flex justify-content-start">
            <h4 class="modal-title"><i class="fas fa-exclamation-triangle"></i> You are about to remove a Need ad.</h4>
            </div>
            <div class="modal-body">
            <p class="fz-12-">Once you've removed a search, you will no longer receive emails with updates about new sharing offers that
            </p>
            <p class="fz-12-"> fall into those search parameters. You can always add it again in the future.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btnwb" data-dismiss="modal">Cancel this action</button>
                <button type="button" class="btn btn-danger text-white font-700 font-open-sans px-5 py-3" data-dismiss="modal">REMOVE FROM WANTLIST</button>
            </div>
        </div>

        </div>
        
    </div>
    
<div class="modal fade" id="updatemyModal" role="dialog" data-backdrop="static">
        
        <div class="modal-dialog modal-lg">
        
            <div class="modal-content">
            
                <div class="modal-header d-flex justify-content-start">
            
                <h4 class="modal-title">You are about to update a Need ad.</h4>
            
            </div>
            <div class="modal-body">
            <p class="fz-12-">Once you've updated it, you will no longer receive emails with the previous search parameters. You can add another back in your dashboard.
            </p>
            <p class="fz-12-">You can add as many searches to your wantlists as you want!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btnwb" data-dismiss="modal">Cancel this action</button>
                
                <button type="button" class="btn btn-secondary bg-orange text-white text-uppercase font-700 font-open-sans px-5 py-3" data-dismiss="modal">UPDATE A NEED AD</button>
            </div>
        </div>

        </div>
    </div>

<!--modal approve-->

<div class="modal fade" id="write-review-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

	<div class="modal-dialog rounded-0" role="document">
		
		<div class="modal-content rounded-0 bg-grey">
			
			<div class="modal-header rounded-0 border-0">
				
				<h4 class="modal-title text-emperor font-700 ">Review Frances</h4>
				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				
				<span aria-hidden="true">&times;</span>
				
				</button>
				
			</div>
			
			<div class="modal-body">
				<div class="row">
					<div class="col-12 col-sm-6">
						<div class="user-container">
							<div class="user-block">
								<div class="avatar-image-container">
									<div class="avatar-image">
										<!--agent-status & status-online styles in style.less-->
										<a href="#chatorsomething">
											<img src="./images/userpicbig.png" alt="agent-photo">
										</a>
									</div>
								</div>
							</div>
							<div class="user-block">
								<div class="user-name">
									Frances
								</div>
								<div class="user-stars">
									<div class="stars" data-stars="3">
										<i class="material-icons star">star</i>
										<i class="material-icons star">star</i>
										<i class="material-icons star">star</i>
										<i class="material-icons star">star</i>
										<i class="material-icons star">star</i>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12 col-sm-6">
						<ul class="no-style-list">
							<li>
								<span>Metric #1 </span>
								<div class="stars orange-stars" data-stars="0">
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
								</div>
							</li>
							<li>
								<span>Metric #1 </span>
								<div class="stars orange-stars" data-stars="0">
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
								</div>
							</li>
						</ul>
					</div>
				</div>
				
				<textarea placeholder="Tell others what Frances was like."></textarea>
				
			</div>
			
			<div class="modal-footer border-0">
				
				<button type="button" class="btn btn-secondary floating-button bg-orange text-white text-uppercase font-700 font-open-sans px-5 py-3" style="font-size:13px;" data-dismiss="modal" data-toggle="modal" data-target="#approval-confirmation-modal" >Post review</button>
				
				
			</div>
			
		</div>
		
	</div>
	
</div>

<!--modal approval confirmation-->

<div class="modal fade" id="search-to-watchlist-modal" role="dialog" data-backdrop="static">
        
        <div class="modal-dialog modal-lg">
            
        <div class="modal-content bg-light-grey">
            
            <div class="modal-header d-flex justify-content-start">
                
            
                <h4 class="modal-title font-900 text-emperor"> Change email frecuency</h4>
            
            </div>
            
            <div class="modal-body">
                
                <div class="modal-block">
                    
                    <div class="mb-4">
                        <div class="row">
                            <div class="col-12">
                                <div class="property-features-block">
                                    <label class="font-open-sans font-400 text-emperor bg-transparent mb-3">How frequently would you like to receive emails about new sharing offers?</label>
                                        <div class="checkboxes-block">
                                            <div>
                                                <label class="radio-label">Daily
                                                    <input type="radio" name="condition">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <br>
                                            </div>
                                            <div>
                                                <label class="radio-label">Weekly
                                                    <input type="radio" name="condition">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <br>
                                            </div>
                                            <div>
                                                <label class="radio-label">Monthly
                                                    <input type="radio" name="condition">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    
                    <div>
                        <div class="row">
                            
                            <div class="col-12">
                                
                                <div class="property-features-block">
                                    
                                    
                                    <label class="font-open-sans font-400 text-emperor bg-transparent mb-3">When would you like to stop receiving emails? </label>
                                        
                                    <div class="checkboxes-block">
                                            
                                        <div>
                                                
                                            <label class="radio-label">On Date
                                                    
                                                <input type="radio" name="datecheck" class="date">
                                                    
                                                <span class="checkmark"></span>
                                                
                                            </label>
                                                
                                            <br>
                                            
                                        </div>
                                            
                                        <div>
                                                
                                            <label class="radio-label">Never
                                                    
                                                <input type="radio" name="datecheck" class="never" >
                                                    
                                                <span class="checkmark"></span>
                                                
                                            </label>
                                                
                                            <br>
                                            
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                                
                            </div>
                        
                        </div>
                    
                    </div>
                    
                    <div class="mb-4 w-25 on-date">
                        
                        <p>On Date</p>
                        <input type="text" class="w-100 datepick">
                        
                    </div>
                    <div>
                        <p style="font-size:12px;">We will only email you if new sharing offers are published</p>
                    </div>
                    
                </div>
                
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default btnwb bg-light-grey" data-dismiss="modal">Discard changes</button>
                <button type="button" class="btn btn-secondary bg-orange text-white text-uppercase font-700 font-open-sans px-5 py-3" data-dismiss="modal"  data-toggle="modal">Save Changes</button>
            </div>
        </div>

        </div>

    </div>
    
<div class="modal fade" id="confirm-wantlist-modal" role="dialog">
        <div class="modal-dialog modal-lg">
        <div class="modal-content bg-light-grey">
            <div class="modal-header">
            <h4 class="modal-title">Wantlist created.</h4>
            <button type="button" class="btn btn-specials-oj"  data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i></button>
            </div>
            <div class="modal-body">
                <p>You can manage your wantlist options from your dashboard, just head to Needs tab!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary bg-orange text-white text-uppercase font-700 font-open-sans px-5 py-3" data-dismiss="modal">Go to your dashboard</button>
            </div>
        </div>

        </div>
    </div>


<div class="modal fade" id="create-need-confirmed" role="dialog" data-backdrop="static" data-keyboard="false">
        
    <div class="modal-dialog modal-lg">
        
        <div class="modal-content">
            
            <div class="modal-header d-flex justify-content-start">
            
                <h4 class="modal-title"> Your need ad has been posted!</h4>
            
            </div>
                
            <div class="modal-body">
                
            </div>
            
            <div class="modal-footer">
                    
                <button type="button" class="btn btn-secondary bg-orange text-white text-uppercase font-700 font-open-sans px-5 py-3" data-dismiss="modal">Close this</button>
                
            </div>
            
        </div>
    
    </div>
    
</div>


<?php include 'scripts.php'; ?>

</body>

</html>