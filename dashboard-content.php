
<!--DASHBOARD-->

<div id="dashboard" class="js-content-blocks p-4">
				
    <h1 class="account-header-top">Dashboard</h1>
                
    <div class="alert bg-grey alert-dismissible p-5 position-relative" role="alert">
                                
                                
                   
        <h4 class="text-emperor font-400">Welcome to your personal information page</h4>
                    
        <br>
                                
                    
        <p>Luviat is about sharing and community. Help other people get to know what you’re about. In this section, you can build a profile with details such as your travel history and the languages you speak. It will describe you as a traveller and sharer and, will help people feel like they know you.</p>
                                
                                
                    
        <button type="button" class="close bg-orange position-absolute " data-dismiss="alert" aria-label="Close">
                        
            <span aria-hidden="true">&times;</span>
                    
        </button>
                            
                            
                
    </div>
                
    <div class="account-header-tabs mb-5 border-bottom">
					
        <ul class="nav" id="myTab" role="tablist">
						
            <li class="nav-item">
							
                <a class="nav-link" id="about-you-tab" data-toggle="tab" href="#about-you" role="tab" aria-controls="about-you" aria-selected="true">About you</a>
						
            </li>
						
            <li class="nav-item">
							
                <a class="nav-link" id="travel-history-tab" data-toggle="tab" href="#travel-history" role="tab" aria-controls="travel-history" aria-selected="false">Travel history</a>
						
            </li>
						
            <li class="nav-item">
							
                <a class="nav-link" id="langua-tab" data-toggle="tab" href="#langua" role="tab" aria-controls="langua" aria-selected="false">Languages</a>
						
            </li>
						
            <li class="nav-item">
							
                <a class="nav-link" id="community-tab" data-toggle="tab" href="#community" role="tab" aria-controls="community" aria-selected="false">Community</a>
						
            </li>
						
            <li class="nav-item">
							
                <a class="nav-link" id="licensesqual-tab" data-toggle="tab" href="#licensesqual" role="tab" aria-controls="licensesqual" aria-selected="false">Licenses & Qualifications</a>
						
            </li>
					
        </ul>
				
    </div>
                
    <div class="tab-content">
                    
        <div class="tab-pane" id="reviews-by-you" role="tabpanel" aria-labelledby="about-you">
						<div class="row">
							<div class="col-12">
								<div class="account-property m-b-30">
									
									<div class="m-b-30">
										<!--AGENT BLOCK BEGIN-->
										<div class="agent-block-wrapper-header">
											Rewrites to write
										</div>
										<!--AGENT BLOCK END-->
										<div class="agent-block-wrapper">
											<div class="agent-block-agent-img">
												<div class="image-wrap">
													<!--agent-status & status-online styles in style.less-->
													<div class="agent-status" title="online">
													</div>
													<a href="#chatorsomething">
														<img src="./images/userpicbig.png" alt="agent-photo">
													</a>
												</div>
											</div>
											<div class="agent-block-agent-information">
												<div class="row">
													<div class="block-left col-3">
														<h2>Pauline Saunders</h2>
													</div>
													<div class="block-right col-9">
														<span class="small-text orange-text">Ski Boots size 10</span>
													</div>
												</div>
												<div class="row">
													<div class="block-left col-3">
														<div class="stars" data-stars="3">
															<i class="material-icons star">star</i>
															<i class="material-icons star">star</i>
															<i class="material-icons star">star</i>
															<i class="material-icons star">star</i>
															<i class="material-icons star">star</i>
														</div>
													</div>
													<div class="block-right col-9">
														<div class="text-container">
															<div class="write-review">
																<div class="review"></div>
																<div class="review"></div>
																<div class="review"></div>
																<div class="review"></div>
															</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="block-left col-3">
														<div class="small-text align-bottom">
															July 08, 2018
														</div>
													</div>
													<div class="block-right col-9">
														<div class="small-text">
															borrow date(s): 12.08.2018 - 15.08.2018
														</div>
													</div>
												</div>
												<div class="send-button">
													<a href="#" data-toggle="modal" data-target="#write-review-modal" class="button button-link-primary-icon" >write review</a>
													
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
                
    <div class="tab-pane" id="travel-history" role="tabpanel" aria-labelledby="travel-history-tab">
					<div class="row">
                        <div class="account-property m-b-30 pdt20 col-md-10">
                            <div class="m-b-30 bg-grey">
                                
                                <div class="agent-block-wrapper-header bg-grey p-4">
                                    
                                    <h4 class="font-700" >Travel history</h4>
                                    
                                </div>
                                
                                <!--AGENT BLOCK BEGIN-->
                                <div class="col-md-12 bg-grey specialcont">
                                    <div class="col-md-12">
                                        <div class="pdb4"><small >I am currently:</small></div>
                                        <div class="row pdb4">
                                            <div class="col-4">
                                                <input type="checkbox" name="planning" id="planning" class="css-checkbox">
                                                <label for="planning" class="css-label">Planning for a comming trip.</label>
                                            </div>
                                            <div class="col-3">
                                                <input type="checkbox" name="travelling" id="travelling" class="css-checkbox">
                                                <label for="travelling" class="css-label">Travelling</label>
                                            </div>
                                            <div class="col-5">
                                                <input type="checkbox" name="connect" id="connect" class="css-checkbox">
                                                <label for="connect" class="css-label">Just looking forward to connecting and sharing with visitors to my city.</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="pdb4"><small >Places I've travelled to:</small></div>
                                    </div>
                                    <div class="col-md-12 pdb4">
                                        <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="Enter a city or country" tabindex="-1" >
                                    </div>
                                    <div class="col-md-6 pdb4">
                                        <div class="button-wrapper btnwrspe">
                                            <a href="#">ADD MORE</a>
                                        </div>
                                    </div>
                                    <div class="col-md-6 pdb4">
                                        <div class="button-wrapper btnwrspe fr">
                                            <a href="#" >REMOVE</a>
                                        </div>
                                    </div>
                                    <div class="col-md-12 ">
                                        <div class="pdb4"><small >Places I'm planning to travel to:</small></div>
                                    </div>
                                    <div class="col-md-12 pdb4">
                                        <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="Enter a city or country" tabindex="-1" >
                                    </div>
                                    <div class="col-md-6 pdb4">
                                        <div class="button-wrapper btnwrspe">
                                            <a href="#">ADD MORE</a>
                                        </div>
                                    </div>
                                    <div class="col-md-6 pdb4">
                                        <div class="button-wrapper btnwrspe fr">
                                            <a href="#" >REMOVE</a>
                                        </div>
                                    </div>
                                </div>
                                <!--AGENT BLOCK END-->
                            </div>
                                
                    </div>
                        <div class="col-md-10">
							<div class="account-property m-b-30 ">
								
								<div class="m-b-30 ">
									<div class="row no-header">
										<div class="col-12">
                                            <div class="col-md-6">
                                            </div>
                                            <div class="col-md-6">
                                                <div class="button-wrapper btnwrspe fr">
                                                    <a href="#" ><i class="fas fa-check-double"></i> UPDATE PROFILE</a>
                                                </div>
                                            </div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
    <div class="tab-pane" id="licensesqual" role="tabpanel" aria-labelledby="licensesqual-tab">
					<div class="row">
                        <div class="account-property m-b-30 pdt20 col-md-10">
                            <div class="m-b-30 bg-grey">
                                
                                
                                <div class="agent-block-wrapper-header bg-grey p-4">
                                    
                                    <h4 class="font-700" >Licenses & qualifications</h4>
                                    
                                </div>
                                
                                <!--AGENT BLOCK BEGIN-->
                                <div class="specialcont col-md-12 bg-grey">
                                    <div class="col-md-12">
                                        <div class="pdb4"><small >What technical or sports qualification do you have?</small></div>
                                    </div>
                                    <div class="col-md-12 pdb4">
                                        <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="e.g scuba diving, sky diving, etc.." tabindex="-1" >
                                    </div>
                                </div>
                                <!--AGENT BLOCK END-->
                            </div>
                                
                    </div>
                        <div class="col-md-10">
							<div class="account-property m-b-30 ">
								
								<div class="m-b-30 ">
									<div class="row no-header">
										<div class="col-12">
                                            <div class="col-md-6">
                                            </div>
                                            <div class="col-md-6">
                                                <div class="button-wrapper btnwrspe fr">
                                                    <a href="#" ><i class="fas fa-check-double"></i> UPDATE PROFILE</a>
                                                </div>
                                            </div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
    <div class="tab-pane" id="community" role="tabpanel" aria-labelledby="community-tab">
					<div class="row">
                        <div class="account-property m-b-30 pdt20 col-md-10">
                            <div class="m-b-30 bg-grey">
                                
                                <div class="agent-block-wrapper-header bg-grey p-4">
                                    
                                    <h4 class="font-700">Community</h4>
                                    
                                </div>
                                
                                <!--AGENT BLOCK BEGIN-->
                                <div class="specialcont col-md-12 bg-grey">
                                    <div class="col-md-12">
                                        <div class="pdb4"><small >Besides standard sharing offers (items, services, experiences) what else can you share with the community?</small></div>
                                    </div>
                                    <div class="col-md-12 pdb4">
                                        <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="Placeholder" tabindex="-1" >
                                    </div>
                                    <div class="col-md-12">
                                        <div class="pdb4"><small >What can the community share with you?</small></div>
                                    </div>
                                    <div class="col-md-12 pdb4">
                                        <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="Placeholder" tabindex="-1" >
                                    </div>
                                </div>
                                <!--AGENT BLOCK END-->
                            </div>
                                
                    </div>
                        <div class="col-md-10">
							<div class="account-property m-b-30 ">
								
								<div class="m-b-30 ">
									<div class="row no-header">
										<div class="col-12">
                                            <div class="col-md-6">
                                            </div>
                                            <div class="col-md-6">
                                                <div class="button-wrapper btnwrspe fr">
                                                    <a href="#" ><i class="fas fa-check-double"></i> UPDATE PROFILE</a>
                                                </div>
                                            </div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
    <div class="tab-pane" id="langua" role="tabpanel" aria-labelledby="langua-tab">
					<div class="row">
                        <div class="account-property m-b-30 pdt20 col-md-10">
                            <div class="m-b-30 bg-grey">
                                
                                <div class="agent-block-wrapper-header bg-grey p-4">
                                    
                                    <h4 class="font-700">Languages</h4>
                                    
                                </div>
                                
                                <!--AGENT BLOCK BEGIN-->
                                <div class="specialcont col-md-12 bg-grey">
                                    <div class="col-md-12">
                                        <div class="pdb4"><small >Native language:</small></div>
                                    </div>
                                    <div class="col-md-12 pdb4">
                                        <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="Enter a language" tabindex="-1" >
                                    </div>
                                    <div class="col-md-8">
                                        <div class="pdb4"><small >Other languages spoken:</small></div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="pdb4"><small >Level:</small></div>
                                    </div>
                                    <div class="col-md-8 pdb4">
                                        <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="Enter a language" tabindex="-1" >
                                    </div>
                                    <div class="col-md-4 pdb4">
                                        <select form="search-form-1" name="location" class="location-select big-input searchspecial selectized" data-placeholder="location" tabindex="-1" style="display: none;"><option value="london" selected="selected">Paris, FR</option>
                                        </select>
                                        <div class="selectize-control location-select big-input searchspecial single">
                                            <div class="selectize-input items full has-options has-items">
                                                <div class="item" data-value="london"></div>
                                                <input type="text" autocomplete="off" tabindex="" style="width: 4px;">
                                            </div>
                                            <div class="selectize-dropdown single location-select big-input searchspecial" style="display: none; width: 152px; top: 39px; left: 0px;">
                                                <div class="selectize-dropdown-content"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 pdb4">
                                        <div class="button-wrapper btnwrspe">
                                            <a href="#">ADD MORE</a>
                                        </div>
                                    </div>
                                    <div class="col-md-6 pdb4">
                                        <div class="button-wrapper btnwrspe fr">
                                            <a href="#" >REMOVE</a>
                                        </div>
                                    </div>
                                </div>
                                <!--AGENT BLOCK END-->
                            </div>
                                
                    </div>
                        <div class="col-md-10">
							<div class="account-property m-b-30 ">
								
								<div class="m-b-30 ">
									<div class="row no-header">
										<div class="col-12">
                                            <div class="col-md-6">
                                            </div>
                                            <div class="col-md-6">
                                                <div class="button-wrapper btnwrspe fr">
                                                    <a href="#" ><i class="fas fa-check-double"></i> UPDATE PROFILE</a>
                                                </div>
                                            </div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
    <div class="tab-pane" id="about-you" role="tabpanel" aria-labelledby="about-you-tab">
					
        <div class="row">
                        <div class="account-property m-b-30 pdt20 col-md-10">
                            <div class="m-b-30 bg-grey">
                                <div class="agent-block-wrapper-header bg-grey p-4">
                                    <h4 class="font-700" >Personal</h4>
                                </div>
                                <!--AGENT BLOCK BEGIN-->
                                <div class="specialcont col-md-12 bg-grey">
                                    
                                    <div class="col-md-12 mb-5 ">
                                        <p>Describe yourself:</p>
                                        <p><small >Tell them about the things you like: What are 5 things you can't live without? Share your favourite travel destinations, books, movies, shows, music,food. <br>Tell them what it's like to spend time with you. What's your style of travelling? Tell them about you: Do you have a life motto?</small></p>
                                        
                                        <textarea placeholder="Describe the type of traveller you are or aspire to be." maxlength="600" data-limit="600" rows="8" ></textarea>
                                        <span class="countdown"><small>600 Characters remaining</small></span><br><br>
                                        
                                        
                                        
                                    </div>
                                    <div class="col-md-12">
                                        <div class="pdb4"><small >What is something you are interested in? (Add up to 8 - we recommend at least 3)</small></div>
                                    </div>
                                    <div class="col-md-12 pdb4">
										<select form="search-form-1" name="location" class="location-select big-input searchspecial selectized" data-placeholder="location" tabindex="-1" style="display: none;"><option value="london" selected="selected">Paris, FR</option>
										</select>
										<div class="selectize-control location-select big-input searchspecial single">
											<div class="selectize-input items full has-options has-items">
												<div class="item" data-value="london">Paris, FR</div>
												<input type="text" autocomplete="off" tabindex="" style="width: 4px;">
											</div>
											<div class="selectize-dropdown single location-select big-input searchspecial" style="display: none; width: 152px; top: 39px; left: 0px;">
												<div class="selectize-dropdown-content"></div>
											</div>
										</div>
                                    </div>
                                    <div class="col-md-6 pdb4">
                                        <div class="button-wrapper btnwrspe">
                                            <a href="#">ADD MORE</a>
                                        </div>
                                    </div>
                                    <div class="col-md-6 pdb4">
                                        <div class="button-wrapper btnwrspe fr">
                                            <a href="#" >REMOVE</a>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="pdb4"><small >Where is your favourite travel destination?</small></div>
                                    </div>
                                    <div class="col-md-12 pdb4">
                                        <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="" tabindex="-1" >
                                    </div>
                                    <div class="col-md-12">
                                        <div class="pdb4"><small >What do you like to do when you travel?</small></div>
                                    </div>
                                    <div class="col-md-12 pdb4">
                                        <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="" tabindex="-1" >
                                    </div>
                                    <div class="col-md-12">
                                        <div class="pdb4"><small >What can't you travel without?</small></div>
                                    </div>
                                    <div class="col-md-12 pdb4">
                                        <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="" tabindex="-1" >
                                    </div>
                                    <div class="col-md-12">
                                        <div class="pdb4"><small >What's your favourite part about travelling?</small></div>
                                    </div>
                                    <div class="col-md-12 pdb4">
                                        <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="" tabindex="-1" >
                                    </div>
                                    <div class="col-md-12">
                                        <div class="pdb4"><small >What's your least favourite part about travellling?</small></div>
                                    </div>
                                    <div class="col-md-12 pdb4">
                                        <input form="search-form-1" name="location" class="location-select big-input inputs" placeholder="" tabindex="-1" >
                                    </div>

                                </div>
                                <!--AGENT BLOCK END-->
                            </div>
                                
                    </div>
                        <div class="col-md-10">
							<div class="account-property m-b-30 ">
								
								<div class="m-b-30 ">
									<div class="row no-header">
										<div class="col-12">
                                            <div class="col-md-6">
                                            </div>
                                            <div class="col-md-6">
                                                <div class="button-wrapper btnwrspe fr">
                                                    <a href="#" ><i class="fas fa-check-double"></i> UPDATE PROFILE</a>
                                                </div>
                                            </div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				
    </div>
			


    </div>


</div>


<!--SUBMIT A SHARING OFFER-->

<div id="submit-sharing" class="js-content-blocks p-4">
				
    <h1 class="account-header-top">Submit a sharing offer</h1>
		
</div>

<!--SUBMIT A SHARING OFFER-->

<div id="my-sharing-offers" class="js-content-blocks p-4">
				
    <h1 class="account-header-top">My sharing offers</h1>
		
</div>


<!--WANTLIST-->

<div id="wantlist" class="js-content-blocks p-4">
				
    <h1 class="account-header-top">Wantlist</h1>
		
</div>


<!--SHARING REQUESTS-->
            
<div id="sharing-requests" class="js-content-blocks p-4">
				
    <h1 class="account-header-top text-emperor border-0">Sharing Requests</h1>
    
    <div class="tab-content">

        <!--BREADCRUMB-->
        
        <div class="row">
        
            <div class="col-lg-12">
            
                <nav aria-label="breadcrumb">
  
                    <ol class="breadcrumb">
                    
                        <li class="breadcrumb-item"><a href="#" onclick="window.history.back()" >Back</a></li>
                    
                        <li class="breadcrumb-item active" aria-current="page">Sharing Requests</li>
                
                    </ol>
            
                </nav>
            
            </div>
    
        </div>				
        
        <div id="reviews-by-you" role="tabpanel" >
						
            <div class="row">
							
                <div class="col-md-8">
								
                    <div class="account-property m-b-30">
										
                                        
                        <!--SHARING REQUEST START-->
                                        
                        <div class="agent-block-wrapper mb-4">
                                            
                            <div class="agent-block-agent-img">
                                                
                                <div class="image-wrap">
                                                    <!--agent-status & status-online styles in style.less-->
                                                    
                                    <div class="agent-status" title="online">
                                                    
                                    </div>
                                                    
                                    <a href="#chatorsomething">
                                                        
                                        <img src="./images/userpicbig.png" alt="agent-photo">
                                                    
                                    </a>
                                                
                                </div>
                                            
                            </div>
                                            
                            <div class="agent-block-agent-information">
                                                
                                <div class="row">
                                                    
                                    <div class="block-left col-3">
                                                        
                                        <h2 class="text-emperor font-400" >Frances</h2>
                                                    
                                    </div>
                                                    
                                    <div class="block-right col-9 mb-4">
                                                        
                                        <span class="small-text text-orange mb-4">Frances is requesting to borrow your (sharing offer name)</span>
                                                    
                                    </div>
                                                
                                </div>
                                                
                                <div class="row mb-4">
                                                    
                                    <div class="block-left col-3">
                                                        
                                        <div class="stars" data-stars="3">
                                                            
                                            <i class="material-icons star">star</i>
                                                            
                                            <i class="material-icons star">star</i>
                                                            
                                            <i class="material-icons star">star</i>
                                                            
                                            <i class="material-icons star">star</i>
                                                            
                                            <i class="material-icons star">star</i>
                                                        
                                        </div>
                                                    
                                    </div>
                                                    
                                    <div class="block-right col-6">
                                                        
                                        <div class="text-container">
                                                            
                                            Hey Vince! I'm Frances and I'm travelling to Canberra with my partner. We're both into skling and would love to borrow your ski boots for a weekend trip to Perisher. We have a few questions: are the ski boots size 11 womens or? Do....
                                                        
                                        </div>
                                                    
                                    </div>
                                                
                                </div>
                                                
                                <div class="row d-flex align-items-center">
                                                    
                                    <div class="block-left col-3">
                                                        
                                        <div class="font-700 text-emperor">
                                                            
                                            July 08, 2018
                                                        
                                        </div>
                                                    
                                    </div>
                                                    
                                    <div class="block-right col-4">
                                                        
                                        <div class="font-700 text-emperor">
                                                            
                                            <span>Borrow date(s):</span>
                                                            
                                            <br>
                                                            
                                            <span>12.08.2018 - 15.08.2018</span>
                                                        
                                        </div>
                                                    
                                    </div>
                                                    
                                                    
                                    <div class="block-right col-5">
                                                        
                                        <div class="button-wrapper btnwrspe fr">
                                                            
                                            <a href="send-message.php">VIEW</a>
                                                        
                                        </div>
                                                        
                                        <div class="button-wrapper btnwrsp btnwrsp fr">
                                                            
                                            <span href="#" class="font-700 text-blue-requested text-uppercase">REQUESTED</span>
                                                        
                                        </div>
                                                    
                                    </div>
                                                
                                </div>
                                            
                            </div>
                                        
                        </div>
                                        
                        <!--SHARING REQUEST START-->
                                        
                        <div class="agent-block-wrapper mb-4">
                                            <div class="agent-block-agent-img">
                                                <div class="image-wrap">
                                                    <!--agent-status & status-online styles in style.less-->
                                                    <div class="agent-status" title="online">
                                                    </div>
                                                    <a href="#chatorsomething">
                                                        <img src="./images/userpicbig.png" alt="agent-photo">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="agent-block-agent-information">
                                                <div class="row">
                                                    <div class="block-left col-3">
                                                        <h2 class="text-emperor font-400" >George</h2>
                                                    </div>
                                                    <div class="block-right col-9 mb-4">
                                                        <span class="small-text text-orange mb-4">Frances is requesting to borrow your (sharing offer name)</span>
                                                    </div>
                                                </div>
                                                <div class="row mb-4">
                                                    <div class="block-left col-3">
                                                        <div class="stars" data-stars="3">
                                                            <i class="material-icons star">star</i>
                                                            <i class="material-icons star">star</i>
                                                            <i class="material-icons star">star</i>
                                                            <i class="material-icons star">star</i>
                                                            <i class="material-icons star">star</i>
                                                        </div>
                                                    </div>
                                                    <div class="block-right col-6">
                                                        <div class="text-container">
                                                            Hey Vince! I'm Frances and I'm travelling to Canberra with my partner. We're both into skling and would love to borrow your ski boots for a weekend trip to Perisher. We have a few questions: are the ski boots size 11 womens or? Do....
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row d-flex align-items-center">
                                                    <div class="col-3">
                                                        <div class="font-700 text-emperor">
                                                            July 08, 2018
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="font-700 text-emperor">
                                                            <span>Borrow date(s):</span>
                                                            <br>
                                                            <span>12.08.2018 - 15.08.2018</span>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-5 text-right">
                                                        <div class="button-wrapper btnwrspe fr">
                                                            <a href="single-offer.php" target="_blank">VIEW</a>
                                                        </div>
                                                        <div class="button-wrapper btnwrsp btnwrsp fr">
                                                            <span class="font-700 text-green text-uppercase" >Accepted</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                        <!--SHARING REQUEST START-->
                                        
                        <div class="agent-block-wrapper mb-4">
                                            <div class="agent-block-agent-img">
                                                <div class="image-wrap">
                                                    <!--agent-status & status-online styles in style.less-->
                                                    <div class="agent-status" title="online">
                                                    </div>
                                                    <a href="#chatorsomething">
                                                        <img src="./images/userpicbig.png" alt="agent-photo">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="agent-block-agent-information">
                                                <div class="row">
                                                    <div class="block-left col-3">
                                                        <h2 class="text-emperor font-400" >George</h2>
                                                    </div>
                                                    <div class="block-right col-9 mb-4">
                                                        <span class="small-text text-orange mb-4">Frances is requesting to borrow your (sharing offer name)</span>
                                                    </div>
                                                </div>
                                                <div class="row mb-4">
                                                    <div class="block-left col-3">
                                                        <div class="stars" data-stars="3">
                                                            <i class="material-icons star">star</i>
                                                            <i class="material-icons star">star</i>
                                                            <i class="material-icons star">star</i>
                                                            <i class="material-icons star">star</i>
                                                            <i class="material-icons star">star</i>
                                                        </div>
                                                    </div>
                                                    <div class="block-right col-6">
                                                        <div class="text-container">
                                                            Hey Vince! I'm Frances and I'm travelling to Canberra with my partner. We're both into skling and would love to borrow your ski boots for a weekend trip to Perisher. We have a few questions: are the ski boots size 11 womens or? Do....
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row d-flex align-items-center">
                                                    <div class="col-3">
                                                        <div class="font-700 text-emperor">
                                                            July 08, 2018
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="font-700 text-emperor">
                                                            <span>Borrow date(s):</span>
                                                            <br>
                                                            <span>12.08.2018 - 15.08.2018</span>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-5 text-right">
                                                        <div class="button-wrapper btnwrspe fr">
                                                            <a href="#">VIEW</a>
                                                        </div>
                                                        <div class="button-wrapper btnwrsp btnwrsp fr">
                                                            <span  class="font-700 text-red text-uppercase" >Denied</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                        
                        <!--SHARING REQUEST START-->
                                        
                        <div class="agent-block-wrapper mb-4">
                                            
                            <div class="agent-block-agent-img">
                                                
                                <div class="image-wrap">
                                                    <!--agent-status & status-online styles in style.less-->
                                                    
                                    <div class="agent-status" title="online">
                                                    
                                    </div>
                                                    
                                    <a href="#chatorsomething">
                                                        
                                        <img src="./images/userpicbig.png" alt="agent-photo">
                                                    
                                    </a>
                                                
                                </div>
                                            
                            </div>
                                            
                            <div class="agent-block-agent-information">
                                                
                                <div class="row">
                                                    
                                    <div class="block-left col-3">
                                                        
                                        <h2 class="text-emperor font-400" >George</h2>
                                                    
                                    </div>
                                                    
                                    <div class="block-right col-9 mb-4">
                                                        
                                        <span class="small-text text-orange mb-4">Frances is requesting to borrow your (sharing offer name)</span>
                                                    
                                    </div>
                                                
                                </div>
                                                
                                <div class="row mb-4">
                                                    
                                    <div class="block-left col-3">
                                                        
                                        <div class="stars" data-stars="3">
                                                            
                                            <i class="material-icons star">star</i>
                                                            
                                            <i class="material-icons star">star</i>
                                                            
                                            <i class="material-icons star">star</i>
                                                            
                                            <i class="material-icons star">star</i>
                                                            
                                            <i class="material-icons star">star</i>
                                                        
                                        </div>
                                                    
                                    </div>
                                                    
                                    <div class="block-right col-6">
                                                        
                                        <div class="text-container">
                                                            
                                            Hey Vince! I'm Frances and I'm travelling to Canberra with my partner. We're both into skling and would love to borrow your ski boots for a weekend trip to Perisher. We have a few questions: are the ski boots size 11 womens or? Do....
                                                        
                                        </div>
                                                    
                                    </div>
                                                
                                </div>
                                                
                                <div class="row d-flex align-items-center">
                                                    
                                    <div class="col-3">
                                                        
                                        <div class="font-700 text-emperor">
                                                            
                                            July 08, 2018
                                                        
                                        </div>
                                                    
                                    </div>
                                                    
                                    <div class="col-4">
                                                        
                                        <div class="font-700 text-emperor">
                                                            
                                            <span>Borrow date(s):</span>
                                                            
                                            <br>
                                                            
                                            <span>12.08.2018 - 15.08.2018</span>
                                                        
                                        </div>
                                                    
                                    </div>
                                                    
                                    
                                                    
                                    <div class="col-5 text-right">
                                                        
                                        <div class="button-wrapper btnwrspe fr">
                                                            
                                            <a href="single-offer.php" target="_blank" >VIEW</a>
                                                        
                                        </div>
                                                        
                                        <div class="button-wrapper btnwrsp btnwrsp fr">
                                                            
                                            <span class="font-700 text-cancelled text-uppercase">Cancelled</span>
                                                        
                                        </div>
                                                    
                                    </div>
                                                
                                </div>
                                            
                            </div>
                                        
                        </div>
                                    
                                    
                        <div class="pagination-wrap m-b-30">
                                    
                            <ul>
                                        
                                <li>
                                            
                                    <a href="#"><i class="material-icons">&#xE5C4;</i></a>

                                </li>
                                        
                                <li>
                                            
                                    <a href="#">1</a>
                                        
                                </li>
                                        
                                <li>
                                            
                                    <a href="#">2</a>
                                        
                                </li>
                                       
                                <li>
                                            
                                    <a href="#">3</a>
                                        
                                </li>
                                        
                                <li>
                                            
                                    <a href="#">4</a>
                                        
                                </li>
                                        
                                <li>
                                            
                                    <a href="#"><i class="material-icons">&#xE5C8;</i></a>
                                        
                                </li>
                                    
                            </ul>
                                
                        </div>
                                
                    </div>
                            
                </div>
                            
                            
                <div class="col-md-4">
                                
                    <div class="account_properties_counters_list">
                                    
                        <a href="#" class="item">
                                        
                            <span class="badge rented"></span> REQUESTED
                                        
                            <span class="count">1</span>
                                    
                        </a>
                                    
                                    
                        <a href="#" class="item">
                                        
                            <span class="badge awaiting-admin"></span> ACCEPTED
                                        
                            <span class="count">1</span>
                                    
                        </a>
                                    
                                    
                        <a href="#" class="item">
                                        
                            <span class="badge awaiting-admin-b"></span> DENIED
                                        
                            <span class="count">1</span>
                                    
                        </a>
                                    
                                    
                        <a href="#" class="item">
                                        
                            <span class="badge cancelled"></span> CANCELLED
                                        
                            <span class="count">1</span>
                                    
                        </a>
                                
                    </div>
                            
                </div>
                        
            </div>
					
        </div>
				
    </div>
			
</div>


<!--BORROWING REQUESTS-->
            
<div id="borrowing-requests" class="js-content-blocks p-4">
				
    <h1 class="account-header-top text-emperor border-0">Borrowing Requests</h1>
				
    <div class="tab-content">
        
        <!--BREADCRUMB-->
        
        <div class="row">
        
            <div class="col-lg-12">
            
                <nav aria-label="breadcrumb">
  
                    <ol class="breadcrumb">
                    
                        <li class="breadcrumb-item"><a href="#" onclick="window.history.back()" >Back</a></li>
                    
                        <li class="breadcrumb-item active" aria-current="page">Borrowing Requests</li>
                
                    </ol>
            
                </nav>
            
            </div>
    
        </div>
					
        <div id="reviews-by-you" role="tabpanel" >
						
            <div class="row">
							
                <div class="col-md-8">
								
                    <div class="account-property m-b-30">
										
                                        
                        <!--SHARING REQUEST START-->
                                        
                        <div class="agent-block-wrapper mb-4">
                                            
                            <div class="agent-block-agent-img">
                                                
                                <div class="image-wrap">
                                                    <!--agent-status & status-online styles in style.less-->
                                                    
                                    <div class="agent-status" title="online">
                                                    
                                    </div>
                                                    
                                    <a href="#chatorsomething">
                                                        
                                        <img src="./images/userpicbig.png" alt="agent-photo">
                                                    
                                    </a>
                                                
                                </div>
                                            
                            </div>
                                            
                            <div class="agent-block-agent-information">
                                                
                                <div class="row">
                                                    
                                    <div class="block-left col-3">
                                                        
                                        <h2 class="text-emperor font-400" >Ski Boots</h2>
                                                    
                                    </div>
                                                    
                                    <div class="block-right col-9 mb-4">
                                                        
                                        <span class="small-text text-orange mb-4">Frances is requesting to borrow your (sharing offer name)</span>
                                                    
                                    </div>
                                                
                                </div>
                                                
                                <div class="row mb-4">
                                                    
                                    <div class="block-left col-3">
                                                        
                                        <div class="stars" data-stars="3">
                                                            
                                            <i class="material-icons star">star</i>
                                                            
                                            <i class="material-icons star">star</i>
                                                            
                                            <i class="material-icons star">star</i>
                                                            
                                            <i class="material-icons star">star</i>
                                                            
                                            <i class="material-icons star">star</i>
                                                        
                                        </div>
                                                    
                                    </div>
                                                    
                                    <div class="block-right col-6">
                                                        
                                        <div class="text-container">
                                                            
                                            Hey Vince! I'm Frances and I'm travelling to Canberra with my partner. We're both into skling and would love to borrow your ski boots for a weekend trip to Perisher. We have a few questions: are the ski boots size 11 womens or? Do....
                                                        
                                        </div>
                                                    
                                    </div>
                                                
                                </div>
                                                
                                <div class="row">
                                                    
                                    <div class="col-xl-3 col-lg-3 col-md-6 col-xs-6 col-6 mb-md-5 mb-sm-5 mb-5">
                                                        
                                        <div class="font-700 text-emperor">
                                                            
                                            July 08, 2018
                                                        
                                        </div>
                                                    
                                    </div>
                                                    
                                    <div class="col-xl-3 col-lg-3 col-md-6 col-xs-6 col-6 mb-xl-0 mb-lg-0 mb-md-5 mb-sm-5 mb-5">
                                                        
                                        <div class="font-700 text-emperor">
                                                            
                                            <span>Borrow date(s):</span>
                                                            
                                            <br>
                                                            
                                            <span>12.08.2018 - 15.08.2018</span>
                                                        
                                        </div>
                                                    
                                    </div>
                                                    
                                    <div class="col-xl-6 col-lg-6 col-md-12 col-xs-12 col-12 text-right">
                                                        
                                        
                                        <div class="button-wrapper btnwrsp d-inline">
                                                            
                                            <span href="#" class="font-700 text-blue-requested text-uppercase">REQUESTED</span>
                                                        
                                        </div>
                                        
                                        <div class="button-wrapper btnwrspe d-inline mr-4">
                                                            
                                            <a href="send-message.php">VIEW</a>
                                                        
                                        </div>
                                        
                                        <div class="d-block m-4">
                                                            
                                            <span class="font-400 text-uppercase" style="font-size:11px;" >Or</span> <a href="#" class="font-400 text-cancelled text-uppercase" style="font-size:11px;" data-toggle="modal" data-target="#cancel-borrowing-request" >Cancel this request</a>
                                            
                                            
                                                        
                                        </div>
                                        
                                                    
                                    </div>
                                                
                                </div>
                                            
                            </div>
                                        
                        </div>
                                        
                        <!--SHARING REQUEST START-->
                                        
                        <div class="agent-block-wrapper mb-4">
                                            
                            <div class="agent-block-agent-img">
                                                
                                <div class="image-wrap">
                                    <div class="agent-status" title="online">
                                                    
                                    </div>
                                                    
                                    <a href="#chatorsomething">
                                                        
                                        <img src="./images/userpicbig.png" alt="agent-photo">
                                                    
                                    </a>
                                                
                                </div>
                                            
                            </div>
                                            
                            <div class="agent-block-agent-information">
                                                
                                <div class="row">
                                                    
                                    <div class="block-left col-3">
                                                        <h2 class="text-emperor font-400" >Skateboard</h2>
                                                    </div>
                                                    <div class="block-right col-9 mb-4">
                                                        <span class="small-text text-orange mb-4">Frances is requesting to borrow your (sharing offer name)</span>
                                                    </div>
                                                </div>
                                                <div class="row mb-4">
                                                    <div class="block-left col-3">
                                                        <div class="stars" data-stars="3">
                                                            <i class="material-icons star">star</i>
                                                            <i class="material-icons star">star</i>
                                                            <i class="material-icons star">star</i>
                                                            <i class="material-icons star">star</i>
                                                            <i class="material-icons star">star</i>
                                                        </div>
                                                    </div>
                                                    <div class="block-right col-6">
                                                        <div class="text-container">
                                                            Hey Vince! I'm Frances and I'm travelling to Canberra with my partner. We're both into skling and would love to borrow your ski boots for a weekend trip to Perisher. We have a few questions: are the ski boots size 11 womens or? Do....
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row d-flex align-items-center">
                                                    <div class="col-3">
                                                        <div class="font-700 text-emperor">
                                                            July 08, 2018
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="font-700 text-emperor">
                                                            <span>Borrow date(s):</span>
                                                            <br>
                                                            <span>12.08.2018 - 15.08.2018</span>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-5 text-right">
                                                        <div class="button-wrapper btnwrspe fr">
                                                            <a href="single-offer.php" target="_blank">VIEW</a>
                                                        </div>
                                                        <div class="button-wrapper btnwrsp btnwrsp fr">
                                                            <span class="font-700 text-green text-uppercase" >Accepted</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                        <!--SHARING REQUEST START-->
                                        
                        <div class="agent-block-wrapper mb-4">
                                            
                            <div class="agent-block-agent-img">
                                                
                                <div class="image-wrap">
                                                    <!--agent-status & status-online styles in style.less-->
                                                    
                                    <div class="agent-status" title="online">
                                                    
                                    </div>
                                                    
                                    <a href="#chatorsomething">
                                                        
                                        <img src="./images/userpicbig.png" alt="agent-photo">
                                                    
                                    </a>
                                                
                                </div>
                                            
                            </div>
                                            
                            <div class="agent-block-agent-information">
                                                
                                <div class="row">
                                                    
                                    <div class="block-left col-3">
                                                        
                                        <h2 class="text-emperor font-400" >Phone Charger</h2>
                                                    
                                    </div>
                                                    
                                    <div class="block-right col-9 mb-4">
                                                        
                                        <span class="small-text text-orange mb-4">Frances is requesting to borrow your (sharing offer name)</span>
                                                    
                                    </div>
                                                
                                </div>
                                                
                                <div class="row mb-4">
                                                    
                                    <div class="block-left col-3">
                                                        
                                        <div class="stars" data-stars="3">
                                                            
                                            <i class="material-icons star">star</i>
                                                            
                                            <i class="material-icons star">star</i>
                                                            
                                            <i class="material-icons star">star</i>
                                                            
                                            <i class="material-icons star">star</i>
                                                            
                                            <i class="material-icons star">star</i>
                                                        
                                        </div>
                                                    
                                    </div>
                                                    
                                    <div class="block-right col-6">
                                                        
                                        <div class="text-container">
                                                            
                                            Hey Vince! I'm Frances and I'm travelling to Canberra with my partner. We're both into skling and would love to borrow your ski boots for a weekend trip to Perisher. We have a few questions: are the ski boots size 11 womens or? Do....
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row d-flex align-items-center">
                                                    <div class="col-3">
                                                        <div class="font-700 text-emperor">
                                                            July 08, 2018
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="font-700 text-emperor">
                                                            <span>Borrow date(s):</span>
                                                            <br>
                                                            <span>12.08.2018 - 15.08.2018</span>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-5 text-right">
                                                        <div class="button-wrapper btnwrspe fr">
                                                            <a href="single-offer.php" target="_blank">VIEW</a>
                                                        </div>
                                                        <div class="button-wrapper btnwrsp btnwrsp fr">
                                                            <span  class="font-700 text-red text-uppercase" >Denied</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                        
                        <!--SHARING REQUEST START-->
                                        
                        <div class="agent-block-wrapper mb-4">
                                            
                            <div class="agent-block-agent-img">
                                                
                                <div class="image-wrap">
                                                    <!--agent-status & status-online styles in style.less-->
                                                    
                                    <div class="agent-status" title="online">
                                                    
                                    </div>
                                                    
                                    <a href="#chatorsomething">
                                                        
                                        <img src="./images/userpicbig.png" alt="agent-photo">
                                                    
                                    </a>
                                                
                                </div>
                                            
                            </div>
                                            
                            <div class="agent-block-agent-information">
                                                
                                <div class="row">
                                                    
                                    <div class="block-left col-3">
                                                        
                                        <h2 class="text-emperor font-400" >Diving equipment</h2>
                                                    
                                    </div>
                                                    
                                    <div class="block-right col-9 mb-4">
                                                        
                                        <span class="small-text text-orange mb-4">Frances is requesting to borrow your (sharing offer name)</span>
                                                    
                                    </div>
                                                
                                </div>
                                                
                                <div class="row mb-4">
                                                    
                                    <div class="block-left col-3">
                                                        
                                        <div class="stars" data-stars="3">
                                                            
                                            <i class="material-icons star">star</i>
                                                            
                                            <i class="material-icons star">star</i>
                                                            
                                            <i class="material-icons star">star</i>
                                                            
                                            <i class="material-icons star">star</i>
                                                            
                                            <i class="material-icons star">star</i>
                                                        
                                        </div>
                                                    
                                    </div>
                                                    
                                    <div class="block-right col-6">
                                                        
                                        <div class="text-container">
                                                            
                                            Hey Vince! I'm Frances and I'm travelling to Canberra with my partner. We're both into skling and would love to borrow your ski boots for a weekend trip to Perisher. We have a few questions: are the ski boots size 11 womens or? Do....
                                                        
                                        </div>
                                                    
                                    </div>
                                                
                                </div>
                                                
                                <div class="row d-flex align-items-center">
                                                    
                                    <div class="col-3">
                                                        
                                        <div class="font-700 text-emperor">
                                                            
                                            July 08, 2018
                                                        
                                        </div>
                                                    
                                    </div>
                                                    
                                    <div class="col-4">
                                                        
                                        <div class="font-700 text-emperor">
                                                            
                                            <span>Borrow date(s):</span>
                                                            
                                            <br>
                                                            
                                            <span>12.08.2018 - 15.08.2018</span>
                                                        
                                        </div>
                                                    
                                    </div>
                                                    
                                    
                                                    
                                    <div class="col-5 text-right">
                                                        
                                        <div class="button-wrapper btnwrspe fr">
                                                            
                                            <a href="single-offer.php" target="_blank">VIEW</a>
                                                        
                                        </div>
                                                        
                                        <div class="button-wrapper btnwrsp btnwrsp fr">
                                                            
                                            <span class="font-700 text-cancelled text-uppercase">Cancelled</span>
                                                        
                                        </div>
                                                    
                                    </div>
                                                
                                </div>
                                            
                            </div>
                                        
                        </div>
                                    
                                    
                        <div class="pagination-wrap m-b-30">
                                    
                            <ul>
                                        
                                <li>
                                            
                                    <a href="#"><i class="material-icons">&#xE5C4;</i></a>

                                </li>
                                        
                                <li>
                                            
                                    <a href="#">1</a>
                                        
                                </li>
                                        
                                <li>
                                            
                                    <a href="#">2</a>
                                        
                                </li>
                                       
                                <li>
                                            
                                    <a href="#">3</a>
                                        
                                </li>
                                        
                                <li>
                                            
                                    <a href="#">4</a>
                                        
                                </li>
                                        
                                <li>
                                            
                                    <a href="#"><i class="material-icons">&#xE5C8;</i></a>
                                        
                                </li>
                                    
                            </ul>
                                
                        </div>
                                
                    </div>
                            
                </div>
                            
                            
                <div class="col-md-4">
                                
                    <div class="account_properties_counters_list">
                                    
                        <a href="#" class="item">
                                        
                            <span class="badge rented"></span> REQUESTED
                                        
                            <span class="count">1</span>
                                    
                        </a>
                                    
                                    
                        <a href="#" class="item">
                                        
                            <span class="badge awaiting-admin"></span> ACCEPTED
                                        
                            <span class="count">1</span>
                                    
                        </a>
                                    
                                    
                        <a href="#" class="item">
                                        
                            <span class="badge awaiting-admin-b"></span> DENIED
                                        
                            <span class="count">1</span>
                                    
                        </a>
                                    
                                    
                        <a href="#" class="item">
                                        
                            <span class="badge cancelled"></span> CANCELLED
                                        
                            <span class="count">1</span>
                                    
                        </a>
                                
                    </div>
                            
                </div>
                        
            </div>
					
        </div>
				
    </div>
			
</div>


<!--REVIEWS-->

<div id="reviews" class="js-content-blocks p-4">
				
    <h1 class="account-header-top">Reviews</h1>
		
</div>


<!--YOUR TRAVEL PERSONA-->

<div id="your-travel-persona" class="js-content-blocks p-4">
				
    <h1 class="account-header-top">Your traveller persona</h1>
                
		
</div>