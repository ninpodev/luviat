<?php include 'header.php' ?>

<body>

    
<div class="page-title-simple">
    
    <div class="container">
        
        <h1>Log In</h1>
        
    </div>
    
</div>
    
<!-- Items to share -->
<div id="join" class="container">
    
    <div class="row">
        
        <div class="col-xl-6 offset-xl-3 col-12 mt-5 mb-5">
            
            <div class="main-contact-form">
                
                <form class="row" >
                    
                    <div class="form-group has-feedback col-12 ">
                        
                        <input type="email" class="form-control pt-3" placeholder="Email" />

                        <i class="glyphicon glyphicon-envelope form-control-feedback"></i>
                        
                    </div>
                    
                    <div class="form-group has-feedback col-12 ">
                        
                        <input type="password" class="form-control pt-3" placeholder="Password *" />

                        <i class="glyphicon glyphicon-asterisk form-control-feedback"></i>
                        
                    </div>
                    
                    
                    <div class="col-6 mt-2 mb-5">
                        
                        <input type="checkbox" name="daily" id="daily" class="css-checkbox">
                        <label for="daily" class="css-label">Remember me </label>
                        
                    </div>
                    
                     <div class="col-6 mt-2 mb-5 text-right">
                        
                        <p><a href="forgot-password.php" class="text-orange">Forgot Password?</a></p>
                        
                    </div>
                    
                    <div class="form-group col-12 has-feedback">
                        
                        <input type="submit" form="contact-main" value="Log In" class="input-button">
                    </div>
                    
                </form>
                
                
                
            </div>
            
        </div>
        
    </div>
    
</div>


    <?php include 'footer.php' ?>