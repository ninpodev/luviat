<div id="hola" class="account_menu_categories">
				
    <!--DASHBOARD-->
    <a href="dashboard.php">
					
        <div class="category_item">
						
            <div class="category_item__category-name">
							
                <i class="material-icons">view_quilt</i>
							
							
                <span>Dashboard</span>
						
            </div>
					
        </div>
				
    </a>
      
    <?php /*
    <!--SUBMIT A SHARING OFFER-->
    <a href="#submit-sharing">
					
        <div class="category_item">
						
            <div class="category_item__category-name">
							
                <i class="material-icons">add_circle</i>
							
                <span>Submit a sharing offer</span>
						
            </div>
					
        </div>
				
    </a>
     */?>
    
    <!--MY SHARING OFFERS-->
    <a href="my-sharing-offers.php">
					
        <div class="category_item">
						
            <div class="category_item__category-name">
							
                <i class="material-icons">card_travel</i>
							
                <span>My sharing offers</span>
						
            </div>
            
            <span class="category_item__item-counts">
                34
            </span>
					
        </div>
				
    </a>

    <!--WANTLIST-->
    <a href="wantlist.php">
					
        <div class="category_item">
						
            <div class="category_item__category-name">
							
                <i class="material-icons">favorite</i>
							
                <span>Wantlist</span>
						
            </div>
            
            <span class="category_item__item-counts">
                3
            </span>
					
        </div>
				
    </a>
    
    <!--SHARING REQUESTS-->
    <a href="sharing-requests.php" >
					
        <div class="category_item">
						
            <div class="category_item__category-name">
							
                <i class="material-icons">drafts</i>
										
                <span>Sharing requests</span>
						
            </div>
					
        </div>
				
    </a>

    <!--BORROWING REQUESTS-->
    <a href="borrowing-requests.php" >
					
        <div class="category_item">
						
            <div class="category_item__category-name">
							
                <i class="material-icons">email</i>
							
                <span>Borrowing requests</span>
						
            </div>
					
        </div>
				
    </a>

    <!--REVIEWS-->
    <a href="review-an-user.php">
					
        <div class="category_item">
						
            <div class="category_item__category-name">
							
                <i class="material-icons">rate_review</i>
										
                <span>Reviews</span>
						
            </div>
					
        </div>
				
    </a>    
    
    <?php /*
    <!--YOUR TRAVEL PERSONA-->
    <a href="#your-travel-persona" >
					
        <div class="category_item">
						
            <div class="category_item__category-name">
							
                <i class="material-icons">person</i>
										
                <span>Your travel persona</span>
						
            </div>
					
        </div>
				
    </a>     
    */ ?>
    
    <!--SETTINGS-->
    <a href="settings.php">
					
        <div class="category_item">
						
            <div class="category_item__category-name">
							
                <i class="material-icons">settings</i>
										
                <span>Settings</span>
						
            </div>
					
        </div>
				
    </a>        
    
    
                <!--
				<a href="#submit" class="js-menu-link" data-display-id="submit">
					<div class="category_item">
						<div class="category_item__category-name">
							<i class="material-icons">add_circle</i>
							
							<span>Submit a sharing offer</span>
						</div>
					</div>
				</a>
				<a href="#property" class="js-menu-link new-notification__enabled" data-display-id="property">
					<div class="category_item">
						<div class="category_item__category-name">
							<i class="material-icons">view_module</i>
							
							<span>My sharing offers</span>
						</div>
						<span class="category_item__item-counts">
							34
						</span>
					</div>
				</a>
				<a href="#favorites" class="js-menu-link" data-display-id="favorites">
					<div class="category_item">
						<div class="category_item__category-name">
							<i class="material-icons">favorite</i>
							
							<span>Needs</span>
						</div>
						<span class="category_item__item-counts">
							3
						</span>
					</div>
				</a>
                
				
                
				<a href="#settings" class="js-menu-link" data-display-id="settings">
					<div class="category_item">
						<div class="category_item__category-name">
							<i class="material-icons">settings</i>
							
							<span>Settings</span>
						</div>
					</div>
				</a>
-->
    
    <!--<i class="material-icons js-test-toggle lg-visible"></i>-->
			
</div>