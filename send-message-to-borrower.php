<?php include 'header.php'; ?>

<body>
    
	<div class="account-page">
		
        <?php include 'user-sidebar.php' ?>
		
        <div class="account-page-wild_block">
            
            <div id="dashboard" class="js-content-blocks">
                
                <h1 class="account-header-top mb-4">Send message to borrower</h1>
                
                <div class="row">
        
                    <div class="col-lg-12">
            
                        <nav aria-label="breadcrumb">
  
                            <ol class="breadcrumb">
                    
                                <li class="breadcrumb-item">
                                    <a href="borrowing-requests.php" class="text-orange">Back to your sharing requests</a>
                                </li>
    
                    
                                <li class="breadcrumb-item active" aria-current="page">Send Message</li>
                
                            </ol>
            
                        </nav>
            
                    </div>
    
                </div>
				
                    <div class="tab-content">
					
                        <div id="reviews-by-you" role="tabpanel" >
						
                            <div class="row">
                            
                                <div class="col-xl-3 col-lg-3 col-md-5 col-sm-12 col-12">
                                
                                    <div class="mb-5 m-0 border-0 pdt20">
                                    
                                        <div class="border p-5">
                                        
                                            <div class="row">
                                            
                                                <div class="col-md-6">
                                                
                                                    <div class="user-block">
                                                    
                                                        <div class="avatar-image-container">
                                                        
                                                            <div class="avatar-image">
                                                            <!--agent-status & status-online styles in style.less-->
                                                            <a href="#chatorsomething">
                                                                <img src="./images/userpicbig.png" alt="agent-photo">
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <span class="font-400 mb-4 fs-16">Pauline</span>
                                                <ul class="wp pdtp10">
                                                    <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li class="list-inline-item text-emperor m-0 p-0"><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                    <li class="list-inline-item text-emperor m-0 p-0"><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                </ul>
                                                <span class="font-400 mb-4">July 08, 2018</span>

                                            </div>
                                            <div class="col-md-12 pdtp10">
                                                <span class="text-orange"><i class="fas fa-mobile-alt"></i> 0431 201 301</span>
                                            </div>
                                            <div class="col-md-12 pdtp10">
                                                <span class="text-orange"><i class="fas fa-envelope"></i> email@gmail.com</span>
                                            </div>
                                            <div class="col-md-12">
                                            </div>
                                            <div class="col-md-12 bor-btm"></div>
                                            <div class="pad-top">
                                                <div class="col-md-12">
                                                    <small>Requesting to borrow your:</small>
                                                </div>
                                                <div class="col-md-12 pd-bt-4 pdtp10">
                                                    <span class="font-400 mb-4 fs-18 ">Ski Boots <small class="pd-l5"> <a href="single-offer.php" target="_blank" >view item</a></small></span>
                                                </div>
                                                <div class="col-md-12 bor-btm"></div>
                                                <div class="col-md-6 pdtp10">
                                                    <div class="mb-4">
                                                        <label class="font-open-sans font-400 text-emperor bg-transparent"> <small>Borrow Date</small></label>
                                                        <div class="clearfix"></div>
                                                        12.07.2018
                                                    </div>
                                                </div>
                                                <div class="col-md-6 pdtp10">
                                                    <div class="mb-4">
                                                        <label class="font-open-sans font-400 text-emperor bg-transparent"><small>Return Date</small></label>
                                                        <div class="clearfix"></div>
                                                        14.07.2018
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 pdtp10">
                                                <div class="mb-4">
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <input type="checkbox" name="Pickup" id="Pickup" class="css-checkbox" checked  disabled="disabled">
                                                            <label for="Pickup" class="css-label">Pickup</label>
                                                        </div>
                                                        <div class="col-6">
                                                            <input type="checkbox" name="Delivery" id="Delivery" class="css-checkbox"  disabled="disabled">
                                                            <label for="Delivery" class="css-label">Delivery</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="mb-0 border-top">
                                                    <div class="row">
                                                        <div class="col-6 text-left">
                                                            <p>Delivery Fee</p>
                                                        </div>
                                                        <div class="col-6 text-right">
                                                            <p>$6</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="mb-0 border-top">
                                                    <div class="row">
                                                        <div class="col-6 text-left">
                                                            <p>Deposit</p>
                                                        </div>
                                                        <div class="col-6 text-right">
                                                            <p>$11</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="border-top">
                                                    <div class="row">
                                                        <div class="col-6 text-left">
                                                            <p class="font-weight-bold font-700" >Total</p>
                                                        </div>
                                                        <div class="col-6 text-right">
                                                            <p>$21</p>
                                                        </div>
                                                        
                                                        <div class="col-12">
                                                            <p class="colorgray"><strong>Note: Luviat does not manage payment.</strong></p>
                                                        </div>
                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                                </div>
                                
                                <div class="col-xl-6 col-lg-6 col-md-4 col-sm-12 col-12">

                                    <div class="account-property m-b-30 pdt20 col-md-12">
                                            <div class=" bg-grey specialtile">
                                                Hey Pauline! I'm Pauline and I'm travelling to Canberra with my partner. We're both into skling and would love to borrow your ski boots for a weekend trip to Perisher. We have a few questions: are the ski boots size 11 womens or? Do...
                                            </div>
                                    </div>

                                    <div class="account-property col-md-12 pt-4 mb-4">
                                        <div class="m-b-30">
                                            <div class="agent-block-wrapper-header">
                                                <h4 class="my-4">Send message via email to Pauline</h4>
                                            </div>
                                            <!--AGENT BLOCK BEGIN-->
                                            <div class="specialcont col-md-12 bg-grey">
                                                <div class="col-md-12 pdtop10">
                                                    <textarea rows="12" placeholder="Thanks for your request. Before I accept, please contact me to discuss your need by <email> / on <phone>. I look forward to hearing from you
    //OR//
    Thanks for your request. Let's discuss, the best way to contact me is by <email> / on <phone>. I look forward to hearing from you" ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="account-property row fleft w-100">
                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 text-left pl-5">
                                            <div class="button-wrapper btnwrsp btnwrsp">
                                                <a href="#" data-toggle="modal" data-target="#denysharing" class="text-red text-uppercase">Deny this request</a>
                                            </div>
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 text-right">
                                            <button type="submit" class="button-wrapper btnwrspe">Send message</button>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12 pt-5">
                                    <p>Are you satisfied with the negotiation? Ready to approve this request? </p>
                                    
                                    <button type="submit" class="btn btn-secondary bg-orange text-white text-uppercase font-700 font-open-sans px-5 py-4 w-100" style="font-size:13px;"  data-toggle="modal" data-target="#updatemyModal">APPROVE REQUEST</button>
                                    
                                </div>
                            
                        </div>
                        
					</div>
				</div>
			</div>
                
		</div>
	</div>

    </div>

    </div>


<!-- MODALS -->

<!--modal approve-->

<div class="modal fade" id="write-review-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

	<div class="modal-dialog rounded-0" role="document">
		
		<div class="modal-content rounded-0 bg-grey">
			
			<div class="modal-header rounded-0 border-0">
				
				<h4 class="modal-title text-emperor font-700 ">Review Pauline</h4>
				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				
				<span aria-hidden="true">&times;</span>
				
				</button>
				
			</div>
			
			<div class="modal-body">
				<div class="row">
					<div class="col-12 col-sm-6">
						<div class="user-container">
							<div class="user-block">
								<div class="avatar-image-container">
									<div class="avatar-image">
										<!--agent-status & status-online styles in style.less-->
										<a href="#chatorsomething">
											<img src="./images/userpicbig.png" alt="agent-photo">
										</a>
									</div>
								</div>
							</div>
							<div class="user-block">
								<div class="user-name">
									Pauline
								</div>
								<div class="user-stars">
									<div class="stars" data-stars="3">
										<i class="material-icons star">star</i>
										<i class="material-icons star">star</i>
										<i class="material-icons star">star</i>
										<i class="material-icons star">star</i>
										<i class="material-icons star">star</i>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12 col-sm-6">
						<ul class="no-style-list">
							<li>
								<span>Metric #1 </span>
								<div class="stars orange-stars" data-stars="0">
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
								</div>
							</li>
							<li>
								<span>Metric #1 </span>
								<div class="stars orange-stars" data-stars="0">
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
								</div>
							</li>
						</ul>
					</div>
				</div>
				
				<textarea placeholder="Tell others what Pauline was like."></textarea>
				
			</div>
			
			<div class="modal-footer border-0">
				
				<button type="button" class="btn btn-secondary floating-button bg-orange text-white text-uppercase font-700 font-open-sans px-5 py-3" style="font-size:13px;" data-dismiss="modal" data-toggle="modal" data-target="#approval-confirmation-modal" >Post review</button>
				
				
			</div>
			
		</div>
		
	</div>
	
</div>

<div class="modal fade" id="updatemyModal" role="dialog" data-backdrop="static">
        <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header d-flex justify-content-start">
            <h4 class="modal-title">You are about to approve a sharing request.</h4>
            
            </div>
            <div class="modal-body">
            <p class="fz-12-">Once you approve this sharing request the borrower will receive an email notifying them their request has been approved. Your email and contact number will also be shared with them.
            </p>
            <p class="fz-12-">No payment will be made through the luviat platform and will need to be arranged externally.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btnwb" data-dismiss="modal">Discard</button>
                <button type="button" class="btn btn-secondary bg-orange text-white text-uppercase font-700 font-open-sans px-5 py-3" data-dismiss="modal"  data-toggle="modal" data-target="#sharingaccepted">Continue</button>
            </div>
        </div>

        </div>
    </div>
    
<div class="modal fade" id="sharingaccepted" role="dialog">
        <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header d-flex justify-content-start">
            
                <h4 class="modal-title">Sharing request accepted!</h4>
                
            </div>
            <div class="modal-body">
            <p class="fz-12-">Thank you for accepting the sharing request! Not only are you keeping Luviat alive but you are helping someone have an unforgettable experience in a new city.
            </p>
            <p class="fz-12-">They have received your contact information you have provided to us. You can either contact them first or wait until they contact you.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary bg-orange text-white text-uppercase font-700 font-open-sans px-5 py-3" data-dismiss="modal" onclick="javascript:window.location='/sharing-requests.php'">Back to your sharing requests</button>
            </div>
        </div>

        </div>
    </div>

<div class="modal fade" id="denysharing" role="dialog" data-backdrop="static">
        <div class="modal-dialog modal-lg">
        <div class="modal-content bg-light-grey">
            <div class="modal-header d-flex justify-content-start">
            <h4 class="modal-title font-700">Deny sharing request and send message via email</h4> 
            
            </div>
            <div class="modal-body">
                <p>Give the Borrower a reason for denying their sharing request</p>
                <textarea id="editor2" placeholder="Lorem ipsum dolor sit amet, consectetur adipsiicis, sed do eisuaid labore et dolore magna aliqua. Ut aneim adms imnir pevnit ventinima laboris ullamanco laboris nisi ut aliquimp ex ae comodo cosequat." ></textarea>
            </div>
            <div class="modal-footer">
                
                <button type="button" class="btn btn-default btnwb bg-transparent" data-dismiss="modal">Discard</button>
                &nbsp;&nbsp;                
                <button type="button" class="btn btn-secondary bg-orange text-white text-uppercase font-700 font-open-sans px-5 py-3" data-dismiss="modal"  data-toggle="modal" data-target="#sharingdenied">DENY REQUEST AND SEND</button>
            </div>
        </div>

        </div>
    </div>
    
<div class="modal fade" id="sharingdenied" role="dialog" data-backdrop="static">
        
    <div class="modal-dialog modal-lg">
            
        <div class="modal-content">
            
            <div class="modal-header justify-content-start">
            
                <h4 class="modal-title">Sharing request denied!</h4>
            
            </div>
            
            <div class="modal-body">
            
                <p>Thanks for responding to this sharing request. The offer will still be available on Luviat to share unless you change it's status to unavailable. You can do this from the My sharing offers tab in your traveller dashboard.</p>
                
            </div>
            
            <div class="modal-footer">
                
                <button type="button" class="btn btn-secondary bg-orange text-white text-uppercase font-700 font-open-sans px-5 py-3" data-dismiss="modal" onclick="javascript:window.location='sharing-requests.php'">Back to your sharing Requests</button>
                
            </div>
            
        </div>
        
    </div>
    
</div>





<?php include 'scripts.php'; ?>
</body>
</html>