<?php include 'header.php'; ?>
<body>

    <div class="container js-home-1-search" style="margin-top: 31px">

    
        <!--PROPERTY SEARCH FORM 1 BEGIN-->
    
        <div class="js-home-2-search">
                        
                        <form id="search-form-1">
            
                            <div class="property-search bg-white">
                
                                <div class="main-apartment-search-options">
                    
                                    <div class="options-title">
                        
                                        <h4 class="agent-name mb-3 text-emperor">What are you looking for?</h4>
                    
                                    </div>
                    
                                    <form>
                    
                                        <div class="options-wrapper-main">
                        
                        
                                            <div class="wrapper search-property">
                            
                                                <input type="search" class="property-searchinput" placeholder="Area of interest">
                        
                                            </div>

                        
                                            <div class="wrapper">

                            
                                                <select form="search-form-1" name="location" class="location-select big-input"
                                    data-placeholder="location">
                                
                                                    <option value="london">London</option>
                                
                                                    <option value="miami">Miami</option>
                                
                                                    <option value="new-york">New-York</option>
                                
                                                    <option value="houston">Houston</option>

                            
                                                </select>
                        
                                            </div>

                        
                                            <div class="wrapper search-property">
                            
                                                <input type="search" class="property-searchinput" placeholder="Keyword">
                        
                                            </div>
                        
                                            <div class="wrapper">

                            <div class="range-picker-wrapper position-relative" >
                                <i class="fa fa-calendar text-orange" aria-hidden="true"></i>
                                <input type="text" class="dates-input pl-5 datepick" placeholder="Dates">
                            </div>
                        </div>
                        
                                            <div class="wrapper d-flex justify-content-start">
                            <a href="#" data-toggle="modal" data-target="#create-need-modal" class="cta-btn" >Search</a>
                        </div>
                    
                                        </div>
                    
                                    </form>
                
                                </div>
            
                            </div>
        
                        </form>
                    
                    </div>



        <!--PROPERTY SEARCH FORM 1 END-->

    </div>

    <div class="container">

        <div class="apartment-grid">
    
            <div class="owl-carousel owl-theme">
            
                <div class="property-carousel-arrow-block">

                <div class="arrow-wrapper">
                    <a href="#" class="arrow-prev">
                        <i class="material-icons">arrow_back</i>
                    </a>
                    <a href="#" class="arrow-next">
                        <i class="material-icons">arrow_forward</i>
                    </a>
                </div>
                <div class="span-wrapper">
                    <span class="txt-grey">Search Results</span>
                </div>
                
                <div class="button-wrapper">
                    <a href="#">SEE RESULTS ON MAP</a>
                </div>
                <div class="button-wrapper btn-g4">
                    <a href="#">SHOW MORE</a>
                </div>
            </div>
        
            </div>
        
            <div class="item">
            <div class="row">


                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="property-block">
                        <p class="property-type">ITEM</p>
                        <p class="property-title"><a href="single-offer.php">Samsung G15 Charger (Aus)..</a></p>

                        <div class="apartment-image">
                            <a href="single-offer.php"><img src="./images/charger.jpeg" alt="image"></a>
                            <div class="badges">
                                <p class="rent">AVAILABLE</p>
                            </div>

                        </div>

                        <div class="apartment-values row">
                            <div class="col-md-5">
                                <i class="fas fa-map-marker-alt"></i>
                                <span class="txt-g">2km away</span>
                            </div>
                            <div class="col-md-5">
                                <i class="far fa-calendar-check"></i>
                                <span class="txt-g">Available now</span>
                            </div>
                            <div class="col-md-2 icons">
                                <a href="#" class="heart"><i class="material-icons icons-style"></i></a>
                            </div>
                        </div>
                        <div class="apartment-info s-grid">
                            <span class="txt-g">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                Nunc dignissim turpis ut posuere tempor. Fusce gravida, 
                            </span>        
                        </div>
                        <div class="apartment-info">
                            <div class="apartment-price">
                                <p class="price-small">$4 AUD / hour</p>
                            </div>
                            <div class="apartment-price">
                                <p class="price-small">$8 AUD / hour</p>
                            </div>
                        </div>
                        <div class="apartment-manager">
                            <div class="manager-wrap row">
                                    <div class="col-md-2">
                                        <div class="manager-icon">
                                            <a href="#chat"> <img src="./images/userpic.png" alt="userpic"></a>
                                            <div class="online-status"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class=" text-orange">
                                            Pauline Saunders
                                        </div>
                                        
                                        <ul class="list-inline">
                                            <div class="fl">
                                                <li class="list-inline-item text-orange">
                                                    <i class="fa fa-globe" aria-hidden="true"></i> 
                                                    <span> Canberra, Aus</span>
                                                </li>
                                            </div>
                                            <div class="fr">
                                                <li class="list-inline-item" >
                                                    
                                                    <ul class="list-inline">

                                                        <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                                        
                                                        <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                                        
                                                        <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>

                                                        <li class="list-inline-item text-emperor m-0 p-0"><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                        
                                                        <li class="list-inline-item text-emperor m-0 p-0"><i class="fa fa-star-o" aria-hidden="true"></i></li>

                                                    </ul>
                                                    
                                                </li>
                                            </div>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="property-block">
                        <p class="property-type">EXPERIENCE</p>
                        <p class="property-title">3 Day Camping/Hiking Adve..</p>

                        <div class="apartment-image">
                            <img src="./images/image.png" alt="image">
                            <div class="badges">
                                <p class="rent">AVAILABLE</p>
                            </div>

                        </div>

                        <div class="apartment-values row">
                            <div class="col-md-5">
                                <i class="fas fa-map-marker-alt"></i>
                                <span class="txt-g">Andes,Peru</span>
                            </div>
                            <div class="col-md-5">
                                <i class="far fa-calendar-check"></i>
                                <span class="txt-g">On Demand</span>
                            </div>
                            <div class="col-md-2 icons">
                                <a href="#" class="heart"><i class="material-icons icons-style"></i></a>
                            </div>
                        </div>
                        <div class="apartment-info s-grid">
                            <span class="txt-g">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                Nunc dignissim turpis ut posuere tempor. Fusce gravida, 
                            </span>        
                        </div>
                        <div class="apartment-info">
                            <div class="apartment-price">
                                <p class="price-small">$400 AUD / person</p>
                            </div>
                            <div class="apartment-price">
                                <p class="price-small">$1200 AUD / gro.</p>
                            </div>
                        </div>
                        <div class="apartment-manager">
                            <div class="manager-wrap row">
                                <div class="col-md-2">
                                    <div class="manager-icon">
                                        <a href="#chat"> <img src="./images/userpic.png" alt="userpic"></a>
                                        <div class="online-status"></div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class=" text-orange">
                                        Pauline Saunders
                                    </div>
                                    
                                    <ul class="list-inline">
                                        <div class="fl">
                                            <li class="list-inline-item text-orange">
                                                <i class="fa fa-globe" aria-hidden="true"></i> 
                                                <span> Lima, Peru</span>
                                            </li>
                                        </div>
                                        <div class="fr">
                                            <li class="list-inline-item" >
                                                
                                                <ul class="list-inline">

                                                    <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    
                                                    <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    
                                                    <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>

                                                    <li class="list-inline-item text-emperor m-0 p-0"><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                    
                                                    <li class="list-inline-item text-emperor m-0 p-0"><i class="fa fa-star-o" aria-hidden="true"></i></li>

                                                </ul>
                                                
                                            </li>
                                        </div>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="property-block">
                        <p class="property-type">SERVICE</p>
                        <p class="property-title">Visa / Immigration Help</p>

                        <div class="apartment-image">
                            <img src="./images/image.png" alt="image">
                            <div class="badges">
                                <p class="rent">AVAILABLE</p>
                            </div>

                        </div>

                        <div class="apartment-values row">
                            <div class="col-md-5">
                                <i class="fas fa-map-marker-alt"></i>
                                <span class="txt-g">8km away</span>
                            </div>
                            <div class="col-md-5">
                                <i class="far fa-calendar-check"></i>
                                <span class="txt-g-2">By Appointment</span>
                            </div>
                            <div class="col-md-2 icons">
                                <a href="#" class="heart"><i class="material-icons icons-style"></i></a>
                            </div>
                        </div>
                        <div class="apartment-info s-grid">
                            <span class="txt-g">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                Nunc dignissim turpis ut posuere tempor. Fusce gravida, 
                            </span>        
                        </div>
                        <div class="apartment-info">
                            <div class="apartment-price">
                                <p class="price-small">By Negotiation</p>
                            </div>
                            <div class="apartment-price">
                                <p class="price-small"></p>
                            </div>
                        </div>
                        <div class="apartment-manager">
                            <div class="manager-wrap row">
                                <div class="col-md-2">
                                    <div class="manager-icon">
                                        <a href="#chat"> <img src="./images/userpic.png" alt="userpic"></a>
                                        <div class="online-status"></div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class=" text-orange">
                                        Kevin Samson
                                    </div>
                                    
                                    <ul class="list-inline">
                                        <div class="fl">
                                            <li class="list-inline-item text-orange">
                                                <i class="fa fa-globe" aria-hidden="true"></i> 
                                                <span> Canberra, Aus</span>
                                            </li>
                                        </div>
                                        <div class="fr">
                                            <li class="list-inline-item" >
                                                
                                                <ul class="list-inline">

                                                    <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    
                                                    <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    
                                                    <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>

                                                    <li class="list-inline-item text-emperor m-0 p-0"><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                    
                                                    <li class="list-inline-item text-emperor m-0 p-0"><i class="fa fa-star-o" aria-hidden="true"></i></li>

                                                </ul>
                                                
                                            </li>
                                        </div>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        
            <div class="item">
            <div class="row">


                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="property-block">
                        <p class="property-type">ITEM</p>
                        <p class="property-title">Samsung G15 Charger (Aus)..</p>

                        <div class="apartment-image">
                            <img src="./images/image.png" alt="image">
                            <div class="badges">
                                <p class="rent">AVAILABLE</p>
                            </div>

                        </div>

                        <div class="apartment-values row">
                            <div class="col-md-5">
                                <i class="fas fa-map-marker-alt"></i>
                                <span class="txt-g">2km away</span>
                            </div>
                            <div class="col-md-5">
                                <i class="far fa-calendar-check"></i>
                                <span class="txt-g">Available now</span>
                            </div>
                            <div class="col-md-2 icons">
                                <a href="#" class="heart"><i class="material-icons icons-style"></i></a>
                            </div>
                        </div>
                        <div class="apartment-info s-grid">
                            <span class="txt-g">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                Nunc dignissim turpis ut posuere tempor. Fusce gravida, 
                            </span>        
                        </div>
                        <div class="apartment-info">
                            <div class="apartment-price">
                                <p class="price-small">$4 AUD / hour</p>
                            </div>
                            <div class="apartment-price">
                                <p class="price-small">$8 AUD / hour</p>
                            </div>
                        </div>
                        <div class="apartment-manager">
                            <div class="manager-wrap row">
                                    <div class="col-md-2">
                                        <div class="manager-icon">
                                            <a href="#chat"> <img src="./images/userpic.png" alt="userpic"></a>
                                            <div class="online-status"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class=" text-orange">
                                            Pauline Saunders
                                        </div>
                                        
                                        <ul class="list-inline">
                                            <div class="fl">
                                                <li class="list-inline-item text-orange">
                                                    <i class="fa fa-globe" aria-hidden="true"></i> 
                                                    <span> Canberra, Aus</span>
                                                </li>
                                            </div>
                                            <div class="fr">
                                                <li class="list-inline-item" >
                                                    
                                                    <ul class="list-inline">

                                                        <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                                        
                                                        <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                                        
                                                        <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>

                                                        <li class="list-inline-item text-emperor m-0 p-0"><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                        
                                                        <li class="list-inline-item text-emperor m-0 p-0"><i class="fa fa-star-o" aria-hidden="true"></i></li>

                                                    </ul>
                                                    
                                                </li>
                                            </div>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="property-block">
                        <p class="property-type">EXPERIENCE</p>
                        <p class="property-title">3 Day Camping/Hiking Adve..</p>

                        <div class="apartment-image">
                            <img src="./images/image.png" alt="image">
                            <div class="badges">
                                <p class="rent">AVAILABLE</p>
                            </div>

                        </div>

                        <div class="apartment-values row">
                            <div class="col-md-5">
                                <i class="fas fa-map-marker-alt"></i>
                                <span class="txt-g">Andes,Peru</span>
                            </div>
                            <div class="col-md-5">
                                <i class="far fa-calendar-check"></i>
                                <span class="txt-g">On Demand</span>
                            </div>
                            <div class="col-md-2 icons">
                                <a href="#" class="heart"><i class="material-icons icons-style"></i></a>
                            </div>
                        </div>
                        <div class="apartment-info s-grid">
                            <span class="txt-g">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                Nunc dignissim turpis ut posuere tempor. Fusce gravida, 
                            </span>        
                        </div>
                        <div class="apartment-info">
                            <div class="apartment-price">
                                <p class="price-small">$400 AUD / person</p>
                            </div>
                            <div class="apartment-price">
                                <p class="price-small">$1200 AUD / gro.</p>
                            </div>
                        </div>
                        <div class="apartment-manager">
                            <div class="manager-wrap row">
                                <div class="col-md-2">
                                    <div class="manager-icon">
                                        <a href="#chat"> <img src="./images/userpic.png" alt="userpic"></a>
                                        <div class="online-status"></div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class=" text-orange">
                                        Pauline Saunders
                                    </div>
                                    
                                    <ul class="list-inline">
                                        <div class="fl">
                                            <li class="list-inline-item text-orange">
                                                <i class="fa fa-globe" aria-hidden="true"></i> 
                                                <span> Lima, Peru</span>
                                            </li>
                                        </div>
                                        <div class="fr">
                                            <li class="list-inline-item" >
                                                
                                                <ul class="list-inline">

                                                    <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    
                                                    <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    
                                                    <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>

                                                    <li class="list-inline-item text-emperor m-0 p-0"><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                    
                                                    <li class="list-inline-item text-emperor m-0 p-0"><i class="fa fa-star-o" aria-hidden="true"></i></li>

                                                </ul>
                                                
                                            </li>
                                        </div>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="property-block">
                        <p class="property-type">SERVICE</p>
                        <p class="property-title">Visa / Immigration Help</p>

                        <div class="apartment-image">
                            <img src="./images/image.png" alt="image">
                            <div class="badges">
                                <p class="rent">AVAILABLE</p>
                            </div>

                        </div>

                        <div class="apartment-values row">
                            <div class="col-md-5">
                                <i class="fas fa-map-marker-alt"></i>
                                <span class="txt-g">8km away</span>
                            </div>
                            <div class="col-md-5">
                                <i class="far fa-calendar-check"></i>
                                <span class="txt-g-2">By Appointment</span>
                            </div>
                            <div class="col-md-2 icons">
                                <a href="#" class="heart"><i class="material-icons icons-style"></i></a>
                            </div>
                        </div>
                        <div class="apartment-info s-grid">
                            <span class="txt-g">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                Nunc dignissim turpis ut posuere tempor. Fusce gravida, 
                            </span>        
                        </div>
                        <div class="apartment-info">
                            <div class="apartment-price">
                                <p class="price-small">By Negotiation</p>
                            </div>
                            <div class="apartment-price">
                                <p class="price-small"></p>
                            </div>
                        </div>
                        <div class="apartment-manager">
                            <div class="manager-wrap row">
                                <div class="col-md-2">
                                    <div class="manager-icon">
                                        <a href="#chat"> <img src="./images/userpic.png" alt="userpic"></a>
                                        <div class="online-status"></div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class=" text-orange">
                                        Kevin Samson
                                    </div>
                                    
                                    <ul class="list-inline">
                                        <div class="fl">
                                            <li class="list-inline-item text-orange">
                                                <i class="fa fa-globe" aria-hidden="true"></i> 
                                                <span> Canberra, Aus</span>
                                            </li>
                                        </div>
                                        <div class="fr">
                                            <li class="list-inline-item" >
                                                
                                                <ul class="list-inline">

                                                    <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    
                                                    <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    
                                                    <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>

                                                    <li class="list-inline-item text-emperor m-0 p-0"><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                    
                                                    <li class="list-inline-item text-emperor m-0 p-0"><i class="fa fa-star-o" aria-hidden="true"></i></li>

                                                </ul>
                                                
                                            </li>
                                        </div>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    
        </div>

    </div>

    <!--APARTMENT GRID END-->
    
    <!-- CREATE A NEED-->

    <div class="create-a-need py-5 bg-light-grey">
    
        <div class="container">
        
            <div class="section-header">
            
                <h4 class="agent-name mb-3 text-emperor" >
                
                    Couldn't find what you're looking for? Create a Need ad.
            
                </h4>
            
                <p>
                
                    This will create a callout to the comunity. If people think they can help you, they'll respond your need.
            
                </p>
        
            </div>
    
        </div>
    
        <div class="container js-home-2-search" style="margin-top: 31px">

        <!--PROPERTY SEARCH FORM 1 BEGIN-->
        <form id="search-form-1">
            <div class="property-search bg-white">
                <div class="main-apartment-search-options">
                    <div class="options-title">
                        <h4 class="agent-name mb-3 text-emperor">Create an add with these keywords</h4>
                    </div>
                    <form>
                    <div class="options-wrapper-main">
                        
                        <div class="wrapper search-property">
                            <input type="search" class="property-searchinput" placeholder="Area of interest">
                        </div>

                        <div class="wrapper">

                            <select form="search-form-1" name="location" class="location-select big-input"
                                    data-placeholder="location">
                                <option value="london">London</option>
                                <option value="miami">Miami</option>
                                <option value="new-york">New-York</option>
                                <option value="houston">Houston</option>

                            </select>
                        </div>

                        <div class="wrapper search-property">
                            <input type="search" class="property-searchinput" placeholder="Keyword">
                        </div>

                        <div class="wrapper">

                            <div class="range-picker-wrapper position-relative" >
                                <i class="fa fa-calendar text-orange" aria-hidden="true"></i>
                                <input type="text" class="dates-input pl-5 datepick" placeholder="Dates">
                            </div>
                        </div>

                        <div class="wrapper d-flex justify-content-start">
                            <a href="#" data-toggle="modal" data-target="#create-need-modal" class="cta-btn" >Create need</a>
                        </div>
                    </div>
                    </form>

                </div>

                <div class="toggle-options">

                    <div class="row advanced-options">
                        <div class="col-md-3">
                            <div class="options-wrapper price">
                                <div class="prop-wrapper">
                                    <span class="properties">Price: </span>
                                    <span class="js-first-number">200k</span>
                                    <span class="js-separator">-</span>
                                    <span class="js-second-number">400k</span>
                                </div>
                                <input type="text" id="price" name="price" value="" data-type="double" data-min="0"
                                       data-max="100000" data-from="20000" data-to="80000" data-step="10000"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="options-wrapper bedrooms">
                                <div class="prop-wrapper">
                                    <span class="properties">Bedrooms</span>

                                </div>
                                <div class="select-wrapper">
                                    <select form="search-form-1" name="bedrooms-min" class="bedrooms-select-min"
                                            data-placeholder="min">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                    <div class="hr">
                                        <hr>
                                    </div>
                                    <select form="search-form-1" name="bedrooms-max" class="bedroom-select-max"
                                            data-placeholder="max">
                                        <option value="1">'1'</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <!--класс single-slider отвечает за стилизацию одиночного слайдера-->
                            <div class="options-wrapper bathrooms single-slider">
                                <div class="prop-wrapper">
                                    <span class="properties">Bathrooms </span>

                                </div>
                                <div class="select-wrapper">
                                    <select form="search-form-1" name="bathrooms-min" class="bathrooms-select-min"
                                            data-placeholder="min">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                    <div class="hr">
                                        <hr>
                                    </div>
                                    <select form="search-form-1" name="bathrooms-max" class="bathrooms-select-max"
                                            data-placeholder="max">
                                        <option value="1">'1'</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="options-wrapper garages">
                                <div class="prop-wrapper">
                                    <span class="properties">Garages</span>

                                </div>
                                <div class="select-wrapper">
                                    <select form="search-form-1" name="garages-min" class="garages-select-min"
                                            data-placeholder="min">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                    <div class="hr">
                                        <hr>
                                    </div>
                                    <select form="search-form-1" name="garages-max" class="garages-select-max"
                                            data-placeholder="max">
                                        <option value="1">'1'</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="apartment-features">
                        <span>Other features:</span>

                        <span class="features-count">4</span>
                        <div class="checkboxes-block">
                            <div>
                                <input type="checkbox" name="conditioning" id="ch1" class="css-checkbox">
                                <label for="ch1" class="css-label">Air Conditioning</label> <br>
                            </div>
                            <div>
                                <input type="checkbox" name="refrigerator" id="ch2" class="css-checkbox">
                                <label for="ch2" class="css-label">Refrigerator</label> <br>
                            </div>


                            <div>
                                <input type="checkbox" name="barbeque" id="ch3" class="css-checkbox">
                                <label for="ch3" class="css-label">Barbeque</label> <br>
                            </div>
                            <div>
                                <input type="checkbox" name="sauna" id="ch4" class="css-checkbox">
                                <label for="ch4" class="css-label">Sauna</label> <br>
                            </div>


                            <div><input type="checkbox" name="dryer" id="ch5" class="css-checkbox">
                                <label for="ch5" class="css-label">Dryer</label> <br>
                            </div>
                            <div><input type="checkbox" name="pool" id="ch6" class="css-checkbox">
                                <label for="ch6" class="css-label">Swimming Pool</label> <br>
                            </div>


                            <div>
                                <input type="checkbox" name="gym" id="ch7" class="css-checkbox">
                                <label for="ch7" class="css-label">Gym</label> <br>
                            </div>

                            <div>
                                <input type="checkbox" name="tv" id="ch8" class="css-checkbox">
                                <label for="ch8" class="css-label">TV Cable</label> <br>
                            </div>


                            <div>
                                <input type="checkbox" name="laundry" id="ch9" class="css-checkbox">
                                <label for="ch9" class="css-label">Laundry</label> <br>
                            </div>
                            <div><input type="checkbox" name="washer" id="ch10" class="css-checkbox">
                                <label for="ch10" class="css-label">Washer</label> <br>
                            </div>


                            <div>
                                <input type="checkbox" name="microwave" id="ch11" class="css-checkbox">
                                <label for="ch11" class="css-label">Microwave</label> <br>
                            </div>
                            <div>
                                <input type="checkbox" name="wifi" id="ch12" class="css-checkbox">
                                <label for="ch12" class="css-label">WI FI</label> <br>
                            </div>
                        </div>


                    </div>

                </div>
            </div>
        </form>
        <!--PROPERTY SEARCH FORM 1 END-->
    </div>

    </div>

    <div class="modal fade" id="create-need-modal" role="dialog">
        <div class="modal-dialog modal-lg">
        <div class="modal-content withoutall">
            <div class="modal-header bgcolorespe">
            <h4 class="modal-title"> Leave a short message so people know exactly what you're after.</h4>
            <button type="button" class="btn btn-specials-oj"  data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i></button>
            </div>
            <div class="modal-body bgcolorespe">
                <textarea placeholder="Tell others what Frances was like."></textarea>
            </div>
            <div class="modal-footer withouthbg">
                <button type="button" class="btn btn-default btnwb" data-dismiss="modal">BACK</button>
                <button type="button" class="btn btn-warning" data-dismiss="modal"  data-toggle="modal" data-target="#create-need-modal2">Create need ad</button>
            </div>
        </div>

        </div>
    </div>
    
</div>


<?php include 'footer.php'; ?>