    <?php include 'head.php' ?>

<!--modal approve-->
<div class="modal fade" id="approve-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    
  <div class="modal-dialog rounded-0" role="document">
      
    <div class="modal-content rounded-0 bg-grey">
        
      <div class="modal-header rounded-0 border-0">
          
        <h4 class="modal-title text-emperor font-700 ">Approve and send message to Sharer</h4>
          
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            
          <span aria-hidden="true">&times;</span>
            
        </button>
          
      </div>
        
      <div class="modal-body">
          
        <textarea></textarea>
          
      </div>
        
        <div class="modal-footer border-0">
            
        <button type="button" class="btn btn-secondary bg-orange text-white text-uppercase font-700 font-open-sans px-5 py-3" style="font-size:13px;" data-dismiss="modal" data-toggle="modal" data-target="#approval-confirmation-modal" >Approve Offer and Send</button>
            

            
        </div>
        
    </div>
      
  </div>
    
</div>


<!--modal approval confirmation-->
<div class="modal fade" id="approval-confirmation-modal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    
  <div class="modal-dialog rounded-0" role="document">
      
    <div class="modal-content rounded-0 bg-grey">
        
      <div class="modal-header rounded-0 border-0 justify-content-start">
          
        <h4 class="modal-title text-emperor font-700 ">You have approved this sharing offer</h4>
          
      </div>
        
      <div class="modal-body">
          
        <p>It has been made live and the sharer has been notified via email.</p>
          
      </div>
        
        <div class="modal-footer border-0">
            
        <button type="button" class="btn btn-secondary bg-orange text-white text-uppercase font-700 font-open-sans px-5 py-3" style="font-size:13px;" data-dismiss="modal">Close</button>
            
        </div>
        
    </div>
      
  </div>
    
</div>


<!--modal deny-->
<div class="modal fade" id="deny-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    
  <div class="modal-dialog rounded-0" role="document">
      
    <div class="modal-content rounded-0 bg-grey">
        
      <div class="modal-header rounded-0 border-0">
          
        <h4 class="modal-title text-emperor font-700 ">Notify Sharer of issues with offer</h4>
          
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            
          <span aria-hidden="true">&times;</span>
            
        </button>
          
      </div>
        
      <div class="modal-body">
          
        <textarea></textarea>
          
      </div>
        
        <div class="modal-footer border-0">
            
        <button type="button" class="btn btn-secondary bg-orange text-white text-uppercase font-700 font-open-sans px-5 py-3" style="font-size:13px;" data-dismiss="modal" data-toggle="modal" data-target="#deny-confirmation-modal">Notify Offer and Send</button>
            

            
        </div>
        
    </div>
      
  </div>
    
</div>

<!--modal deny confirmation-->
<div class="modal fade" id="deny-confirmation-modal" tabindex="-1" role="dialog" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
    
  <div class="modal-dialog rounded-0" role="document">
      
    <div class="modal-content rounded-0 bg-grey">
        
      <div class="modal-header rounded-0 border-0 justify-content-start">
          
        <h4 class="modal-title text-emperor font-700">You have notified the Sharer of issues with offer</h4>
          
      </div>
        
      <div class="modal-body">
          
        <p>An email has been sent to the sharer with the changes you have suggested. The Sharer will have the option to resubmit the offer.</p>
          
      </div>
        
        <div class="modal-footer border-0">
            
        <button type="button" class="btn btn-secondary bg-orange text-white text-uppercase font-700 font-open-sans px-5 py-3" style="font-size:13px;" data-dismiss="modal">Close</button>
            
        </div>
        
    </div>
      
  </div>
    
</div>



<!--modal deny-->
    <div class="modal fade" id="dispute-modal"  data-backdrop="static" data-keyboard="false"  tabindex="-1" role="dialog" aria-hidden="true">
    
  <div class="modal-dialog rounded-0" role="document">
      
    <div class="modal-content rounded-0 bg-grey">
        
      <div class="modal-header rounded-0 border-0">
          
        <h4 class="modal-title text-emperor font-700">Deny and send message to Sharer</h4>
          
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            
          <span aria-hidden="true">&times;</span>
            
        </button>
          
      </div>
        
      <div class="modal-body">
          
        <p>Under Luviat's <a href="#">terms and conditions</a> this sharing offer is not permitted</p>
          
      </div>
        
        <div class="modal-footer border-0">
            
        <button type="button" class="btn btn-secondary bg-orange text-white text-uppercase font-700 font-open-sans px-5 py-3" style="font-size:13px;" data-dismiss="modal" data-toggle="modal" data-target="#dispute-confirmation-modal">Deny and Send</button>
            

            
        </div>
        
    </div>
      
  </div>
    
</div>

<!--modal deny confirmation-->
<div class="modal fade" id="dispute-confirmation-modal"  data-backdrop="static" data-keyboard="false"  tabindex="-1" role="dialog" aria-hidden="true">
    
  <div class="modal-dialog rounded-0" role="document">
      
    <div class="modal-content rounded-0 bg-grey">
        
      <div class="modal-header rounded-0 border-0 justify-content-start">
          
        <h4 class="modal-title text-emperor font-700">You have denied this sharing offer</h4>
          
      </div>
        
      <div class="modal-body">
          
          <p>The sharer has been notified via email.</p>
          
      </div>
        
        <div class="modal-footer border-0">
            
        <button type="button" class="btn btn-secondary bg-orange text-white text-uppercase font-700 font-open-sans px-5 py-3" style="font-size:13px;" data-dismiss="modal">Close</button>
            
        </div>
        
    </div>
      
  </div>
    
</div>


<div class="header">
    
    <div id="approval-head" class="container-fluid d-flex align-items-center">
        <div class="container">
            
            <div class="row">
                
                <div class="col-6 text-left">
                    
                    <h1><i class="fa fa-exclamation-circle icons" aria-hidden="true"></i>&nbsp; Review Sharing Offer</h1>
                    
                </div>
                
                <div class="col-6 text-right d-flex align-items-center justify-content-end">
                    
                    <a href="#" data-toggle="modal" data-target="#approve-modal" class="button-link-normal font-700 mr-3" >Approve</a>
                    
                    <a href="#" data-toggle="modal" data-target="#deny-modal" class="button-link-normal font-700 mr-3" >Notify</a>
                    
                    <a href="#" data-toggle="modal" data-target="#dispute-modal" class="button-link-normal font-700" >Deny</a>
                    
                </div>
                
            </div>
            
        </div>
        
    </div>
    
    <div class="container">
        <div class="wrapper">
            <a href="index.php" class="logo">
                <img src="images/logo-head.png" alt="logo">
            </a>
            <button type="button" data-toggle="collapse" data-target="#header" aria-expanded="false"
                    class="navbar-toggle">
                <i class="fa fa-bars" aria-hidden="true"></i>
            </button>
            <div class="menu-list">
                <div id="header" class="navbar-collapse collapse">
                    <ul>
                        <li>
                            <a href="./home1.html">
                                Explore
                            </a>
                        </li>
                        <li>
                            <a href="./home1.html">
                                About
                            </a>
                        </li>
                        <li>
                            <a href="./home1.html">
                                Contact
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            
            
            
            <div class="subscribe-buttons">
                
                <button class="registration">
                    
                    <a href="share-an-item.php">
                        
                        <i class="fa fa-user text-white" aria-hidden="true"></i>
                        
                        <span class="text-white" >Share Something</span>
                    </a>
                    
                </button>
                
            </div>
        </div>
    </div>
    
</div>