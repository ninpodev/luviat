<?php include 'header.php'; ?>

<body>

	<div class="account-page">
		<div class="account_menu account_menu__normal">
			<a href="#" class="js-toggle-menu">
				<div class="account_menu_agent">
					<div class="agent-avatar">
						<img src="./images/userpic.png" alt="user">
						<div class="agent-status status-online"></div>
					</div>
					<h2>Eunice Rios</h2>
				</div>
			</a>
            
			<?php include 'user-dashboard-menu.php' ?>
            
		</div>
		<div class="account-page-wild_block">
            
			
            <?php include 'dashboard-content.php' ?>
            
            
            
		</div>
	</div>

    </div>

</div>

<!-- MODALS -->

<!-- MODAL APPROVE -->

<div class="modal fade" id="write-review-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

	<div class="modal-dialog rounded-0" role="document">
		
		<div class="modal-content rounded-0 bg-grey">
			
			<div class="modal-header rounded-0 border-0">
				
				<h4 class="modal-title text-emperor font-700 ">Review Frances</h4>
				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				
				<span aria-hidden="true">&times;</span>
				
				</button>
				
			</div>
			
			<div class="modal-body">
				<div class="row">
					<div class="col-12 col-sm-6">
						<div class="user-container">
							<div class="user-block">
								<div class="avatar-image-container">
									<div class="avatar-image">
										<!--agent-status & status-online styles in style.less-->
										<a href="#chatorsomething">
											<img src="./images/userpicbig.png" alt="agent-photo">
										</a>
									</div>
								</div>
							</div>
							<div class="user-block">
								<div class="user-name">
									Frances
								</div>
								<div class="user-stars">
									<div class="stars" data-stars="3">
										<i class="material-icons star">star</i>
										<i class="material-icons star">star</i>
										<i class="material-icons star">star</i>
										<i class="material-icons star">star</i>
										<i class="material-icons star">star</i>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12 col-sm-6">
						<ul class="no-style-list">
							<li>
								<span>Metric #1 </span>
								<div class="stars orange-stars" data-stars="0">
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
								</div>
							</li>
							<li>
								<span>Metric #1 </span>
								<div class="stars orange-stars" data-stars="0">
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
									<i class="material-icons star">star_border</i>
								</div>
							</li>
						</ul>
					</div>
				</div>
				
				<textarea placeholder="Tell others what Frances was like."></textarea>
				
			</div>
			
			<div class="modal-footer border-0">
				
				<button type="button" class="btn btn-secondary floating-button bg-orange text-white text-uppercase font-700 font-open-sans px-5 py-3" style="font-size:13px;" data-dismiss="modal" data-toggle="modal" data-target="#approval-confirmation-modal" >Post review</button>
				
				
			</div>
			
		</div>
		
	</div>
	
</div>

<!-- DENY BORROWING REQUEST -->

<div class="modal fade" id="cancel-borrowing-request" role="dialog" data-backdrop="static">
        
    <div class="modal-dialog modal-lg">
        
        <div class="modal-content bg-light-grey">
            
            <div class="modal-header d-flex justify-content-start">
            
                <h4 class="modal-title">Cancel borrowing request</h4> 
            
            
            </div>
            
            <div class="modal-body">
                
                <p>You are about to cancel borrowing <a href="#" target="_blank"> Ski boots</a>. If you are sure you want to cancel this click on "Confirm Cancellation."</p>
                
            </div>
            
            <div class="modal-footer">
                
                <button type="button" class="btn btn-default btnwb bg-transparent" data-dismiss="modal">Discard</button>
                &nbsp;&nbsp;                
               
                <button type="button" class="btn btn-secondary bg-orange text-white text-uppercase font-700 font-open-sans px-5 py-3" data-dismiss="modal"  data-toggle="modal" data-target="#confirm-borrowing-cancel">Confirm cancellation</button>
            
            </div>
        
        </div>
        
    </div>
    
</div>
    
<!-- CONFIRM BORROWING CANCELLATION -->

<div class="modal fade" id="confirm-borrowing-cancel" role="dialog" data-backdrop="static">
        
    <div class="modal-dialog modal-lg">
        
        <div class="modal-content">
            
            <div class="modal-header justify-content-start">
            
                <h4 class="modal-title">Borrowing request cancelled!</h4>
            
            </div>
            
            <div class="modal-body">
            
                <p class="fz-12-">[Missing copy]</p>
            
            </div>
            
            <div class="modal-footer">
                
                <button type="button" class="btn btn-secondary bg-orange text-white text-uppercase font-700 font-open-sans px-5 py-3" data-dismiss="modal">Close this</button>
            
            </div>
        
        </div>
        
    </div>
    
</div>

<div class="modal fade" id="denysharing" role="dialog">
        <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title">Deny sharing request and send message via email</h4>
                <p></p>
            <button type="button" class="btn btn-specials-oj"  data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i></button>
            </div>
            <div class="modal-body">
                <textarea id="editor2" placeholder="Lorem ipsum dolor sit amet, consectetur adipsiicis, sed do eisuaid labore et dolore magna aliqua. Ut aneim adms imnir pevnit ventinima laboris ullamanco laboris nisi ut aliquimp ex ae comodo cosequat." ></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger redspecial" data-dismiss="modal"  data-toggle="modal" data-target="#sharingdenied">DENY OFFER AND SEND</button>
            </div>
        </div>

        </div>
    </div>
    
<div class="modal fade" id="sharingden" role="dialog">
        <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title"><i class="fas fa-exclamation-triangle"></i> You are about to deny a sharing request!</h4>
            <button type="button" class="btn btn-specials-oj"  data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i></button>
            </div>
            <div class="modal-body">
            <p class="fz-12-">Once you deny this sharing request, the borrower will receive an email notifying them their request has been denied. Their sharing request will then be removed from your Sharing requests tab in your Dashboard.
            </p>
            <p class="fz-12-"> No payment will be made through the Luviat platform and will need to be arranged externally.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btnwb" data-dismiss="modal">DENY REQUEST</button>
                <button type="button" class="btn btn-danger redspecial" data-dismiss="modal"  data-toggle="modal" data-target="#denysharing">DENY REQUEST AND SEND MESSAGE</button>
            </div>
        </div>

        </div>
    </div>



<?php include 'scripts.php'; ?>
</body>
</html>