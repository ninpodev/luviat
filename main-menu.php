<body>
    
<!-- Image and text -->
<div id="main-head" class="header d-flex align-items-center">
    
    <div class="container">
        
        <div class="row">
            
            <!--Logo-->
            <div id="logo-desktop" class="d-flex align-items-center col-xl-2 col-lg-2 col-md-5 col-sm-5 col-5">
                
                <a href="home.php" title="Back to Luviat Home">
                    
                    <img src="images/logo-head.png" alt="">
                    
                </a>
            
            </div>
            
            <!--Menu-->
            <div id="menu-desktop" class="col-xl-3 col-lg-3 col-md-3 d-flex align-items-center">
                
                <ul class="list-inline m-0 p-0">
                    
                    <!--<li class="list-inline-item">
                        
                        <a href="#">Explore</a>
                        
                    </li>-->
                    
                    <li class="list-inline-item">
                        
                        <a href="about.php">About</a>
                        
                    </li>
                    
                    <li class="list-inline-item">
                        
                        <a href="contact.php">Contact</a>
                        
                    </li>
                
                </ul>
            
            </div>
            
            <!--Search-->
            <div id="search-desktop" class="col-xl-3 col-lg-3 col-md-3 d-flex align-items-center">
                
                <form>
                    
                    <input type="text" placeholder="Search eg. Ski Boots">
                    
                </form>
                
            </div>
            
            <!--Registration / Welcome -->
            <div id="reg-desktop" class="col-xl-2 col-lg-2 col-md-2 col-sm-2 d-flex align-items-center justify-content-center flex-column">
            
                <div>
                    <a href="join.php" class="text-orange">Register</a>&nbsp;or&nbsp;<a href="login.php" class="text-orange" >Sign In</a>  
                </div>
                <div>
                    <p class="text-orange m-0" > Welcome <a href="dashboard.php" class="text-orange" style="text-decoration:underline;" >Anita</a>!</p>  
                </div>
            </div>
            
            <!--Share Something-->
            <div id="share-something-desktop" class="d-flex align-items-center justify-content-end col-xl-2 col-lg-2 col-md-7 col-sm-7 col-7 text-right">
                
                <a class="btn btn-sm btn-outline-secondary bg-orange text-white" href="promote-a-sharing-offer.php">
                     Share Something
                </a>
                
                <div id="nav-icon1" class="d-block d-xl-none d-lg-none d-md-block d-sm-block">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                
            </div>
        
        </div>
    
    </div>
    
</div>

<div id="mobile-menu" class="w-100 position-fixed">
    <ul>
        <li>
            <a href="join.php">Join</a>
        </li>
        <li>
            <a href="login.php">Sign In</a>
        </li>
        
        <li>
            <p class="text-orange m-0 p-0" >Welcome <a href="#" style="text-decoration:underline;">Anita</a>!</p>
        </li>
        <li>
            <form class="d-flex justify-content-center" >
                <input type="text" placeholder="Search">
                <input type="submit" value="Search" placeholder="Search">
            </form>
        </li>
            
        <!--<li>
            <a href="#" class="text-uppercase" >Explore</a>
        </li>-->
        <li>
            <a href="#" class="text-uppercase">About</a>
        </li>
        <li>
            <a href="#" class="text-uppercase">Contact</a>
        </li>
            
    </ul>
</div>
    
    