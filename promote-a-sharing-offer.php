<?php include 'header.php' ?>

<body>

    <div class="modal fade" id="updatemyModal" role="dialog">
        <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title">Respond to a need.</h4>
            <button type="button" class="btn btn-specials-red"  data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i></button>
            </div>
            <div class="modal-body">
            <p class="fz-12-">Thanks you for showing interest in responding to a community member's need. To do so, you will need to promote sharing offer. To promote a sharing offer, you'll need to provide us with some details such as a brief overview and availabilities. If you want to share one-off, you can choose yo do so.
            </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btnwb" data-dismiss="modal">BACK</button>
                <button type="button" class="btn btn-warning" data-dismiss="modal">RESPOND</button>
            </div>
        </div>

        </div>
    </div>
    <!--
<div id="preloader">
    <div>
        <canvas id='circleback' height='100' width='100'></canvas>
        <canvas id='circlemain' height='100' width='100'></canvas>
    </div>
</div>-->
    
    
    


    
    
<div class="page-title-simple">
    <div class="container">
        <h1>Share Something</h1>
    </div>
</div>
    
<!-- Items to share -->
<div class="container">
    <div class="row">
         <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12  d-flex align-items-center mb-5">
            <div class="box-share w-100 p-5 mt-5">
                <h3 class="text-white mb-4">Share an Item</h3>
                <h5 class="text-white mb-5 font-weight-bold">I have an item that a local traveller can borrow.</h5>
<!--hola-->
                
                
                <form action="share-an-item.php" method="get" >
                    <input type="text" placeholder="Item Name" class="input-margin-bottom_small w-100 mb-4">
                    <input type="submit" value="Log in / Register to Share" > 
                    
                    
                </form>
            </div>
            
        </div>
        
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12  d-flex align-items-center mb-5">
            <div class="box-share w-100 p-5 mt-5">
                <h3 class="text-white mb-4">Share an Experience</h3>
                <h5 class="text-white mb-5 font-weight-bold">I know something about my local area I want to share with a traveller.</h5>

                <form method="get" action="share-an-experience.php">
                    <input type="text" name="Experience Name" placeholder="Experience Name" class="input-margin-bottom_small w-100 mb-4">
                    <input type="submit" value="Log in / Register to Share"> 
                </form>
            </div>
            
        </div>
        
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12  d-flex align-items-center mb-5">
            <div class="box-share w-100 p-5 mt-5">
                <h3 class="text-white mb-4">Share a Service</h3>
                <h5 class="text-white mb-5 font-weight-bold">I can offer a service or skill that will be helpful to a local traveller.</h5>

                <form method="get" action="share-a-service.php">
                    <input type="text" name="Service Name" placeholder="Service Name" class="input-margin-bottom_small w-100 mb-4">
                    <input type="submit" value="Log in / Register to Share"> 
                </form>
            </div>
            
        </div>
        
        
        
        
        
       
        
        
    </div>
</div>
    
    
<!--PROPERTY SEARCH FORM 1 END-->
<div class="container">

    <!--APARTMENT GRID BEGIN-->
<div class="apartment-grid">
    <div class="item">
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="property-block">
                    <p class="property-type">Item</p>
                    <p class="property-title"><a href="#">Ski Boots</a></p>
                    <!--<div class="apartment-address">
                        <i class="material-icons">place</i>
                        <span class="address">153 Adriana Mews Suite 247</span>
                    </div>-->

                    <div class="apartment-image">
                        <img src="./images/image.png" alt="image">
                        <div class="badges">
                            
                            <p class="sale">For Sale</p>
                        </div>

                    </div>

                    <div class="apartment-values">
                        <span><i class="material-icons">place</i> 6Km Away</span>
                    </div>
                    <div class="apartment-values">
                        <span>Size 12</span>
                        <span>Nordica </span>
                    </div>
                    <div class="apartment-info">
                        <div class="apartment-price">
                            <p class="price-big">$ 24</p>
                            <p class="price-small">week $67</p>
                        </div>
                        <div class="icons">
                            <a href="#" class="clone" data-toggle="modal" data-target="#updatemyModal"> <i class="material-icons icons-style"></i></a>
                            <a href="#" class="heart"><i class="material-icons icons-style"></i></a>
                        </div>
                    </div>
                    <div class="apartment-manager">
                        <div class="manager-wrap">
                            <div class="manager-icon">
                                <a href="#chat"> <img src="./images/userpic.png" alt="userpic"></a>
                                <div class="online-status"></div>
                            </div>
                            <span class="manager-name">Eleanor French</span>
                        </div>
                        <div class="calendar">
                            <i class="material-icons">insert_invitation</i>
                            <span> 2 days ago</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="property-block">
                    <p class="property-type">Experience</p>
                    <p class="property-title"><a href="#">Ski Boots</a></p>
                    <!--<div class="apartment-address">
                        <i class="material-icons">place</i>
                        <span class="address">153 Adriana Mews Suite 247</span>
                    </div>-->

                    <div class="apartment-image">
                        <img src="./images/image.png" alt="image">
                        <div class="badges">
                            
                            <p class="sale">For Sale</p>
                        </div>

                    </div>

                    <div class="apartment-values">
                        <span><i class="material-icons">place</i> 6Km Away</span>
                    </div>
                    <div class="apartment-values">
                        <span>Size 12</span>
                        <span>Nordica </span>
                    </div>
                    <div class="apartment-info">
                        <div class="apartment-price">
                            <p class="price-big">$ 24</p>
                            <p class="price-small">week $67</p>
                        </div>
                        <div class="icons">
                            <a href="#" class="clone"  data-toggle="modal" data-target="#updatemyModal"> <i class="material-icons icons-style"></i></a>
                            <a href="#" class="heart"><i class="material-icons icons-style"></i></a>
                        </div>
                    </div>
                    <div class="apartment-manager">
                        <div class="manager-wrap">
                            <div class="manager-icon">
                                <a href="#chat"> <img src="./images/userpic.png" alt="userpic"></a>
                                <div class="online-status"></div>
                            </div>
                            <span class="manager-name">Eleanor French</span>
                        </div>
                        <div class="calendar">
                            <i class="material-icons">insert_invitation</i>
                            <span> 2 days ago</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="property-block">
                    <p class="property-type">Service</p>
                    <p class="property-title"><a href="#">Ski Boots</a></p>
                    <!--<div class="apartment-address">
                        <i class="material-icons">place</i>
                        <span class="address">153 Adriana Mews Suite 247</span>
                    </div>-->

                    <div class="apartment-image">
                        <img src="./images/image.png" alt="image">
                        <div class="badges">
                            
                            <p class="sale">For Sale</p>
                        </div>

                    </div>

                    <div class="apartment-values">
                        <span><i class="material-icons">place</i> 6Km Away</span>
                    </div>
                    <div class="apartment-values">
                        <span>Size 12</span>
                        <span>Nordica </span>
                    </div>
                    <div class="apartment-info">
                        <div class="apartment-price">
                            <p class="price-big">$ 24</p>
                            <p class="price-small">week $67</p>
                        </div>
                        <div class="icons">
                            <a href="#" class="clone" data-toggle="modal" data-target="#updatemyModal"> <i class="material-icons icons-style"></i></a>
                            <a href="#" class="heart"><i class="material-icons icons-style"></i></a>
                        </div>
                    </div>
                    <div class="apartment-manager">
                        <div class="manager-wrap">
                            <div class="manager-icon">
                                <a href="#chat"> <img src="./images/userpic.png" alt="userpic"></a>
                                <div class="online-status"></div>
                            </div>
                            <span class="manager-name">Eleanor French</span>
                        </div>
                        <div class="calendar">
                            <i class="material-icons">insert_invitation</i>
                            <span> 2 days ago</span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!--APARTMENT GRID END-->
</div>
    


    <?php include 'footer.php' ?>