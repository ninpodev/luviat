<?php include 'header.php'; ?>
<body>

<div class="modal fade" data-backdrop="static" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header justify-content-start text-left">
          <h4 class="modal-title">Your Request has been sent.</h4>
          <button type="button" class="btn btn-specials"  data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i></button>
        </div>
        <div class="modal-body">
          <p class="fz-12-">Thank you for requesting a sharing offer. The sharer has been notified via email and should respond to you shortly. They will either reply via email or approve your request directly.</p>
          <p class="fz-12-">In the mean time, we've created a dashboard for you where you can view your offers, send and receive messages and much more. Feel free to explore this!</p>
        </div>
          <div class="modal-footer">              
              <a href="#" class="text-emperor">Back to Search Results page</a>&nbsp;
              <a href="user-dashboard.php">
              
                  <button type="button" class="btn bg-orange btn-secondary">Go to your Dashboard</button>
                  
              </a>
          </div>
      </div>
    </div>
  </div>

<div class="page-title-simple m-b-30">
    <div class="container">
        <h1 class="text-emperor">Send Request</h1>
    </div>
</div>

<div class="container">
    
    <div class="row">
        
        <div class="col-lg-12">
            
            <nav aria-label="breadcrumb">
  
                <ol class="breadcrumb">
    
                    <li class="breadcrumb-item"><a href="home.php" class="text-orange" >&larr; Back to Review Details</a></li>
                    
  
                </ol>

            </nav>
            
        </div>
        
    </div>
    
</div>


<div class="container">
    <div class="row bor-btm-2">
        <div class="col-lg-8 col-md-7">
            <div class="single-property-description m-b-30">
                <div class="sidebar-agent bg-white border-0">
                    <div class="agent-info-wrap p-0 bg-white border-0">
                        <div class="row">
                            <div class="col-2">
                                <div class="agent-img">
                                    <img src="./images/mask.png" alt="agent-img">
                                </div>
                            </div>
                            <div class="col-10">
                                <div class="agent-name mb-3 text-emperor">
                                    Frances Modalu
                                </div>
                                <ul class="list-inline">
                                    <li class="list-inline-item text-orange mr-5">
                                        <h5><i class="fa fa-globe" aria-hidden="true"></i> Canberra, Australia</h5>
                                    </li>
                                    <li class="list-inline-item text-orange mr-5">
                                        <h5><i class="fa fa-mobile" aria-hidden="true"></i> 686-986-7249</h5>
                                    </li>
                                    <ul class="list-inline-item">
                                        <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                        <li class="list-inline-item text-emperor m-0 p-0"><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                        <li class="list-inline-item text-emperor m-0 p-0"><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                    </ul>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="single-property-details mb-5 bg-grey">
                <h4 class="text-emperor font-700 mb-5">Email the Sharer</h3>
                
                <textarea rows="8" placeholder="Description" maxlength="600" data-limit="600" ></textarea>
                            <span class="countdown">600 Characters remaining</span>
                
            </div>            
            <span class="fs-12"><small>Please write a message to your host sharer. Tell them a little about yourself and why you are requesting the offer. Your sharer will feel more willing to accept your request if they feel they know you a little.</small></span>
        </div>
        <div class="col-lg-4 col-md-5">
            <div class="mb-5 m-0 border-0">
                <div class="border bg-grey p-5">
                    <div class="row">
                        <div class="col-md-12 pd-bt-4">
                            <span class="font-400 mb-4 fs-18">Ski Boots <small class="pd-l5"> <a href="#">view item</a></small></span>
                        </div>
                        <div class="col-md-12">
                            <ul class="list-inline">
                                <li class="list-inline-item text-orange">
                                    <span class="fs-18"><i class="fa fa-globe" aria-hidden="true"></i> Canberra, Australia</span>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-12">
                            <ul class="wp">
                                <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                <li class="list-inline-item text-emperor m-0 p-0"><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                <li class="list-inline-item text-emperor m-0 p-0"><i class="fa fa-star-o" aria-hidden="true"></i></li>
                            </ul>
                        </div>
                        <div class="col-md-12 bor-btm"></div>
                        <div class="pad-top">
                            <div class="col-md-6">
                                <div class="mb-4">
                                    <label class="font-open-sans font-400 text-emperor bg-transparent">Borrow Date</label>
                                    <div class="clearfix"></div>
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1">
                                            <i class="far fa-calendar-alt"></i>
                                        </span>
                                        <input type="text" class="form-control w-100 datepick" aria-describedby="basic-addon1">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-4">
                                    <label class="font-open-sans font-400 text-emperor bg-transparent">Return Date</label>
                                    <div class="clearfix"></div>
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1">
                                            <i class="far fa-calendar-alt"></i>
                                        </span>
                                        <input type="text" class="form-control w-100 datepick" aria-describedby="basic-addon1">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="mb-4">
                                <div class="row">
                                    <div class="col-6">
                                        <input type="checkbox" name="Pickup" id="Pickup" class="css-checkbox" checked>
                                        <label for="Pickup" class="css-label">Pickup</label>
                                    </div>
                                    <div class="col-6">
                                        <input type="checkbox" name="Delivery" id="Delivery" class="css-checkbox">
                                        <label for="Delivery" class="css-label">Delivery</label>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-0 border-top">
                                <div class="row">
                                    <div class="col-6 text-left">
                                        <p>Delivery Fee</p>
                                    </div>
                                    <div class="col-6 text-right">
                                        <p>$6</p>
                                    </div>
                                </div>
                            </div>
                            <div class="border-top">
                                <div class="row">
                                    <div class="col-6 text-left">
                                        <p class="font-weight-bold font-700" >Total</p>
                                    </div>
                                    <div class="col-6 text-right">
                                        <p>$21</p>
                                    </div>
                                </div>
                            </div>
                            
                            <div>
                                
                                <p class="colorgray"><strong>Note: Luviat does not manage payment.</strong></p>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <p></p>
                
            </div>
            <div class="mb-4">
                <button type="submit" class="btn btn-secondary bg-orange text-white text-uppercase font-700 font-open-sans px-5 py-4 w-100" style="font-size:13px;" data-toggle="modal" data-target="#myModal">SEND REQUEST</button>
            </div>
        </div>
    </div>
</div>
<!--PROPERTY SLIDER END-->
<div class="container js-home-1-search" style="margin-top: 31px">

    <!--PROPERTY SEARCH FORM 1 BEGIN-->

<form id="search-form-1"  class="mt-5">
    <div class="property-search">
        <div class="main-apartment-search-options bg-orange">
        <div class="options-wrapper-main special-grid">
                <div class="wrapper">
                    <h3 class="t-white">Not what you're looking for? Try another search</h3>
                </div>
                <div class="wrapper">
                </div>
                <div class="wrapper">
                </div>
                <div class="wrapper">
                </div>
                <div class="wrapper">
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 search-property">
                    <input type="search" class="property-searchinput searchspecial" placeholder="Keyword">
                </div>
                <div class="col-md-2">
                    <select form="search-form-1" name="location" class="location-select big-input searchspecial"
                            data-placeholder="location">
                        <option value="london">Paris, FR</option>
                        <option value="miami">Miami</option>
                        <option value="new-york">New-York</option>
                        <option value="houston">Houston</option>

                    </select>
                </div>
                <div class="col-md-3">
                    <select form="search-form-1" name="property" class="property-select big-input"
                            data-placeholder="type">
                        <option value="apartment">Area of Interest</option>
                        <option value="farm">Farm</option>
                        <option value="condo">Condo</option>
                        <option value="multi-family-house">Multi Family House</option>
                        <option value="townhouse">Townhouse</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <select form="search-form-1" name="status" class="status-select big-input"
                            data-placeholder="status">
                        <option value="Dates">Dates</option>
                        <option value="rent" data-text="rent">For Rent</option>
                        <option value="new" data-text="new">New</option>
                        <option value="featured" data-text="featured">Featured</option>
                        <option value="reused" data-text="reused">Reused</option>
                        <option value="hot" data-text="hot">Hot Offer</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <button class="property-button">SEARCH</button>
                </div>
            </div>
        </div>

    </div>
</form>


<!--PROPERTY SEARCH FORM 1 END-->
</div>


<?php include 'footer.php' ?>