<?php include 'header.php'; ?>
<body>


    <div id="main-slder-home" class="d-flex align-items-center cover" >
        
        <div class="container">
        
            <div class="row justify-content-center">
            
                <div class="col-12">
                    <h1 class="text-white mb-2 display-3" >Our wonderous world - see it, feel it!</h1>
                    <br>
                    <h4 class="text-white mb-2">Connect and share items, experiences and services with travellers and locals</h2>
                    <br>

                    <div class="js-home-2-search">
                        
                        <form id="search-form-1">
            
                            <div class="property-search bg-white">
                
                                <div class="main-apartment-search-options">
                    
                                    <div class="options-title">
                        
                                        <h4 class="agent-name mb-3 text-emperor">What are you looking for?</h4>
                    
                                    </div>
                    
                                    <form>
                    
                                        <div class="options-wrapper-main">
                        
                        
                                            <div class="wrapper search-property">
                            
                                                <input type="search" class="property-searchinput" placeholder="Area of interest">
                        
                                            </div>

                        
                                            <div class="wrapper">

                            
                                                <select form="search-form-1" name="location" class="location-select big-input"
                                    data-placeholder="location">
                                
                                                    <option value="london">London</option>
                                
                                                    <option value="miami">Miami</option>
                                
                                                    <option value="new-york">New-York</option>
                                
                                                    <option value="houston">Houston</option>

                            
                                                </select>
                        
                                            </div>

                        
                                            <div class="wrapper search-property">
                            
                                                <input type="search" class="property-searchinput" placeholder="Keyword">
                        
                                            </div>
                        
                                            <div class="wrapper">

                            <div class="range-picker-wrapper position-relative" >
                                <i class="fa fa-calendar text-orange" aria-hidden="true"></i>
                                <input type="text" class="dates-input pl-5 datepick" placeholder="Dates">
                            </div>
                        </div>
                        
                                            <div class="wrapper d-flex justify-content-start">
                            <a href="#" data-toggle="modal" data-target="#create-need-modal" class="cta-btn" >Search</a>
                        </div>
                    
                                        </div>
                    
                                    </form>
                
                                </div>
            
                            </div>
        
                        </form>
                    
                    </div>
                
                </div>
        
            </div>
            
        </div>
        
    </div>
    

    <div class="container js-home-1-search" style="margin-top: 31px"></div>

    <div class="container">

        <div class="apartment-grid">
        <div class="col-md-6">
            <h3>NEW SHARING OFFERS <span class="flssl">(Canberra, Australia)</span></h3> 
        </div>
        <div class="col-md-6">
            <div class="button-wrapper btnwrspe" style="float: right;">
                <a href="#"><i class="fas fa-map-marker-alt"></i> SEE RESULTS ON MAP</a>
            </div>
        </div>
    <div class="owl-carousel owl-theme">
            <div class="property-carousel-arrow-block">
            </div>
        </div>
        <div class="item">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="property-block">
                        <p class="property-type">ITEM</p>
                        <p class="property-title">Samsung G15 Charger (Aus)..</p>
                        <div class="apartment-image">
                            <img src="./images/image.png" alt="image">
                            <div class="badges">
                                <p class="rent">AVAILABLE</p>
                            </div>
                        </div>

                        <div class="apartment-values row">
                            <div class="col-md-5">
                                <i class="fas fa-map-marker-alt"></i>
                                <span class="txt-g">2km away</span>
                            </div>
                            <div class="col-md-5">
                                <i class="far fa-calendar-check"></i>
                                <span class="txt-g">Available now</span>
                            </div>
                            <div class="col-md-2 icons">
                                <a href="#" class="heart"><i class="material-icons icons-style"></i></a>
                            </div>
                        </div>
                        <div class="apartment-info s-grid">
                            <span class="txt-g">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                Nunc dignissim turpis ut posuere tempor. Fusce gravida, 
                            </span>        
                        </div>
                        <div class="apartment-info">
                            <div class="apartment-price">
                                <p class="price-small">$4 AUD / hour</p>
                            </div>
                            <div class="apartment-price">
                                <p class="price-small">$8 AUD / hour</p>
                            </div>
                        </div>
                        <div class="apartment-manager">
                            <div class="manager-wrap row">
                                    <div class="col-md-2">
                                        <div class="manager-icon">
                                            <a href="#chat"> <img src="./images/userpic.png" alt="userpic"></a>
                                            <div class="online-status"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class=" text-orange">
                                            Pauline Saunders
                                        </div>
                                        
                                        <ul class="list-inline">
                                            <div class="fl">
                                                <li class="list-inline-item text-orange">
                                                    <i class="fa fa-globe" aria-hidden="true"></i> 
                                                    <span> Canberra, Aus</span>
                                                </li>
                                            </div>
                                            <div class="fr">
                                                <li class="list-inline-item" >
                                                    
                                                    <ul class="list-inline">

                                                        <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                                        
                                                        <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                                        
                                                        <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>

                                                        <li class="list-inline-item text-emperor m-0 p-0"><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                        
                                                        <li class="list-inline-item text-emperor m-0 p-0"><i class="fa fa-star-o" aria-hidden="true"></i></li>

                                                    </ul>
                                                    
                                                </li>
                                            </div>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="property-block">
                        <p class="property-type">EXPERIENCE</p>
                        <p class="property-title">3 Day Camping/Hiking Adve..</p>

                        <div class="apartment-image">
                            <img src="./images/image.png" alt="image">
                            <div class="badges">
                                <p class="rent">AVAILABLE</p>
                            </div>

                        </div>

                        <div class="apartment-values row">
                            <div class="col-md-5">
                                <i class="fas fa-map-marker-alt"></i>
                                <span class="txt-g">Andes,Peru</span>
                            </div>
                            <div class="col-md-5">
                                <i class="far fa-calendar-check"></i>
                                <span class="txt-g">On Demand</span>
                            </div>
                            <div class="col-md-2 icons">
                                <a href="#" class="heart"><i class="material-icons icons-style"></i></a>
                            </div>
                        </div>
                        <div class="apartment-info s-grid">
                            <span class="txt-g">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                Nunc dignissim turpis ut posuere tempor. Fusce gravida, 
                            </span>        
                        </div>
                        <div class="apartment-info">
                            <div class="apartment-price">
                                <p class="price-small">$400 AUD / person</p>
                            </div>
                            <div class="apartment-price">
                                <p class="price-small">$1200 AUD / gro.</p>
                            </div>
                        </div>
                        <div class="apartment-manager">
                            <div class="manager-wrap row">
                                <div class="col-md-2">
                                    <div class="manager-icon">
                                        <a href="#chat"> <img src="./images/userpic.png" alt="userpic"></a>
                                        <div class="online-status"></div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class=" text-orange">
                                        Pauline Saunders
                                    </div>
                                    
                                    <ul class="list-inline">
                                        <div class="fl">
                                            <li class="list-inline-item text-orange">
                                                <i class="fa fa-globe" aria-hidden="true"></i> 
                                                <span> Lima, Peru</span>
                                            </li>
                                        </div>
                                        <div class="fr">
                                            <li class="list-inline-item" >
                                                
                                                <ul class="list-inline">

                                                    <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    
                                                    <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    
                                                    <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>

                                                    <li class="list-inline-item text-emperor m-0 p-0"><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                    
                                                    <li class="list-inline-item text-emperor m-0 p-0"><i class="fa fa-star-o" aria-hidden="true"></i></li>

                                                </ul>
                                                
                                            </li>
                                        </div>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="property-block">
                        <p class="property-type">SERVICE</p>
                        <p class="property-title">Visa / Immigration Help</p>

                        <div class="apartment-image">
                            <img src="./images/image.png" alt="image">
                            <div class="badges">
                                <p class="rent">AVAILABLE</p>
                            </div>

                        </div>

                        <div class="apartment-values row">
                            <div class="col-md-5">
                                <i class="fas fa-map-marker-alt"></i>
                                <span class="txt-g">8km away</span>
                            </div>
                            <div class="col-md-5">
                                <i class="far fa-calendar-check"></i>
                                <span class="txt-g-2">By Appointment</span>
                            </div>
                            <div class="col-md-2 icons">
                                <a href="#" class="heart"><i class="material-icons icons-style"></i></a>
                            </div>
                        </div>
                        <div class="apartment-info s-grid">
                            <span class="txt-g">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                Nunc dignissim turpis ut posuere tempor. Fusce gravida, 
                            </span>        
                        </div>
                        <div class="apartment-info">
                            <div class="apartment-price">
                                <p class="price-small">By Negotiation</p>
                            </div>
                            <div class="apartment-price">
                                <p class="price-small"></p>
                            </div>
                        </div>
                        <div class="apartment-manager">
                            <div class="manager-wrap row">
                                <div class="col-md-2">
                                    <div class="manager-icon">
                                        <a href="#chat"> <img src="./images/userpic.png" alt="userpic"></a>
                                        <div class="online-status"></div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class=" text-orange">
                                        Kevin Samson
                                    </div>
                                    
                                    <ul class="list-inline">
                                        <div class="fl">
                                            <li class="list-inline-item text-orange">
                                                <i class="fa fa-globe" aria-hidden="true"></i> 
                                                <span> Canberra, Aus</span>
                                            </li>
                                        </div>
                                        <div class="fr">
                                            <li class="list-inline-item" >
                                                
                                                <ul class="list-inline">

                                                    <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    
                                                    <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    
                                                    <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>

                                                    <li class="list-inline-item text-emperor m-0 p-0"><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                    
                                                    <li class="list-inline-item text-emperor m-0 p-0"><i class="fa fa-star-o" aria-hidden="true"></i></li>

                                                </ul>
                                                
                                            </li>
                                        </div>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="item">
            <div class="row">


                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="property-block">
                        <p class="property-type">ITEM</p>
                        <p class="property-title">Samsung G15 Charger (Aus)..</p>

                        <div class="apartment-image">
                            <img src="./images/image.png" alt="image">
                            <div class="badges">
                                <p class="rent">AVAILABLE</p>
                            </div>

                        </div>

                        <div class="apartment-values row">
                            <div class="col-md-5">
                                <i class="fas fa-map-marker-alt"></i>
                                <span class="txt-g">2km away</span>
                            </div>
                            <div class="col-md-5">
                                <i class="far fa-calendar-check"></i>
                                <span class="txt-g">Available now</span>
                            </div>
                            <div class="col-md-2 icons">
                                <a href="#" class="heart"><i class="material-icons icons-style"></i></a>
                            </div>
                        </div>
                        <div class="apartment-info s-grid">
                            <span class="txt-g">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                Nunc dignissim turpis ut posuere tempor. Fusce gravida, 
                            </span>        
                        </div>
                        <div class="apartment-info">
                            <div class="apartment-price">
                                <p class="price-small">$4 AUD / hour</p>
                            </div>
                            <div class="apartment-price">
                                <p class="price-small">$8 AUD / hour</p>
                            </div>
                        </div>
                        <div class="apartment-manager">
                            <div class="manager-wrap row">
                                    <div class="col-md-2">
                                        <div class="manager-icon">
                                            <a href="#chat"> <img src="./images/userpic.png" alt="userpic"></a>
                                            <div class="online-status"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class=" text-orange">
                                            Pauline Saunders
                                        </div>
                                        
                                        <ul class="list-inline">
                                            <div class="fl">
                                                <li class="list-inline-item text-orange">
                                                    <i class="fa fa-globe" aria-hidden="true"></i> 
                                                    <span> Canberra, Aus</span>
                                                </li>
                                            </div>
                                            <div class="fr">
                                                <li class="list-inline-item" >
                                                    
                                                    <ul class="list-inline">

                                                        <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                                        
                                                        <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                                        
                                                        <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>

                                                        <li class="list-inline-item text-emperor m-0 p-0"><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                        
                                                        <li class="list-inline-item text-emperor m-0 p-0"><i class="fa fa-star-o" aria-hidden="true"></i></li>

                                                    </ul>
                                                    
                                                </li>
                                            </div>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="property-block">
                        <p class="property-type">EXPERIENCE</p>
                        <p class="property-title">3 Day Camping/Hiking Adve..</p>

                        <div class="apartment-image">
                            <img src="./images/image.png" alt="image">
                            <div class="badges">
                                <p class="rent">AVAILABLE</p>
                            </div>

                        </div>

                        <div class="apartment-values row">
                            <div class="col-md-5">
                                <i class="fas fa-map-marker-alt"></i>
                                <span class="txt-g">Andes,Peru</span>
                            </div>
                            <div class="col-md-5">
                                <i class="far fa-calendar-check"></i>
                                <span class="txt-g">On Demand</span>
                            </div>
                            <div class="col-md-2 icons">
                                <a href="#" class="heart"><i class="material-icons icons-style"></i></a>
                            </div>
                        </div>
                        <div class="apartment-info s-grid">
                            <span class="txt-g">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                Nunc dignissim turpis ut posuere tempor. Fusce gravida, 
                            </span>        
                        </div>
                        <div class="apartment-info">
                            <div class="apartment-price">
                                <p class="price-small">$400 AUD / person</p>
                            </div>
                            <div class="apartment-price">
                                <p class="price-small">$1200 AUD / gro.</p>
                            </div>
                        </div>
                        <div class="apartment-manager">
                            <div class="manager-wrap row">
                                <div class="col-md-2">
                                    <div class="manager-icon">
                                        <a href="#chat"> <img src="./images/userpic.png" alt="userpic"></a>
                                        <div class="online-status"></div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class=" text-orange">
                                        Pauline Saunders
                                    </div>
                                    
                                    <ul class="list-inline">
                                        <div class="fl">
                                            <li class="list-inline-item text-orange">
                                                <i class="fa fa-globe" aria-hidden="true"></i> 
                                                <span> Lima, Peru</span>
                                            </li>
                                        </div>
                                        <div class="fr">
                                            <li class="list-inline-item" >
                                                
                                                <ul class="list-inline">

                                                    <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    
                                                    <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    
                                                    <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>

                                                    <li class="list-inline-item text-emperor m-0 p-0"><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                    
                                                    <li class="list-inline-item text-emperor m-0 p-0"><i class="fa fa-star-o" aria-hidden="true"></i></li>

                                                </ul>
                                                
                                            </li>
                                        </div>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="property-block">
                        <p class="property-type">SERVICE</p>
                        <p class="property-title">Visa / Immigration Help</p>

                        <div class="apartment-image">
                            <img src="./images/image.png" alt="image">
                            <div class="badges">
                                <p class="rent">AVAILABLE</p>
                            </div>

                        </div>

                        <div class="apartment-values row">
                            <div class="col-md-5">
                                <i class="fas fa-map-marker-alt"></i>
                                <span class="txt-g">8km away</span>
                            </div>
                            <div class="col-md-5">
                                <i class="far fa-calendar-check"></i>
                                <span class="txt-g-2">By Appointment</span>
                            </div>
                            <div class="col-md-2 icons">
                                <a href="#" class="heart"><i class="material-icons icons-style"></i></a>
                            </div>
                        </div>
                        <div class="apartment-info s-grid">
                            <span class="txt-g">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                Nunc dignissim turpis ut posuere tempor. Fusce gravida, 
                            </span>        
                        </div>
                        <div class="apartment-info">
                            <div class="apartment-price">
                                <p class="price-small">By Negotiation</p>
                            </div>
                            <div class="apartment-price">
                                <p class="price-small"></p>
                            </div>
                        </div>
                        <div class="apartment-manager">
                            <div class="manager-wrap row">
                                <div class="col-md-2">
                                    <div class="manager-icon">
                                        <a href="#chat"> <img src="./images/userpic.png" alt="userpic"></a>
                                        <div class="online-status"></div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class=" text-orange">
                                        Kevin Samson
                                    </div>
                                    
                                    <ul class="list-inline">
                                        <div class="fl">
                                            <li class="list-inline-item text-orange">
                                                <i class="fa fa-globe" aria-hidden="true"></i> 
                                                <span> Canberra, Aus</span>
                                            </li>
                                        </div>
                                        <div class="fr">
                                            <li class="list-inline-item" >
                                                
                                                <ul class="list-inline">

                                                    <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    
                                                    <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    
                                                    <li class="list-inline-item m-0 p-0 text-emperor"><i class="fa fa-star" aria-hidden="true"></i></li>

                                                    <li class="list-inline-item text-emperor m-0 p-0"><i class="fa fa-star-o" aria-hidden="true"></i></li>
                                                    
                                                    <li class="list-inline-item text-emperor m-0 p-0"><i class="fa fa-star-o" aria-hidden="true"></i></li>

                                                </ul>
                                                
                                            </li>
                                        </div>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>

    </div>


    <div id="how-it-works" class="py-5">
    
        <div class="container">
        
            <div class="row">
            
                <div class="col-md-12">
                
                    <h2 class="text-white display-3 text-shadow mb-5">
                        How it works
                    </h2>
                
                    <h3 class="text-white text-shadow mb-5 w-50">
                        Find locals who are willing to share items, services and experiences based on an area of interest.
                    </h3>
                
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12 mb-4 item">
                    
                        <div class="bg-og p-4 box-shadow">
                            
                            <img src="./images/map.png" alt="agent" class="mb-4" >
                            <div class="clearfix"></div>
                            <p class="text-uppercase mb-3 d-block text-white">Step One</p>
                            <div class="clearfix"></div>
                            <h4 class="mb-3 font-700">Plan your trip!</h4>
                            <div class="clearfix"></div>
                            <p class="text-white" >Decide where you're going to travel or start by searching what Luviat's community is offering.</p>
                    
                        </div>
                
                    </div>
                
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12 mb-4 item">
                    
                        <div class="bg-og p-4 box-shadow">
                            
                            <img src="./images/map.png" alt="agent" class="mb-4" >
                            <div class="clearfix"></div>
                            <p class="text-uppercase mb-3 d-block text-white">Step Two</p>
                            <div class="clearfix"></div>
                            <h4 class="mb-3 font-700">Find sharing offers</h4>
                            <div class="clearfix"></div>
                            <p class="text-white">Search items, experiences and services by location, date or area of interest.</p>
                    
                        </div>
                
                    </div>
                
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12 mb-4 item">
                    
                        <div class="bg-og p-4 box-shadow">
                            
                            <img src="./images/map.png" alt="agent" class="mb-4" >
                            <div class="clearfix"></div>
                            <p class="text-uppercase mb-3 d-block text-white">Step Three</p>
                            <div class="clearfix"></div>
                            <h4 class="mb-3 font-700">Request a Booking</h4>
                            <div class="clearfix"></div>
                            <p class="text-white">Select the sharing offers you're interested in &amp; contact the owner to arrange pickup or meetup.</p>
                    
                        </div>
                
                    </div>
                
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12 mb-4 item">
                    
                        <div class="bg-og p-4 box-shadow">
                            
                            <img src="./images/map.png" alt="agent" class="mb-4" >
                            <div class="clearfix"></div>
                            <p class="text-uppercase mb-3 d-block text-white">Step Four</p>
                            <div class="clearfix"></div>
                            <h4 class="mb-3 font-700">Travel Light, worry less</h4>
                            <div class="clearfix"></div>
                            <p class="text-white">Don't agonise over what to pack, travel lighter and forgotten or lost luggage won't sour your trip.</p>
                    
                        </div>
                
                    </div>
            
                </div>
        
            </div>
    
        </div>

    </div>


<div class="container mt-20">
    <div class="row">
        <div class="col-md-12 mb-4">
            <h2 class="display-3">
                Share something
            </h2> 
        </div>
        <div class="col-md-6">
            <h4>Meet travellers, share your local knowledge and join the Luviat sharing community.</h4> 
        </div>

        <div class="container">
            
            <div class="row">
                
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12  d-flex align-items-center">
                    
                    <div class="box-share w-100 p-5 mt-5">
                        
                        <h3 class="text-white mb-4">Share an Item</h3>
                        
                        <h5 class="text-white mb-5 font-weight-bold">I have an item that a local traveller can borrow.</h5>
                        
                        <form action="share-an-item.php" method="get" >
                            
                            <input type="text" placeholder="Item Name" class="input-margin-bottom_small w-100 mb-4">
                            
                            <input type="submit" value="Log in / Register to Share" >                             
                        </form>
                    
                    </div>
                    
                </div>
                
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12  d-flex align-items-center">
                    <div class="box-share w-100 p-5 mt-5">
                        
                        <h3 class="text-white mb-4">Share an Experience</h3>
                        
                        <h5 class="text-white mb-5 font-weight-bold">I know something about my local area I want to share with a traveller.</h5>

                        <form method="get" action="share-an-experience.php">
                            <input type="text" name="Experience Name" placeholder="Experience Name" class="input-margin-bottom_small w-100 mb-4">
                            <input type="submit" value="Log in / Register to Share"> 
                        </form>
                        
                    </div>
                    
                </div>
                
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12  d-flex align-items-center">
                    <div class="box-share w-100 p-5 mt-5">
                        <h3 class="text-white mb-4">Share an Service</h3>
                        <h5 class="text-white mb-5 font-weight-bold">I can offer a service or skill that will be helpful to a local traveller.</h5>

                        <form method="get" action="share-a-service.php">
                            <input type="text" name="Service Name" placeholder="Service Name" class="input-margin-bottom_small w-100 mb-4">
                            <input type="submit" value="Log in / Register to Share"> 
                        </form>
                    </div>
                    
                </div>
                
            </div>
        </div>
        
    </div>
    
    <div class="row">
        
        <div id="or-line" class="col-12 text-center p-5 position-relative mt-4">
            
            <p class="d-inline bg-white px-4 font-700">Or</p>
            
            <span class="position-absolute border-bottom w-100">&nbsp;</span>

        </div>
        
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <h2>Try searching for something</h2> 
        </div>

        <div class="container">
            <div class="row pdbtn">
                <div class="col-md-12">
                    <form id="search-form-1"  class="mt-5">
                        <div class="property-search">
                            <div class="main-apartment-search-options bg-orange">
                                
                            <div class="options-wrapper-main">
                                    <div class="wrapper search-property">
                                        <h3 class="t-white">What are you looking for?</h3>
                                    </div>
                                    <div class="wrapper">
                                    </div>
                                    <div class="wrapper">
                                    </div>
                                    <div class="wrapper">
                                    </div>
                                    <div class="wrapper advanced-wrapper">
                                    </div>
                                </div>
                                
                                <div class="row">
                                    
                                    <div class="col-md-3 search-property">
                                        <input type="search" class="property-searchinput searchspecial" placeholder="Keyword">
                                    </div>
                                    
                                    <div class="col-md-2">
                                        
                                        <select form="search-form-1" name="location" class="location-select big-input searchspecial selectized" data-placeholder="location" tabindex="-1" style="display: none;">
                                            <option value="london" selected="selected">
                                                Paris, FR
                                            </option>
                                            
                                        </select>
                                        
                                        <div class="selectize-control location-select big-input searchspecial single">
                                            <div class="selectize-input items full has-options has-items">
                                                <div class="item" data-value="london">Paris, FR</div>
                                                
                                                <input type="text" autocomplete="off" tabindex="" style="width: 4px;">
                                            </div>
                                            
                                            <div class="selectize-dropdown single location-select big-input searchspecial" style="display: none; width: 152px; top: 39px; left: 0px;">
                                                
                                                <div class="selectize-dropdown-content"></div>
                                                
                                            </div>
                                            
                                        </div>
                                        
                                    </div>
                                    
                                    <div class="col-md-3">
                                        <select form="search-form-1" name="location" class="location-select big-input searchspecial selectized" data-placeholder="location" tabindex="-1" style="display: none;"><option value="london" selected="selected">Paris, FR</option>
                                        </select>
                                        <div class="selectize-control location-select big-input searchspecial single">
                                            <div class="selectize-input items full has-options has-items">
                                                <div class="item" data-value="london">Area of Interest</div>
                                                <input type="text" autocomplete="off" tabindex="" style="width: 4px;">
                                            </div>
                                            <div class="selectize-dropdown single location-select big-input searchspecial" style="display: none; width: 152px; top: 39px; left: 0px;">
                                                <div class="selectize-dropdown-content"></div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2">
                                        
                                        <select form="search-form-1" name="location" class="location-select big-input searchspecial selectized" data-placeholder="location" tabindex="-1" style="display: none;"><option value="london" selected="selected">Paris, FR</option>
                                        </select>
                                        
                                        <div class="selectize-control location-select big-input searchspecial single">
                                            <div class="selectize-input items full has-options has-items">
                                                <div class="item" data-value="london">Dates</div>
                                                <input type="text" autocomplete="off" tabindex="" style="width: 4px;">
                                            </div>
                                            <div class="selectize-dropdown single location-select big-input searchspecial" style="display: none; width: 152px; top: 39px; left: 0px;">
                                                <div class="selectize-dropdown-content"></div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2">
                                        <button class="property-button">SEARCH</button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
        
    </div>
    
</div>


<!--CALL TO ACTION3 BEGIN-->
<div class="call-to-action-section-3">
    <div class="container">
        <div class="col-md-12">
            <span class="ll">Interested in partnering with Luviat? Get in touch... <b>hello@luviat.com</b></span>
        </div>
        <div class="col-md-12">
            <span class="lm">Luviat aims to make travel more enriching for our visitors, we're looking for partners that we can connect travellers with. If this sounds like you, we'd love to hear from you!</span>
        </div>
    </div>
</div>
<!--CALL TO ACTION3 END-->


<?php include 'footer.php'; ?>