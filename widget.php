<?php include 'head.php'; ?>

<body>

    <div class="container">
    
        <div class="row">
            
            <div class="col-12">

                <div id="widget-modal" class="specialcont col-md-12 bg-grey">
                            
                        <div class="col-12">

                            <h3>Availability*</h3>
                            
                            <br>
                            
                            <p>When is this item/experience/service available?</p>

                            <div class="form-box">

                                <div class="row">

                                    <div class="col-6" >

                                        <label class="radio-label">
                                            On request (available until you make it unavailable)

                                            <input type="radio" name="availability" id="on-request-check" checked>

                                            <span class="checkmark"></span>

                                        </label>

                                    </div>

                                    <div class="col-6">

                                        <label class="radio-label">On specified date(s)/time(s)

                                            <input type="radio" name="availability" id="on-specific-check">

                                            <span class="checkmark"></span>

                                        </label>

                                    </div>

                                </div>

                            </div>

                        </div>
                        
                        <div id="request" class="row">
                    
                        </div>
                        
                        <div id="specific-times" class="row">
                            
                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6 mb-4">
                                <input class="datepick" placeholder="DDMMYY">
                            </div>
                            
                            <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6 col-6 mb-4">
                                <input class="timepick" placeholder="00:00">
                            </div>
                            <div class="col-xl-2 col-lg-2 col-md-12 col-sm-12 col-12 p-xl-0 p-lg-0 p-md-0 p-sm-5 d-flex justify-content-center align-items-center mb-4">
                                <span> to </span>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6 mb-4">
                                <input class="datepick" placeholder="DDMMYY">
                            </div>
                            <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6 col-6 mb-4">
                                <input class="timepick" placeholder="00:00">
                            </div>
                            
                            <!--<div class="col-12">
                                
                                <select id="cities" class="selectpicker" data-size="40">
                                    <option>Doesn't repeat</option>
                                    <option>Daily</option>
                                    <option>Weekly on [day of the week]</option>
                                    <option>Monthly on the first [day]</option>
                                    <option>Anually on 01 sept</option>
                                    <option>Every weekday</option>
                                    <option value="custom">Custom</option>
                                </select>       
                                
                            </div>-->
                    
                        </div>
                        
                    </div>

            </div>

        </div>

    </div>


    
    <div class="modal fade" id="custom-modal" tabindex="-1" role="dialog" aria-labelledby="custom-modalLabel" aria-hidden="true">
      
        <div class="modal-dialog" role="document">
        
            <div class="modal-content">
          
                <div class="modal-body">
            
                    <p>hola</p>
          
                </div>
        
            </div>
      
        </div>
    
    </div>
    
    
<?php include 'scripts.php'; ?>

</body>

</html>